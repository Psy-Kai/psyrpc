#include <mutex>
#include <thread>

#include <google/protobuf/wrappers.pb.h>

#include <psyrpc/client/channel.h>
#include <psyrpc/client/result.h>
#include <psyrpc/client/servicebase.h>
#include <psyrpc/client/transferinterface.h>
#include <psyrpc/remote/acceptorinterface.h>
#include <psyrpc/remote/server.h>
#include <psyrpc/remote/servicebase.h>
#include <psyrpc/remote/transferinterface.h>

namespace integration {

class SafeBuffer final {
public:
    using BufferType = std::vector<std::string>;

    class LockedBuffer final {
    public:
        LockedBuffer(BufferType &buffer, std::mutex &mutex) : m_buffer{&buffer}, m_mutex{&mutex}
        {
            m_mutex->lock();
        }
        ~LockedBuffer()
        {
            m_mutex->unlock();
        }

        const BufferType *operator->() const
        {
            return m_buffer;
        }
        BufferType *operator->()
        {
            return m_buffer;
        }
        const BufferType &operator*() const
        {
            return *m_buffer;
        }
        BufferType &operator*()
        {
            return *m_buffer;
        }

    private:
        BufferType *m_buffer = nullptr;
        std::mutex *m_mutex = nullptr;
    };

    LockedBuffer lock()
    {
        return {m_buffer, m_mutex};
    }

private:
    std::mutex m_mutex;
    BufferType m_buffer;
};

using ResultMsg = google::protobuf::Int32Value;
using ParameterMsg = google::protobuf::Int32Value;

namespace client {

/* generate by compiler */
class MyService final : public psyrpc::client::ServiceBase {
public:
    explicit MyService(psyrpc::client::ChannelInterface &channel) :
        psyrpc::client::ServiceBase{channel}
    {}

    /* rpc */
    nonstd::optional<psyrpc::client::Result<ResultMsg>>
    myCall(const ParameterMsg &paramMsg,
           const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max())
    {
        static constexpr auto methodId = 1;
        static constexpr auto customMethodId = 0;

        auto paramStream = psyrpc::client::stream::Parameter<const ParameterMsg>{};
        auto resultStream = call<ResultMsg, psyrpc::client::servicebase::result::unary>(
            serviceId, customServiceId, methodId, customMethodId, paramStream, waitFor);
        if (!resultStream || !paramStream.write(paramMsg, psyrpc::Eot{}))
            return {};
        return resultStream;
    }
    nonstd::optional<psyrpc::client::Result<ResultMsg>>
    myCallStreamingParam(psyrpc::client::stream::Parameter<const ParameterMsg> &paramStream,
                         const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max())
    {
        static constexpr auto methodId = 2;
        static constexpr auto customMethodId = 0;

        return call<ResultMsg, psyrpc::client::servicebase::result::unary>(
            serviceId, customServiceId, methodId, customMethodId, paramStream, waitFor);
    }
    nonstd::optional<psyrpc::client::stream::Result<ResultMsg>> myCallStreamingResult(
        const ParameterMsg &paramMsg,
        const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max())
    {
        static constexpr auto methodId = 3;
        static constexpr auto customMethodId = 0;

        auto paramStream = psyrpc::client::stream::Parameter<const ParameterMsg>{};
        auto resultStream = call<ResultMsg, psyrpc::client::servicebase::result::stream>(
            serviceId, customServiceId, methodId, customMethodId, paramStream, waitFor);
        if (!resultStream || !paramStream.write(paramMsg, psyrpc::Eot{}))
            return {};
        return resultStream;
    }
    nonstd::optional<psyrpc::client::stream::Result<ResultMsg>>
    myCallFullStreaming(psyrpc::client::stream::Parameter<const ParameterMsg> &paramStream,
                        const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max())
    {
        static constexpr auto methodId = 4;
        static constexpr auto customMethodId = 0;

        return call<ResultMsg, psyrpc::client::servicebase::result::stream>(
            serviceId, customServiceId, methodId, customMethodId, paramStream, waitFor);
    }

private:
    static constexpr auto serviceId = 1337;
    static constexpr auto customServiceId = 0;
};

class MyTransfer final : public psyrpc::client::TransferInterface {
public:
    MyTransfer(SafeBuffer &read, SafeBuffer &write) : m_buffer{read, write} {}

    bool write(const google::protobuf::MessageLite &message) override
    {
        auto bytes = std::string{};
        if (!message.SerializeToString(&bytes))
            return false;

        m_buffer.write.lock()->emplace_back(std::move(bytes));
        return true;
    }
    bool read(google::protobuf::MessageLite &message, const std::chrono::milliseconds) override
    {
        for (;;) {
            auto readBuffer = m_buffer.read.lock();
            if (readBuffer->empty())
                continue;
            const auto front = std::move(readBuffer->front());
            readBuffer->erase(std::begin(*readBuffer));
            return message.ParseFromString(front);
        }
    }

private:
    struct Buffer final {
        SafeBuffer &read;
        SafeBuffer &write;
    };
    Buffer m_buffer;
};

} // namespace client

namespace remote {

/* generate by compiler */
class MyServiceBase : public psyrpc::remote::ServiceBase<1337, 0> {
private:
    bool has(const psyrpc::proto::XxHashedSignature &method) override
    {
        switch (method.value()) {
            case 1:
                return method.customidtoavoidcollision() == 0;
            case 2:
                return method.customidtoavoidcollision() == 0;
            case 3:
                return method.customidtoavoidcollision() == 0;
            case 4:
                return method.customidtoavoidcollision() == 0;
        }
        return false;
    }

    bool call(const psyrpc::proto::XxHashedSignature &method, psyrpc::common::call::Interface &call,
              const std::chrono::milliseconds waitFor) override
    {
        assert(has(method));

        if (method.value() == 1) {
            auto parameterStream =
                psyrpc::remote::detail::stream::makeParameter<ParameterMsg>(call);
            auto resultStream = psyrpc::remote::detail::stream::makeResult<ResultMsg>(call);
            auto parameter = ParameterMsg{};
            if (!parameterStream.read(parameter, waitFor))
                return false;
            auto result = ResultMsg{};
            if (!myCall(parameter, result))
                return false;
            return resultStream.write(result, Eot{});
        }
        if (method.value() == 2) {
            auto result = ResultMsg{};
            auto resultStream = psyrpc::remote::detail::stream::makeResult<ResultMsg>(call);
            if (!myCall(psyrpc::remote::detail::stream::makeParameter<ParameterMsg>(call), result))
                return false;
            return resultStream.write(result, Eot{});
        }
        if (method.value() == 3) {
            auto parameterStream =
                psyrpc::remote::detail::stream::makeParameter<ParameterMsg>(call);
            auto resultStream = psyrpc::remote::detail::stream::makeResult<ResultMsg>(call);
            auto parameter = ParameterMsg{};
            if (!parameterStream.read(parameter, waitFor))
                return false;
            return myCall(parameter, std::move(resultStream));
        }
        if (method.value() == 4)
            return myCall(psyrpc::remote::detail::stream::makeParameter<ParameterMsg>(call),
                          psyrpc::remote::detail::stream::makeResult<ResultMsg>(call));

        return false;
    }

    virtual bool myCall(const ParameterMsg &paramMsg, ResultMsg &resultMsg) = 0;
    virtual bool myCall(psyrpc::remote::stream::Parameter<ParameterMsg> paramMsg,
                        ResultMsg &resultMsg) = 0;
    virtual bool myCall(ParameterMsg paramMsg,
                        psyrpc::remote::stream::Result<ResultMsg> resultMsg) = 0;
    virtual bool myCall(psyrpc::remote::stream::Parameter<ParameterMsg> paramMsg,
                        psyrpc::remote::stream::Result<ResultMsg> resultMsg) = 0;
};

class MyService final : public MyServiceBase {
public:
    bool finished() const
    {
        return m_finished;
    }

private:
    bool myCall(const ParameterMsg &paramMsg, ResultMsg &resultMsg) override
    {
        resultMsg = paramMsg;
        return true;
    }
    bool myCall(psyrpc::remote::stream::Parameter<ParameterMsg> paramMsg,
                ResultMsg &resultMsg) override
    {
        while (!paramMsg.eot()) {
            auto param = ParameterMsg{};
            if (!paramMsg.read(param))
                return false;
            resultMsg.set_value(resultMsg.value() + param.value() + 1);
        }

        return true;
    }
    bool myCall(ParameterMsg paramMsg, psyrpc::remote::stream::Result<ResultMsg> resultMsg) override
    {
        paramMsg.set_value(paramMsg.value() + 1);
        return resultMsg.write(paramMsg, Eot{});
    }
    bool myCall(psyrpc::remote::stream::Parameter<ParameterMsg> paramMsg,
                psyrpc::remote::stream::Result<ResultMsg> resultMsg) override
    {
        while (!paramMsg.eot()) {
            auto result = ResultMsg{};
            if (!paramMsg.read(result))
                return false;
            result.set_value(result.value() + 1);
            if (!resultMsg.write(result, Eot{}))
                return false;
        }
        m_finished = true;
        return true;
    }

    bool m_finished = false;
};

class MyTransfer final : public psyrpc::remote::TransferInterface {
public:
    MyTransfer(SafeBuffer &read, SafeBuffer &write) : m_buffer{read, write} {}

    void peek()
    {
        if (m_callDispatcher == nullptr) {
            return;
        }
        if (!m_buffer.read.lock()->empty()) {
            m_callDispatcher->dispatch(*this);
        }
    }

    bool write(const google::protobuf::MessageLite &message) override
    {
        auto bytes = std::string{};
        if (!message.SerializeToString(&bytes))
            return false;

        m_buffer.write.lock()->emplace_back(std::move(bytes));
        return true;
    }
    bool read(google::protobuf::MessageLite &message, std::chrono::milliseconds) override
    {
        for (;;) {
            auto readBuffer = m_buffer.read.lock();
            if (readBuffer->empty()) {
                continue;
            }
            const auto front = std::move(readBuffer->front());
            readBuffer->erase(std::begin(*readBuffer));
            return message.ParseFromString(front);
        }
    }
    void attach(psyrpc::remote::CallDispatcherInterface *callDispatcher) override
    {
        m_callDispatcher = callDispatcher;
    }

private:
    struct Buffer final {
        SafeBuffer &read;
        SafeBuffer &write;
    };
    Buffer m_buffer;
    psyrpc::remote::CallDispatcherInterface *m_callDispatcher = nullptr;
};

class MyAcceptor final : public psyrpc::remote::AcceptorInterface {
public:
    void assign(psyrpc::remote::ChannelManagerInterface &channelManager) override
    {
        m_channelManager = &channelManager;
    }

    /* communication interface dependent part */
    void onNew(psyrpc::remote::TransferInterface &transfer)
    {
        m_channelManager->onOpened(transfer);
    }
    void onClosed(psyrpc::remote::TransferInterface &transfer)
    {
        m_channelManager->onClosed(transfer);
    }

private:
    psyrpc::remote::ChannelManagerInterface *m_channelManager = nullptr;
};

} // namespace remote
} // namespace integration

void clientMain(integration::SafeBuffer &read, integration::SafeBuffer &write)
{
    auto transfer = integration::client::MyTransfer{read, write};
    auto channel = psyrpc::client::Channel{transfer};
    auto service = integration::client::MyService{channel};

    auto resultMsg = integration::ResultMsg{};
    auto parameterMsg = integration::ParameterMsg{};
    parameterMsg.set_value(1337);

    auto parameterMsgStream = psyrpc::client::stream::Parameter<const integration::ParameterMsg>{};
    const auto checkResult =
        [&resultMsg](nonstd::optional<psyrpc::client::Result<integration::ResultMsg>> result) {
            assert(result);
            assert(result->read(resultMsg));
        };
    const auto checkResultStream =
        [&resultMsg](
            nonstd::optional<psyrpc::client::stream::Result<integration::ResultMsg>> result) {
            assert(result);
            assert(result->read(resultMsg));
            assert(result->eot());
        };
    const auto checkResultParamStream =
        [&parameterMsgStream, &parameterMsg,
         &resultMsg](nonstd::optional<psyrpc::client::Result<integration::ResultMsg>> result) {
            assert(result);
            assert(parameterMsgStream.write(parameterMsg, psyrpc::Eot{}));
            assert(result->read(resultMsg));
        };
    const auto checkResultStreamParamStream =
        [&parameterMsgStream, &parameterMsg, &resultMsg](
            nonstd::optional<psyrpc::client::stream::Result<integration::ResultMsg>> result) {
            assert(result);
            assert(parameterMsgStream.write(parameterMsg, psyrpc::Eot{}));
            assert(result->read(resultMsg));
            assert(result->eot());
        };

    checkResult(service.myCall(parameterMsg));
    checkResultParamStream(service.myCallStreamingParam(parameterMsgStream));
    parameterMsgStream = {};
    checkResultStream(service.myCallStreamingResult(parameterMsg));
    checkResultStreamParamStream(service.myCallFullStreaming(parameterMsgStream));
}

void serverMain(integration::SafeBuffer &read, integration::SafeBuffer &write)
{
    auto acceptor = integration::remote::MyAcceptor{};
    auto server = psyrpc::remote::Server{};
    auto service = integration::remote::MyService{};

    server.add(service);
    acceptor.assign(server);

    auto transfer = integration::remote::MyTransfer{read, write};
    acceptor.onNew(transfer);

    while (!service.finished()) {
        transfer.peek();
    }
}

int main()
{
    integration::SafeBuffer clientReadServerWrite;
    integration::SafeBuffer clientWriteServerRead;

    auto client =
        std::thread{clientMain, std::ref(clientReadServerWrite), std::ref(clientWriteServerRead)};
    auto server =
        std::thread{serverMain, std::ref(clientWriteServerRead), std::ref(clientReadServerWrite)};

    client.join();
    server.join();
    return EXIT_SUCCESS;
}
