CppApplication {
    condition: psyRPCbuildconfig.buildExamples
    qbs.profile: "conan"

    files: [
        "fullexample.cpp",
    ]

    Depends { name: "psyRPC.cpp" }
    Depends { name: "psyRPCbuildconfig" }
}
