import "../../share/qbs/modules/psyrpc_generator/psyrpc_generator.js" as HelperFunctions

StaticLibrary {
    condition: psyRPCbuildconfig.buildExamples
    qbs.profile: "conan"

    readonly property string _outputDir: buildDirectory + "/protobuf"

    type: base.concat(["psyrpc.client.hpp", "psyrpc.remote.hpp"])
    protobuf.cpp.importPaths: sourceDirectory

    Group {
        name: "proto"
        files: [ "example.proto" ]
        fileTags: ["psyrpc.client.input", "psyrpc.remote.input", "protobuf.input"]
    }

    Rule {
        inputs: ["psyrpc.client.input", "psyrpc.remote.input"]
        inputsFromDependencies: "application"
        outputFileTags: ["hpp", "psyrpc.client.hpp", "psyrpc.remote.hpp", "cpp"]
        outputArtifacts: {
            var module = {
                outputDir: product._outputDir,
                importPaths: [product.sourceDirectory]
            };

            var outputDir = HelperFunctions.getOutputDir(module, input);
            var result = [];
            if (input.fileTags.contains("psyrpc.client.input")) {
                result.push(
                        HelperFunctions.cppArtifact(outputDir, input, ["hpp", "psyrpc.client.hpp"],
                                                    ".psyrpc_client.h"),
                        HelperFunctions.cppArtifact(outputDir, input, "cpp", ".psyrpc_client.cpp"));
            }
            if (input.fileTags.contains("psyrpc.remote.input")) {
                result.push(
                        HelperFunctions.cppArtifact(outputDir, input, ["hpp", "psyrpc.remote.hpp"],
                                                    ".psyrpc_remote.h"),
                        HelperFunctions.cppArtifact(outputDir, input, "cpp", ".psyrpc_remote.cpp"));
            }
            return result;
        }

        prepare: {
            var module = {
                outputDir: product._outputDir,
                importPaths: [product.sourceDirectory],
                compilerPath: product.protobuf.cpp.compilerPath
            };

            var result = HelperFunctions.doPrepare(
                        module, product, input, outputs, "psyRPC",
                        product.psyRPC.generator.cpp.pluginPath + "/protoc-gen-psyRPC");
            return result;
        }
    }

    Depends { name: "psyRPC.generator.cpp" }
    Depends { name: "psyRPCbuildconfig" }

    Export {
        cpp.includePaths: exportingProduct._outputDir

        Depends { name: "cpp" }
        Depends { name: "psyRPC.cpp" }
    }
}
