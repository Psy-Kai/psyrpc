CppApplication {
    condition: psyRPCbuildconfig.buildExamples && qbs.targetOS.contains("unix")
    qbs.profile: "conan"

    files: [
        "remoteexample.cpp",
    ]

    Depends { name: "protoexample" }
    Depends { name: "psyRPCbuildconfig" }
}
