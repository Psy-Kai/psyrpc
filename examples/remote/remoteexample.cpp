#include <array>
#include <cinttypes>

#include <fts.h>
#include <poll.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/utsname.h>

#include <psyrpc/remote/acceptorinterface.h>
#include <psyrpc/remote/server.h>
#include <example.psyrpc_remote.h>

namespace example {
namespace remote {

class Transfer final : public psyrpc::remote::TransferInterface {
public:
    explicit Transfer(int socket) : m_socket{socket} {}

    void peek()
    {
        static constexpr auto event = POLLIN;
        pollfd fd{m_socket, event, {}};
        const auto result = poll(&fd, 1, 0);
        if (result == 0) {
            return;
        }
        if (result == -1) {
            throw std::runtime_error{"poll error"};
        }

        if (fd.revents & event) {
            if (m_callDispatcher != nullptr) {
                m_callDispatcher->dispatch(*this);
            }
        } else {
            throw std::runtime_error{"poll unused event"};
        }
    }

private:
    static constexpr auto headerSize = sizeof(uint32_t) * 2;
    static constexpr auto charConvHex = 16;

    bool write(const google::protobuf::MessageLite &message) override
    {
        const auto data = message.SerializeAsString();
        if (data.empty()) {
            return false;
        }
        if (data.size() >= std::numeric_limits<uint32_t>::max()) {
            return false;
        }
        auto header = std::string(headerSize + 1, '0');
        std::snprintf(&header.front(), header.size(), "%08" PRIx32,
                      static_cast<uint32_t>(data.size()));
        return (send(m_socket, header.data(), headerSize, MSG_NOSIGNAL) > 0) &&
            (send(m_socket, data.data(), data.size(), MSG_NOSIGNAL) > 0);
    }
    bool read(google::protobuf::MessageLite &message, const std::chrono::milliseconds) override
    {
        auto header = std::string(headerSize, '0');
        auto recvRes = recv(m_socket, &header.front(), header.size(), MSG_WAITALL);
        if (recvRes < 0) {
            return false;
        }
        auto dataSize = uint32_t{};
        if (std::sscanf(header.data(), "%08" PRIx32, &dataSize) < 0) {
            return false;
        }
        auto data = std::string(static_cast<std::size_t>(dataSize), '\0');
        recvRes = recv(m_socket, &data.front(), data.size(), MSG_WAITALL);
        if (recvRes < 0) {
            return false;
        }
        return message.ParseFromString(data);
    }
    void attach(psyrpc::remote::CallDispatcherInterface *callDispatcher) override
    {
        m_callDispatcher = callDispatcher;
    }

    int m_socket;
    psyrpc::remote::CallDispatcherInterface *m_callDispatcher = nullptr;
};

class Acceptor final : public psyrpc::remote::AcceptorInterface {
public:
    void listen()
    {
        assert(m_channelManager != nullptr);
        std::puts("start listen for incomming connections");
        sockaddr_in address;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_family = AF_INET;
        constexpr auto tcpPort = 13337;
        address.sin_port = htons(tcpPort);
        if (bind(m_socket, reinterpret_cast<const sockaddr *>(&address), sizeof(address)) < 0) {
            throw std::runtime_error{"failed to bind to socket"};
        }
        if (::listen(m_socket, 0) < 0) {
            throw std::runtime_error{"failed to listen on socket"};
        }
        auto clientSocket = accept(m_socket, nullptr, nullptr);
        if (clientSocket < 0) {
            throw std::runtime_error{"failed to accept connection"};
        }
        auto transfer = std::unique_ptr<Transfer>(new Transfer{clientSocket});
        m_channelManager->onOpened(*transfer);
        m_transfers.emplace_back(std::move(transfer));
    }
    void assign(psyrpc::remote::ChannelManagerInterface &channelManager) override
    {
        m_channelManager = &channelManager;
    }

    std::vector<std::unique_ptr<Transfer>> &transfers()
    {
        return m_transfers;
    }

private:
    int m_socket = socket(AF_INET, SOCK_STREAM, 0);
    std::vector<std::unique_ptr<Transfer>> m_transfers;
    psyrpc::remote::ChannelManagerInterface *m_channelManager = nullptr;
};

class Foobar final : public example::proto::FoobarBase {
private:
    Rpc uname(const proto::Empty &, proto::String &resultMsg) override
    {
        utsname buf;
        if (::uname(&buf) < 0) {
            return Rpc::Success;
        }
        resultMsg.set_value(buf.sysname);
        return Rpc::Success;
    }
    Rpc ls(const proto::Empty &,
           psyrpc::remote::stream::Result<proto::String> resultStream) override
    {
        const auto compare = [](const FTSENT **lhs, const FTSENT **rhs) {
            return std::strcmp((*lhs)->fts_name, (*rhs)->fts_name);
        };

        using PathBuffer = std::array<char, 0x100>;
        using PathList = std::array<char *const, 0x10>;
        auto path = PathBuffer{"/tmp"};
        auto paths = PathList{path.data()};
        auto *filesystem = fts_open(paths.data(), FTS_NOCHDIR | FTS_COMFOLLOW, compare);
        while (auto *parent = fts_read(filesystem)) {
            auto entryName = proto::String{};
            entryName.set_value(parent->fts_name);
            if (!resultStream.write(entryName)) {
                return Rpc::Failure;
            }
        }
        if (!resultStream.write({}, psyrpc::Eot{})) {
            return Rpc::Failure;
        }
        return Rpc::Success;
    }
    Rpc upload(psyrpc::remote::stream::Parameter<proto::File> paramStream,
               proto::Boolean &resultMsg) override
    {
        resultMsg.set_value(false);
        auto fileMsg = proto::File{};
        if (!paramStream.read(fileMsg)) {
            return Rpc::Failure;
        }
        if (fileMsg.file_case() != proto::File::FileCase::kName) {
            return Rpc::Success;
        }
        const auto filePath = "/tmp/remote_" + fileMsg.name();
        std::unique_ptr<FILE, decltype(&std::fclose)> file{std::fopen(filePath.data(), "w+"),
                                                           &std::fclose};
        if (file == nullptr) {
            return Rpc::Success;
        }
        while (!paramStream.eot()) {
            if (!paramStream.read(fileMsg)) {
                return Rpc::Failure;
            }
            if (fileMsg.file_case() != proto::File::FileCase::kData) {
                return Rpc::Success;
            }
            if (std::fwrite(fileMsg.data().data(), 1, fileMsg.data().size(), file.get()) !=
                fileMsg.data().size()) {
                return Rpc::Success;
            }
        }
        std::fflush(file.get());
        resultMsg.set_value(true);
        return Rpc::Success;
    }
    Rpc download(const proto::String &paramMsg,
                 psyrpc::remote::stream::Result<proto::File> resultStream) override
    {
        const auto filePath = "/tmp/remote_" + paramMsg.value();
        std::unique_ptr<FILE, decltype(&std::fclose)> file{std::fopen(filePath.data(), "r"),
                                                           &std::fclose};
        if (file == nullptr) {
            return Rpc::Failure;
        }
        auto buf = std::array<char, 0x10>{};
        auto fileMsg = proto::File{};
        fileMsg.set_name(filePath);
        if (!resultStream.write(fileMsg)) {
            return Rpc::Failure;
        }
        while (const auto consumed = std::fread(buf.data(), 1, buf.size(), file.get())) {
            fileMsg.set_data(std::string{std::begin(buf), std::begin(buf) + consumed});
            if (!resultStream.write(fileMsg)) {
                return Rpc::Failure;
            }
        }
        return resultStream.write({}, psyrpc::Eot{}) ? Rpc::Success : Rpc::Failure;
    }
};

} // namespace remote
} // namespace example

int main()
{
    try {
        auto acceptor = example::remote::Acceptor{};
        auto server = psyrpc::remote::Server{};
        auto foobar = example::remote::Foobar{};
        server.add(foobar);
        acceptor.assign(server);
        acceptor.listen();
        for (;;) {
            for (auto &transfer : acceptor.transfers()) {
                transfer->peek();
            }
        }
    } catch (const std::exception &e) {
        std::puts(e.what());
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
