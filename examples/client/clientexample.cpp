#include <cinttypes>

#include <arpa/inet.h>
#include <sys/socket.h>

#include <nonstd/string_view.hpp>

#include <psyrpc/client/channel.h>
#include <example.psyrpc_client.h>

namespace example {
namespace client {

class Transfer final : public psyrpc::client::TransferInterface {
public:
    explicit Transfer(nonstd::string_view tcpAddress)
    {
        sockaddr_in address;
        std::printf("try to connect to %s\n", tcpAddress.data());
        if (inet_pton(AF_INET, tcpAddress.data(), &address.sin_addr.s_addr) < 0) {
            throw std::runtime_error{"failed to parse tcp address"};
        }
        address.sin_family = AF_INET;
        constexpr auto tcpPort = 13337;
        address.sin_port = htons(tcpPort);
        if (connect(m_socket, reinterpret_cast<const sockaddr *>(&address), sizeof(address)) < 0) {
            throw std::runtime_error{"failed to connect to server"};
        }
    }

private:
    static constexpr auto headerSize = sizeof(uint32_t) * 2;
    static constexpr auto charConvHex = 16;

    bool write(const google::protobuf::MessageLite &message) override
    {
        const auto data = message.SerializeAsString();
        if (data.empty()) {
            return false;
        }
        if (data.size() >= std::numeric_limits<uint32_t>::max()) {
            return false;
        }
        auto header = std::string(headerSize + 1, '0');
        std::snprintf(&header.front(), header.size(), "%08" PRIx32,
                      static_cast<uint32_t>(data.size()));
        return (send(m_socket, header.data(), headerSize, MSG_NOSIGNAL) > 0) &&
            (send(m_socket, data.data(), data.size(), MSG_NOSIGNAL) > 0);
    }
    bool read(google::protobuf::MessageLite &message, const std::chrono::milliseconds) override
    {
        auto header = std::string(headerSize, '0');
        if (recv(m_socket, &header.front(), header.size(), MSG_WAITALL) < 0) {
            return false;
        }
        auto dataSize = uint32_t{};
        if (std::sscanf(header.data(), "%08" PRIx32, &dataSize) < 0) {
            return false;
        }
        auto data = std::string(static_cast<std::size_t>(dataSize), '\0');
        if (recv(m_socket, &data.front(), data.size(), MSG_WAITALL) < 0) {
            return false;
        }
        return message.ParseFromString(data);
    }

    int m_socket = socket(AF_INET, SOCK_STREAM, 0);
};

} // namespace client
} // namespace example

int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::puts("wrong amount of arguments: missing tcp address");
        return EXIT_FAILURE;
    }

    try {
        constexpr auto tcpAddressArgument = 1;
        auto transfer = example::client::Transfer{argv[tcpAddressArgument]};
        auto channel = psyrpc::client::Channel{transfer};
        auto foobar = example::proto::Foobar{channel};

        const auto uname = [&] {
            std::puts("call uname");
            auto uname = foobar.uname({});
            if (!uname) {
                throw std::runtime_error{"failed to call Foobar::uname"};
            }
            example::proto::String name;
            if (!uname->read(name)) {
                throw std::runtime_error{"failed to receive filename"};
            }
            std::puts(name.value().data());
        };
        const auto ls = [&] {
            std::puts("call ls");
            auto file = foobar.ls({});
            if (!file) {
                throw std::runtime_error{"failed to call Foobar::ls"};
            }
            while (!file->eot()) {
                example::proto::String fileName;
                if (!file->read(fileName)) {
                    throw std::runtime_error{"failed to receive filename"};
                }
                const auto last = [&] { return file->eot(); };
                if (!last()) {
                    std::puts(fileName.value().data());
                }
            }
        };
        const auto upload = [&] {
            std::puts("call upload");
            auto fileStream = psyrpc::client::stream::Parameter<const example::proto::File>{};
            auto resultStream = foobar.upload(fileStream);
            if (!resultStream) {
                throw std::runtime_error{"failed to call Foobar::upload"};
            }
            auto file = example::proto::File{};
            file.set_name("stub_filename.txt");
            if (!fileStream.write(file)) {
                throw std::runtime_error{"failed to stream filename"};
            }
            constexpr auto linesToWrite = 10;
            for (auto i = 1; i <= linesToWrite; ++i) {
                file.set_data("Hello World\n");
                if (!(i < linesToWrite ? fileStream.write(file) :
                                         fileStream.write(file, psyrpc::Eot{}))) {
                    throw std::runtime_error{"failed to stream file content"};
                }
            }
            auto success = example::proto::Boolean{};
            if (!resultStream->read(success)) {
                throw std::runtime_error{"failed to recevie result"};
            }
            if (!success.value()) {
                throw std::runtime_error{"upload failed"};
            }
        };
        const auto download = [&] {
            std::puts("call download");
            auto fileName = example::proto::String{};
            fileName.set_value("stub_filename.txt");
            auto fileStream = foobar.download(fileName);
            if (!fileStream) {
                throw std::runtime_error{"failed to call Foobar::download"};
            }
            const auto filePath = "/tmp/client_" + fileName.value();
            std::unique_ptr<FILE, decltype(&std::fclose)> file{std::fopen(filePath.data(), "w+"),
                                                               std::fclose};
            if (file == nullptr) {
                throw std::runtime_error{"failed to open local file for write"};
            }
            while (!fileStream->eot()) {
                auto fileMsg = example::proto::File{};
                if (!fileStream->read(fileMsg)) {
                    throw std::runtime_error{"failed to receive file"};
                }
                switch (fileMsg.file_case()) {
                    case example::proto::File::FileCase::kName:
                        std::printf("downloaded file name: %s\n", fileMsg.name().data());
                        break;
                    case example::proto::File::FileCase::kData:
                        if (std::fwrite(fileMsg.data().data(), 1, fileMsg.data().size(),
                                        file.get()) != fileMsg.data().size()) {
                            throw std::runtime_error{"failed to write data to local file"};
                        }
                        break;
                    case example::proto::File::FileCase::FILE_NOT_SET:
                        break;
                }
            }
        };

        uname();
        ls();
        upload();
        download();

    } catch (const std::exception &e) {
        std::puts(e.what());
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
