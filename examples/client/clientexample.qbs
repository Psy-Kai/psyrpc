CppApplication {
    condition: psyRPCbuildconfig.buildExamples && qbs.targetOS.contains("unix")
    qbs.profile: "conan"

    files: [
        "clientexample.cpp",
    ]

    Depends { name: "protoexample" }
    Depends { name: "psyRPCbuildconfig" }
}
