import qbs.File
import qbs.FileInfo
import qbs.Probes
import qbs.TextFile

Project {
    readonly property path conanGeneratorsFolder: undefined
    readonly property path _conanGeneratorsFolder: {
        if (!conanGeneratorsFolder){
            throw "conanGeneratorsFolder not set";
        }
        const absoluteConanGeneratorsFolder =
            FileInfo.isAbsolutePath(conanGeneratorsFolder) ?
                conanGeneratorsFolder : FileInfo.joinPaths(sourceDirectory, conanGeneratorsFolder);
        if (!File.exists(absoluteConanGeneratorsFolder)) {
            throw 'conanGeneratorsFolder "' + absoluteConanGeneratorsFolder + '" does not exists. Did you forget to call "conan install"?';
        }
        return absoluteConanGeneratorsFolder;
    }

    readonly property var _customConanBuildInfoJson: customConanBuildInfoProbe.json

    minimumQbsVersion: "1.23.0"

    references: [
        FileInfo.joinPaths(_conanGeneratorsFolder, "conan_toolchain_profile.qbs"),
        "profiles/profiles.qbs",

        "examples",
        "share/share.qbs",
        "src/src.qbs",
        "tests",
    ]

    qbsSearchPaths: ["qbs"]
    qbsModuleProviders: ["qbspkgconfig"]

    Product {
        name: "misc"
        condition: false
        files: [
            ".gitlab-ci.yml",
            "conanfile.py",
            "README.md",
        ]
    }

    Probe {
        id: customConanBuildInfoProbe
        readonly property path conanGeneratorsFolder: _conanGeneratorsFolder
        property var json

        configure: {
            const buildInfoJsonFilePath = FileInfo.joinPaths(conanGeneratorsFolder,
                                                             "customconanbuildinfo.json");
            if (!File.exists(buildInfoJsonFilePath)) {
                found = false;
                throw "No customconanbuildinfo.json has been generated."
            }

            var jsonFile = new TextFile(buildInfoJsonFilePath, TextFile.ReadOnly);
            json = JSON.parse(jsonFile.readAll());
            jsonFile.close();
            found = true;
        }
    }
}
