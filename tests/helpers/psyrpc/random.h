#pragma once

#include <random>

namespace psyrpc {

class Random {
public:
    int operator()()
    {
        return distribution(generator);
    }

private:
    std::random_device device;
    std::default_random_engine generator{device()};
    std::uniform_int_distribution<> distribution;
};

} // namespace psyrpc
