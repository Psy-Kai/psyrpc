#include <psyrpc/proto/channel.pb.h>

namespace psyrpc {

inline bool operator==(const proto::Package &lhs, const proto::Package &rhs)
{
    return lhs.index() == rhs.index() && (lhs.eot() ? rhs.eot() : !rhs.eot()) &&
        lhs.has_data() == rhs.has_data() && lhs.data().value() == rhs.data().value();
}

inline bool operator==(const proto::XxHashedSignature &lhs, const proto::XxHashedSignature &rhs)
{
    return lhs.value() == rhs.value() &&
        lhs.customidtoavoidcollision() == rhs.customidtoavoidcollision();
}

inline bool operator==(const proto::Call::Init &lhs, const proto::Call::Init &rhs)
{
    return lhs.service() == rhs.service() && lhs.method() == rhs.method();
}

inline bool operator==(const proto::Call &lhs, const proto::Call &rhs)
{
    if (lhs.call_case() != rhs.call_case() || lhs.id() != rhs.id())
        return false;
    switch (lhs.call_case()) {
        case proto::Call::CallCase::kInit:
            return lhs.init() == rhs.init();
        case proto::Call::CallCase::kParameter:
            return lhs.parameter() == rhs.parameter();
        case proto::Call::CallCase::kCancel:
            return lhs.cancel() ? rhs.cancel() : !rhs.cancel();
        default:
            return false;
    }
}

inline bool operator==(const proto::Result &lhs, const proto::Result &rhs)
{
    return lhs.callid() == rhs.callid() && (lhs.init() ? rhs.init() : !rhs.init()) &&
        lhs.result() == rhs.result();
}

inline bool operator==(const proto::Rpc &lhs, const proto::Rpc &rhs)
{
    if (lhs.magicconst() != rhs.magicconst() || lhs.flowcontrol() != rhs.flowcontrol())
        return false;
    if (lhs.rpc_case() != rhs.rpc_case())
        return false;
    switch (lhs.rpc_case()) {
        case proto::Rpc::RpcCase::kCall:
            return lhs.call() == rhs.call();
        case proto::Rpc::RpcCase::kResult:
            return lhs.result() == rhs.result();
        case proto::Rpc::RpcCase::kError:
            return lhs.error() == rhs.error();
        default:
            return false;
    }
}
namespace proto {

template <class T>
bool matcherImpl(const google::protobuf::MessageLite &lhs, const google::protobuf::MessageLite &rhs)
{
    const auto *value = dynamic_cast<const T *>(&lhs);
    const auto *expected = dynamic_cast<const T *>(&rhs);
    if (!value || !expected)
        return false;
    return *value == *expected;
}

} // namespace proto
} // namespace psyrpc
