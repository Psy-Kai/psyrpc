StaticLibrary {
    condition: psyRPCbuildconfig.buildTests
    qbs.profile: "conan"

    files: [
        "psyrpc/proto/operators.h",
        "psyrpc/random.h",
    ]

    Depends { name: "psyRPC.cpp" }
    Depends { name: "psyRPCbuildconfig" }

    Export {
        cpp.includePaths: exportingProduct.sourceDirectory

        Depends { name: "cpp" }
        Depends { name: "psyRPC.cpp" }
    }
}
