#pragma once

#include <gmock/gmock.h>

#include <psyrpc/common/call/interface.h>
#include <psyrpc/common/types.h>
#include <psyrpc/proto/package.pb.h>

namespace mock {

class Call : public psyrpc::common::call::Interface {
public:
    virtual ~Call() = default;
    MOCK_METHOD(bool, read, (psyrpc::proto::Package &, const std::chrono::milliseconds),
                (noexcept, override));
    MOCK_METHOD(bool, write, (psyrpc::proto::Package &&), (noexcept, override));
};

} // namespace mock
