#pragma once

#include <gmock/gmock.h>

#include <psyrpc/remote/calldispatcherinterface.h>

namespace mock {
namespace remote {

class CallDispatcher : public psyrpc::remote::CallDispatcherInterface {
public:
    MOCK_METHOD(void, dispatch,
                (psyrpc::remote::TransferInterface &, const std::chrono::milliseconds),
                (noexcept, override));
};

} // namespace remote
} // namespace mock
