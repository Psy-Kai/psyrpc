#pragma once

#include <gmock/gmock.h>

#include <psyrpc/remote/transferinterface.h>

namespace mock {
namespace remote {

class Transfer : public psyrpc::remote::TransferInterface {
public:
    MOCK_METHOD(bool, write, (const google::protobuf::MessageLite &), (noexcept, override));
    MOCK_METHOD(bool, read, (google::protobuf::MessageLite &, const std::chrono::milliseconds),
                (noexcept, override));
    MOCK_METHOD(void, attach, (psyrpc::remote::CallDispatcherInterface *), (override));
};

} // namespace remote
} // namespace mock
