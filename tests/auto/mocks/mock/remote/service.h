#pragma once

#include <gmock/gmock.h>

#include <psyrpc/remote/serviceinterface.h>

namespace mock {
namespace remote {

class Service : public psyrpc::remote::ServiceInterface {
public:
    MOCK_METHOD(bool, has, (const psyrpc::proto::XxHashedSignature &method), (override));
    MOCK_METHOD(bool, call,
                (const psyrpc::proto::XxHashedSignature &method,
                 psyrpc::common::call::Interface &call, const std::chrono::milliseconds waitFor),
                (override));
    MOCK_METHOD(psyrpc::ServiceId, id, (), (const, override));
    MOCK_METHOD(psyrpc::ServiceId, customId, (), (const, override));
};

} // namespace remote
} // namespace mock
