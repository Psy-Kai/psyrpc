#pragma once

#include <gmock/gmock.h>

#include <psyrpc/remote/channelinterface.h>

namespace mock {
namespace remote {

class Channel : public psyrpc::remote::ChannelInterface {
public:
    MOCK_METHOD(bool, write, (psyrpc::proto::Result &&), (override));
    MOCK_METHOD(void, write, (psyrpc::proto::Rpc::Error), (override));
    MOCK_METHOD(bool, read, (psyrpc::proto::Call &, const std::chrono::milliseconds), (override));
    MOCK_METHOD(const psyrpc::remote::TransferInterface &, transfer, (), (const, override));
};

} // namespace remote
} // namespace mock
