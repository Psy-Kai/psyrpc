#pragma once

#include <gmock/gmock.h>

#include <psyrpc/impl/ascendingnumbergenerator.h>

namespace mock {

class AscendingNumberGenerator : public psyrpc::ascendingnumbergenerator::Interface {
public:
    MOCK_METHOD(psyrpc::ascendingnumbergenerator::ValueType, next, (), (noexcept, override));
};

} // namespace mock
