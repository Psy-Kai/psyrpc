#pragma once

#include <gmock/gmock.h>

#include <psyrpc/client/callinterface.h>

namespace mock {
namespace client {

class Call : public psyrpc::client::CallInterface {
public:
    MOCK_METHOD(bool, init,
                (const psyrpc::ServiceId, const psyrpc::ServiceId, const psyrpc::MethodId,
                 const psyrpc::MethodId, const std::chrono::milliseconds),
                (noexcept, override));
    MOCK_METHOD(bool, read, (psyrpc::proto::Package &, const std::chrono::milliseconds),
                (noexcept, override));
    MOCK_METHOD(bool, write, (psyrpc::proto::Package &&), (noexcept, override));
};

} // namespace client
} // namespace mock
