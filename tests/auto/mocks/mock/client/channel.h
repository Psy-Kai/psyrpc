#pragma once

#include <gmock/gmock.h>

#include <psyrpc/client/channelinterface.h>

namespace mock {
namespace client {

class Channel : public psyrpc::client::ChannelInterface {
public:
    MOCK_METHOD(bool, write, (psyrpc::proto::Call &&), (noexcept, override));
    MOCK_METHOD(bool, read, (psyrpc::proto::Result &, const std::chrono::milliseconds),
                (noexcept, override));
};

} // namespace client
} // namespace mock
