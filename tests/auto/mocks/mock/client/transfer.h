#pragma once

#include <gmock/gmock.h>

#include <psyrpc/client/transferinterface.h>

namespace mock {
namespace client {

class Transfer : public psyrpc::client::TransferInterface {
public:
    MOCK_METHOD(bool, write, (const google::protobuf::MessageLite &), (noexcept, override));
    MOCK_METHOD(bool, read, (google::protobuf::MessageLite &, const std::chrono::milliseconds),
                (noexcept, override));
};

} // namespace client
} // namespace mock
