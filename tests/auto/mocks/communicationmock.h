#pragma once

#include <gmock/gmock.h>
#include <psyrpc/communicationinterface.h>

namespace psyrpc {
namespace remote {
class CommunicationMock : public CommunicationInterface {
public:
    MOCK_METHOD(bool, send, (const ConnectionToken &token, std::string message), (override));
    MOCK_METHOD(void, registerObserver, (std::weak_ptr<ObserverInterface> observer), (override));
    MOCK_METHOD(void, removeObserver, (const ObserverInterface &observer), (override));
};
} // namespace remote
namespace client {
class CommunicationMock : public CommunicationInterface {
public:
    MOCK_METHOD(bool, send, (std::string message), (override));
    MOCK_METHOD(void, registerObserver, (std::weak_ptr<ObserverInterface> observer), (override));
    MOCK_METHOD(void, removeObserver, (const ObserverInterface &observer), (override));
};
} // namespace client
} // namespace psyrpc
