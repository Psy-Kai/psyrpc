StaticLibrary {
    condition: psyRPCbuildconfig.buildTests
    qbs.profile: "conan"
    files: [
        "mock/**/*.h",
        "communicationmock.h",
        "servicemock.h",
    ]

    Depends { name: "gmock" }
    Depends { name: "psyRPC.cpp" }
    Depends { name: "psyRPCbuildconfig" }
    Export {
        cpp.includePaths: exportingProduct.sourceDirectory

        Depends { name: "cpp" }
        Depends { name: "gmock" }
        Depends { name: "psyRPC.cpp" }
    }
}
