#pragma once

#include <gmock/gmock.h>
#include <psyrpc/serviceinterface.h>

namespace psyrpc {

class ServiceMock : public ServiceInterface {
public:
    MOCK_METHOD(const std::string &, uri, (), (const, override));
    MOCK_METHOD(void, callMethod,
                (std::size_t callId, std::size_t methodIndex, const proto::Package &parameter),
                (override));
    MOCK_METHOD(const google::protobuf::ServiceDescriptor *, descriptor, (), (const, override));
};

} // namespace psyrpc
