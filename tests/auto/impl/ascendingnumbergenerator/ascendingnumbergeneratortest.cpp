#include <gtest/gtest.h>

#include <psyrpc/impl/ascendingnumbergenerator.h>

namespace psyrpc {

struct FlowControlGenerator : testing::Test {
    ascendingnumbergenerator::ValueType value =
        std::rand() % std::numeric_limits<ascendingnumbergenerator::ValueType>::max();
    ascendingnumbergenerator::AscendingNumberGenerator generator{value};
};

TEST_F(FlowControlGenerator, next)
{
    ASSERT_EQ(generator.next(), value + 1);
}

TEST_F(FlowControlGenerator, nextAndRollover)
{
    value = -1;
    ASSERT_EQ(generator.next(), 1);
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
