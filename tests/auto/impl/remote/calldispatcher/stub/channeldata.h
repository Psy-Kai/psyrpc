#include <atomic>

#include <psyrpc/remote/channelinterface.h>

namespace psyrpc {
namespace stub {

struct ChannelData {
    explicit ChannelData(remote::ChannelInterface &channel) : channel{channel} {}
    ChannelData(ChannelData &&other) :
        channel{other.channel}, hasActiveCall{other.hasActiveCall.load()}
    {}
    ChannelData &operator=(ChannelData &&other)
    {
        if (this == &other) {
            return *this;
        }
        channel = other.channel;
        hasActiveCall = other.hasActiveCall.load();
        return *this;
    }
    remote::ChannelInterface &channel;
    std::atomic<bool> hasActiveCall{false};
};

} // namespace stub
} // namespace psyrpc
