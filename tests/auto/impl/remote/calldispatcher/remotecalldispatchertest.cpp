#include <mock/remote/channel.h>
#include <mock/remote/service.h>
#include <mock/remote/transfer.h>
#include <psyrpc/impl/remote/calldispatcher.h>

namespace psyrpc {
namespace mock {

struct ChannelFinder {
    static inline ChannelFinder *self = nullptr;
    ChannelFinder()
    {
        assert(self == nullptr);
        self = this;
    }
    virtual ~ChannelFinder()
    {
        self = nullptr;
    }

    MOCK_METHOD(impl::remote::ChannelData *, static_find,
                (impl::remote::container::ChannelData::iterator,
                 impl::remote::container::ChannelData::iterator, remote::TransferInterface &));

    template <class TInputIterator>
    static impl::remote::ChannelData *find(TInputIterator begin, TInputIterator end,
                                           remote::TransferInterface &transfer)
    {
        return self->static_find(begin, end, transfer);
    }
};

struct Dispatcher {
    MOCK_METHOD(void, dispatch,
                (impl::remote::calldispatcher::adapter::ChannelData<impl::remote::ChannelData>,
                 const std::chrono::milliseconds));
};

} // namespace mock

struct RemoteCallDispatcher : public testing::Test {
    void SetUp() override
    {
        channels.emplace_back(stub.transfer);
        channels.emplace_back(stub.transfer);
    }

    struct {
        testing::StrictMock<mock::ChannelFinder> channelFinder;
        testing::StrictMock<mock::Dispatcher> dispatcher;
    } mock;
    struct {
        testing::StrictMock<::mock::remote::Transfer> transfer;
    } stub;

    testing::InSequence seq;
    std::chrono::milliseconds waitFor{std::rand()};
    impl::remote::container::Services services;
    impl::remote::container::ChannelData channels;
    impl::remote::calldispatcher::CallDispatcher<mock::ChannelFinder, mock::Dispatcher>
        callDispatcher{mock.dispatcher, channels};
};

TEST_F(RemoteCallDispatcher, dispatchButNoChannelFoundForTransfer)
{
    EXPECT_CALL(mock.channelFinder,
                static_find(std::begin(channels), std::end(channels), testing::Ref(stub.transfer)))
        .WillOnce(testing::Return(nullptr));
    callDispatcher.dispatch(stub.transfer, waitFor);
}

TEST_F(RemoteCallDispatcher, dispatch)
{
    auto *channelData = &channels.front();
    EXPECT_CALL(mock.channelFinder,
                static_find(std::begin(channels), std::end(channels), testing::Ref(stub.transfer)))
        .WillOnce(testing::Return(channelData));
    EXPECT_CALL(mock.dispatcher, dispatch(testing::_, waitFor));
    callDispatcher.dispatch(stub.transfer, waitFor);
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
