#include <psyrpc/impl/remote/calldispatcher.h>

#include <mock/remote/channel.h>
#include <mock/remote/service.h>
#include <mock/remote/transfer.h>

#include "stub/channeldata.h"

namespace psyrpc {

struct RemoteCallDispatcherDispatcher : testing::Test {
    using AdaptedChannelData =
        impl::remote::calldispatcher::adapter::ChannelData<stub::ChannelData>;

    struct {
        testing::StrictMock<::mock::remote::Service> service;
        testing::StrictMock<::mock::remote::Channel> channel;
    } mock;

    testing::InSequence seq;
    stub::ChannelData channelData{mock.channel};
    std::chrono::milliseconds waitFor{std::rand()};
    impl::remote::container::Services services;
    impl::remote::calldispatcher::Dispatcher dispatcher{services};
};

TEST_F(RemoteCallDispatcherDispatcher, dispatchButHasActiveCall)
{
    channelData.hasActiveCall = true;
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_TRUE(channelData.hasActiveCall);
}

TEST_F(RemoteCallDispatcherDispatcher, dispatchButChannelReadFails)
{
    EXPECT_CALL(mock.channel, read(testing::_, waitFor)).WillOnce(testing::Return(false));
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_TRUE(channelData.hasActiveCall);
}

TEST_F(RemoteCallDispatcherDispatcher, dispatchButReceivedNonInitMessage)
{
    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([](psyrpc::proto::Call &call, const std::chrono::milliseconds) {
            call.mutable_parameter();
            return true;
        });
    EXPECT_CALL(mock.channel, write(proto::Rpc::NoActiveCall));
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_TRUE(channelData.hasActiveCall);
}

MATCHER_P(ResultMessageInitEq, param, "")
{
    const proto::Result &result = arg;
    const bool value = param;
    return result.has_init() && (result.init() == value);
}

TEST_F(RemoteCallDispatcherDispatcher, dispatchButNoServicesAssigned)
{
    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([](psyrpc::proto::Call &call, const std::chrono::milliseconds) {
            call.mutable_init();
            return true;
        });
    EXPECT_CALL(mock.channel,
                write(testing::Matcher<proto::Result &&>(ResultMessageInitEq(false))));
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_TRUE(channelData.hasActiveCall);
}

struct RemoteCallDispatcherDispatcher_atLeastOneServiceAttached : RemoteCallDispatcherDispatcher {
    static constexpr int serviceId = 1;
    static constexpr int methodId = 10;

    void SetUp() override
    {
        services.emplace_back(&mock.service);
    }
};

TEST_F(RemoteCallDispatcherDispatcher_atLeastOneServiceAttached, dispatchButCallOnUnknownServiceId)
{
    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([](psyrpc::proto::Call &call, const std::chrono::milliseconds) {
            auto *service = call.mutable_init()->mutable_service();
            service->set_value(serviceId + 1);
            service->set_customidtoavoidcollision(serviceId);
            return true;
        });
    EXPECT_CALL(mock.service, id).WillOnce(testing::Return(serviceId));
    EXPECT_CALL(mock.channel,
                write(testing::Matcher<proto::Result &&>(ResultMessageInitEq(false))));
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_TRUE(channelData.hasActiveCall);
}

TEST_F(RemoteCallDispatcherDispatcher_atLeastOneServiceAttached,
       dispatchButCallOnUnknownServiceIdToAvoidCollision)
{
    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([](psyrpc::proto::Call &call, const std::chrono::milliseconds) {
            auto *service = call.mutable_init()->mutable_service();
            service->set_value(serviceId);
            service->set_customidtoavoidcollision(serviceId + 1);
            return true;
        });
    EXPECT_CALL(mock.service, id).WillOnce(testing::Return(serviceId));
    EXPECT_CALL(mock.service, customId).WillOnce(testing::Return(serviceId));
    EXPECT_CALL(mock.channel,
                write(testing::Matcher<proto::Result &&>(ResultMessageInitEq(false))));
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_TRUE(channelData.hasActiveCall);
}

MATCHER_P(MethodMessageEq, param, "")
{
    const proto::XxHashedSignature &lhs = arg;
    const proto::XxHashedSignature &rhs = param;
    return (lhs.value() == rhs.value()) &&
        (lhs.customidtoavoidcollision() == rhs.customidtoavoidcollision());
}

TEST_F(RemoteCallDispatcherDispatcher_atLeastOneServiceAttached,
       dispatchButServiceDoesNotHaveMethod)
{
    proto::XxHashedSignature method;
    method.set_value(methodId);
    method.set_customidtoavoidcollision(methodId);

    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([method](psyrpc::proto::Call &call, const std::chrono::milliseconds) {
            auto *init = call.mutable_init();
            auto *service = init->mutable_service();
            service->set_value(serviceId);
            service->set_customidtoavoidcollision(serviceId);
            *init->mutable_method() = method;
            return true;
        });
    EXPECT_CALL(mock.service, id).WillOnce(testing::Return(serviceId));
    EXPECT_CALL(mock.service, customId).WillOnce(testing::Return(serviceId));
    EXPECT_CALL(mock.service, has(MethodMessageEq(method))).WillOnce(testing::Return(false));
    EXPECT_CALL(mock.channel,
                write(testing::Matcher<proto::Result &&>(ResultMessageInitEq(false))));
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_TRUE(channelData.hasActiveCall);
}

TEST_F(RemoteCallDispatcherDispatcher_atLeastOneServiceAttached, dispatch)
{
    proto::XxHashedSignature method;
    method.set_value(methodId);
    method.set_customidtoavoidcollision(methodId);

    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([method](psyrpc::proto::Call &call, const std::chrono::milliseconds) {
            auto *init = call.mutable_init();
            auto *service = init->mutable_service();
            service->set_value(serviceId);
            service->set_customidtoavoidcollision(serviceId);
            *init->mutable_method() = method;
            return true;
        });
    EXPECT_CALL(mock.service, id).WillOnce(testing::Return(serviceId));
    EXPECT_CALL(mock.service, customId).WillOnce(testing::Return(serviceId));
    EXPECT_CALL(mock.service, has(MethodMessageEq(method))).WillOnce(testing::Return(true));
    EXPECT_CALL(mock.channel, write(testing::Matcher<proto::Result &&>(ResultMessageInitEq(true))));
    EXPECT_CALL(mock.service, call(MethodMessageEq(method), testing::_, waitFor));
    dispatcher.dispatch(AdaptedChannelData{channelData}, waitFor);
    ASSERT_FALSE(channelData.hasActiveCall);
}

} // namespace psyrpc
