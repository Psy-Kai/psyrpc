#include <psyrpc/impl/remote/calldispatcher.h>

#include <mock/remote/channel.h>
#include <mock/remote/transfer.h>

#include "stub/channeldata.h"

namespace psyrpc {

struct RemoteCallDispatcherChannelFinder : testing::Test {
    using ChannelFinder = impl::remote::calldispatcher::ChannelFinder<stub::ChannelData>;

    struct {
        testing::StrictMock<mock::remote::Channel> channel;
    } mock;
    struct {
        testing::StrictMock<mock::remote::Transfer> transfer;
    } stub;

    std::vector<stub::ChannelData> channels;
};

TEST_F(RemoteCallDispatcherChannelFinder, find)
{
    channels.emplace_back(stub::ChannelData{mock.channel});
    auto &channelData = channels.front();

    EXPECT_CALL(mock.channel, transfer).WillOnce(testing::ReturnRef(stub.transfer));
    ASSERT_EQ(ChannelFinder::find(std::begin(channels), std::end(channels), stub.transfer),
              &channelData);
}

TEST_F(RemoteCallDispatcherChannelFinder, findWithEmptyRange)
{
    ASSERT_EQ(ChannelFinder::find(std::begin(channels), std::end(channels), stub.transfer),
              nullptr);
}

TEST_F(RemoteCallDispatcherChannelFinder, findButNoMatch)
{
    channels.emplace_back(stub::ChannelData{mock.channel});
    auto &channelData = channels.front();

    EXPECT_CALL(mock.channel, transfer).WillOnce(testing::ReturnRef(stub.transfer));
    testing::StrictMock<::mock::remote::Transfer> anotherTransfer;
    ASSERT_EQ(ChannelFinder::find(std::begin(channels), std::end(channels), anotherTransfer),
              nullptr);
}

} // namespace psyrpc
