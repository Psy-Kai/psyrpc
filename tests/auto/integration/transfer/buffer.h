#pragma once

#include <mutex>
#include <string>

namespace psyrpc {
namespace integration {
namespace transfer {
namespace buffer {

class Locked final {
public:
    using LockType = std::unique_lock<std::mutex>;

    Locked(std::string &stream, LockType lock) : m_lock{std::move(lock)}, m_stream{&stream} {}

    std::string &stream()
    {
        return *m_stream;
    }
    operator std::string &()
    {
        return stream();
    }

private:
    LockType m_lock;
    std::string *m_stream;
};

} // namespace buffer

class Buffer final {
public:
    Buffer(std::string &in, std::string &out, std::mutex &mutex) :
        m_in{&in}, m_out{&out}, m_mutex{&mutex}
    {}

    buffer::Locked in()
    {
        return {*m_in, buffer::Locked::LockType{*m_mutex}};
    }
    buffer::Locked out()
    {
        return {*m_out, buffer::Locked::LockType{*m_mutex}};
    }

private:
    std::string *m_in;
    std::string *m_out;
    std::mutex *m_mutex;
};

} // namespace transfer
} // namespace integration
} // namespace psyrpc
