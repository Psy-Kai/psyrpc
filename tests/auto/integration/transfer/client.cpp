#include "client.h"

#include <array>
#include <thread>

namespace psyrpc {
namespace integration {
namespace transfer {

Client::Client(Buffer buffer) : m_buffer{buffer} {}

bool Client::defaultWrite(const google::protobuf::MessageLite &message)
{
    const auto s = message.SerializeAsString();
    auto sizeBuffer = std::array<char, 2>{};
    sizeBuffer[0] = static_cast<char>(static_cast<uint8_t>(s.size()));
    sizeBuffer[1] =
        static_cast<char>(static_cast<uint8_t>(s.size() >> std::numeric_limits<uint8_t>::digits));

    auto out = m_buffer.out();
    out.stream().append(sizeBuffer.data(), sizeBuffer.size());
    out.stream().append(s);
    return true;
}

bool Client::defaultRead(google::protobuf::MessageLite &message,
                         const std::chrono::milliseconds waitFor)
{
    const auto now = std::chrono::steady_clock::now();
    const auto nowMax = std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::steady_clock::time_point::max() - now);
    const auto timeoutTimepoint = now + std::min(nowMax, waitFor);
    const auto waitUntilReceived = [&](const std::size_t count) {
        while (m_buffer.in().stream().size() < count) {
            if (timeoutTimepoint < std::chrono::steady_clock::now()) {
                return false;
            }
            std::this_thread::yield();
        }
        return true;
    };
    const auto waitUntilSizeIsReceived = [&] { return waitUntilReceived(2); };
    const auto readSize = [&] {
        std::size_t size = 0;

        auto in = m_buffer.in();
        auto &s = in.stream();
        size |= s[1];
        size <<= std::numeric_limits<uint8_t>::digits;
        size |= s[0];
        s.erase(0, 2);
        return size;
    };
    const auto readMessage = [&](google::protobuf::MessageLite &message, const std::size_t size) {
        auto in = m_buffer.in();
        message.ParseFromString(in);
        in.stream().erase(0, size);
    };

    if (!waitUntilSizeIsReceived()) {
        return false;
    }
    const auto size = readSize();
    if (!waitUntilReceived(size)) {
        return false;
    }
    readMessage(message, size);
    return true;
}

} // namespace transfer
} // namespace integration
} // namespace psyrpc
