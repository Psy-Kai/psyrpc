#pragma once

#include <gmock/gmock.h>

#include <psyrpc/client/transferinterface.h>

#include "buffer.h"

namespace psyrpc {
namespace integration {
namespace transfer {

class Client : public psyrpc::client::TransferInterface {
public:
    explicit Client(Buffer buffer);

    MOCK_METHOD(bool, write, (const google::protobuf::MessageLite &), (override));
    MOCK_METHOD(bool, read, (google::protobuf::MessageLite &, const std::chrono::milliseconds),
                (override));

    bool defaultWrite(const google::protobuf::MessageLite &);
    bool defaultRead(google::protobuf::MessageLite &, const std::chrono::milliseconds);

private:
    Buffer m_buffer;
};

} // namespace transfer
} // namespace integration
} // namespace psyrpc
