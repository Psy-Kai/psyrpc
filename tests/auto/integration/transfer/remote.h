#pragma once

#include <gmock/gmock.h>

#include <psyrpc/remote/transferinterface.h>

#include "buffer.h"

namespace psyrpc {
namespace integration {
namespace transfer {

class Remote : public psyrpc::remote::TransferInterface {
public:
    explicit Remote(Buffer buffer);

    MOCK_METHOD(bool, read, (google::protobuf::MessageLite &, const std::chrono::milliseconds),
                (override));
    MOCK_METHOD(bool, write, (const google::protobuf::MessageLite &), (override));
    void attach(remote::CallDispatcherInterface *callDispatcher) override;

    bool defaultRead(google::protobuf::MessageLite &, const std::chrono::milliseconds);
    bool defaultWrite(const google::protobuf::MessageLite &);
    void peek(const std::chrono::seconds waitFor);

private:
    Buffer m_buffer;
    remote::CallDispatcherInterface *m_callDispatcher = nullptr;
};

} // namespace transfer
} // namespace integration
} // namespace psyrpc
