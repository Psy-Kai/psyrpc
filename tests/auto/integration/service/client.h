#pragma once

#include <psyrpc/client/servicebase.h>

#include "common.h"

namespace psyrpc {
namespace integration {
namespace service {

class Client : public client::ServiceBase {
public:
    using ServiceBase::ServiceBase;

    nonstd::optional<psyrpc::client::stream::Result<ResultMsg>>
    intToString(psyrpc::client::stream::Parameter<const ParameterMsg> &paramStream,
                const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max())
    {
        static constexpr auto methodId = callId;
        static constexpr auto customMethodId = 0;

        return call<ResultMsg, psyrpc::client::servicebase::result::stream>(
            serviceId, customServiceId, methodId, customMethodId, paramStream, waitFor);
    }

private:
    static constexpr auto serviceId = service::serviceId;
    static constexpr auto customServiceId = 0;
};

} // namespace service
} // namespace integration
} // namespace psyrpc
