#pragma once

#include <psyrpc/remote/servicebase.h>

#include "common.h"

namespace psyrpc {
namespace integration {
namespace service {

class RemoteBase : public remote::ServiceBase<serviceId, 0> {
private:
    bool has(const psyrpc::proto::XxHashedSignature &method) override
    {
        switch (method.value()) {
            case callId:
                return method.customidtoavoidcollision() == 0;
        }
        return false;
    }

    bool call(const psyrpc::proto::XxHashedSignature &method, psyrpc::common::call::Interface &call,
              const std::chrono::milliseconds waitFor) override
    {
        assert(has(method));
        (void)waitFor;

        if (method.value() == callId) {
            return intToString(psyrpc::remote::detail::stream::makeParameter<ParameterMsg>(call),
                               psyrpc::remote::detail::stream::makeResult<ResultMsg>(call));
        }

        return false;
    }

    virtual bool intToString(psyrpc::remote::stream::Parameter<ParameterMsg> paramMsg,
                             psyrpc::remote::stream::Result<ResultMsg> resultMsg) = 0;
};

class Remote : public RemoteBase {
public:
    bool success = false;

private:
    bool intToString(psyrpc::remote::stream::Parameter<ParameterMsg> paramStream,
                     psyrpc::remote::stream::Result<ResultMsg> resultStream) override
    {
        const auto intToString = [&] {
            ParameterMsg param;
            while (paramStream.read(param, std::chrono::seconds{1})) {
                ResultMsg result;
                result.set_value(std::to_string(param.value()));

                std::puts("#### remote - write");
                if (paramStream.eot()) {
                    if (!resultStream.write(result, psyrpc::eot)) {
                        return false;
                    }
                } else {
                    if (!resultStream.write(result)) {
                        return false;
                    }
                }
            }
            if (!paramStream.eot()) {
                return false;
            }
            return true;
        };
        success = intToString();
        return success;
    }
};

} // namespace service
} // namespace integration
} // namespace psyrpc
