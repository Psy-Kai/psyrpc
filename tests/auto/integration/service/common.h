#pragma once

#include <google/protobuf/wrappers.pb.h>

namespace psyrpc {
namespace integration {
namespace service {

constexpr auto serviceId = 1234567890;
constexpr auto callId = 7;

using ParameterMsg = google::protobuf::Int32Value;
using ResultMsg = google::protobuf::StringValue;

} // namespace service
} // namespace integration
} // namespace psyrpc
