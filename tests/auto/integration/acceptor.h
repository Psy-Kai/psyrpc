#pragma once

#include <psyrpc/remote/acceptorinterface.h>

namespace psyrpc {
namespace integration {

class Acceptor final : public remote::AcceptorInterface {
public:
    void assign(remote::ChannelManagerInterface &channelManager) override
    {
        m_channelManager = &channelManager;
    }

    void onNew(remote::TransferInterface &transfer)
    {
        m_channelManager->onOpened(transfer);
    }
    void onClosed(remote::TransferInterface &transfer)
    {
        m_channelManager->onClosed(transfer);
    }

private:
    remote::ChannelManagerInterface *m_channelManager = nullptr;
};

} // namespace integration
} // namespace psyrpc
