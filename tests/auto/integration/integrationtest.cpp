#include <thread>

#include <gmock/gmock.h>

#include <psyrpc/client/channel.h>
#include <psyrpc/remote/server.h>

#include "acceptor.h"
#include "service/client.h"
#include "service/remote.h"
#include "transfer/client.h"
#include "transfer/remote.h"

namespace psyrpc {

constexpr auto successfulTransfers = 5;
constexpr auto timeoutTime = std::chrono::seconds{2};

struct Integration : testing::Test {
    void TearDown() override
    {
        stopRemote();
    }

    void startRemote()
    {
        thread = std::thread{[this] {
            while (runRemote) {
                remote.transfer.peek(timeoutTime);
            }
        }};
    }
    void stopRemote()
    {
        runRemote = false;
        if (thread.joinable()) {
            thread.join();
        }
    }

    struct {
        std::string clientInput;
        std::string remoteInput;
        std::mutex mutex;
    } communication;

    struct Client {
        explicit Client(integration::transfer::Buffer buffer) : transfer{buffer} {}
        testing::StrictMock<integration::transfer::Client> transfer;
        psyrpc::client::Channel channel{transfer};
        integration::service::Client service{channel};
    } client{{communication.clientInput, communication.remoteInput, communication.mutex}};
    struct Remote {
        explicit Remote(integration::transfer::Buffer buffer) : transfer{buffer}
        {
            acceptor.assign(server);
            acceptor.onNew(transfer);
            server.add(service);
        }
        testing::StrictMock<integration::transfer::Remote> transfer;
        integration::Acceptor acceptor;
        psyrpc::remote::Server server;
        integration::service::Remote service;
    } remote{{communication.remoteInput, communication.clientInput, communication.mutex}};

    std::thread thread;
    bool runRemote = true;
};

TEST_F(Integration, call)
{
    EXPECT_CALL(client.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return client.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(client.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return client.transfer.defaultWrite(message);
        });
    EXPECT_CALL(remote.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return remote.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(remote.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return remote.transfer.defaultWrite(message);
        });

    startRemote();

    auto param = client::stream::Parameter<const integration::service::ParameterMsg>{};
    auto result = client.service.intToString(param, timeoutTime);
    ASSERT_TRUE(result.has_value());

    integration::service::ParameterMsg integer;
    integer.set_value(42);
    ASSERT_TRUE(param.write(integer, psyrpc::eot));

    integration::service::ResultMsg string;
    ASSERT_TRUE(result->read(string, timeoutTime));
    ASSERT_EQ(string.value(), std::string{"42"});
    ASSERT_TRUE(result->eot());

    ASSERT_TRUE(remote.service.success);
}

TEST_F(Integration, callButErrorWhileClientTransfer)
{
    testing::Sequence seq;
    EXPECT_CALL(client.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return client.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(client.transfer, write)
        .Times(successfulTransfers)
        .InSequence(seq)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return client.transfer.defaultWrite(message);
        });
    EXPECT_CALL(client.transfer, write)
        .InSequence(seq)
        .WillOnce([&](const google::protobuf::MessageLite &) { return false; });

    EXPECT_CALL(remote.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return remote.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(remote.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return remote.transfer.defaultWrite(message);
        });

    startRemote();

    auto param = client::stream::Parameter<const integration::service::ParameterMsg>{};
    auto result = client.service.intToString(param, timeoutTime);
    ASSERT_TRUE(result.has_value());

    integration::service::ParameterMsg integer;
    for (auto i = 0; i < (successfulTransfers - 1); ++i) {
        integer.set_value(i + 1337);
        ASSERT_TRUE(param.write(integer));

        integration::service::ResultMsg string;
        ASSERT_TRUE(result->read(string, timeoutTime));
        ASSERT_EQ(string.value(), std::to_string(integer.value()));
    }

    integer.set_value(1337);
    ASSERT_FALSE(param.write(integer));

    stopRemote();
    ASSERT_FALSE(remote.service.success);
}

TEST_F(Integration, callButErrorWhileClientReceive)
{
    testing::Sequence seq;
    EXPECT_CALL(client.transfer, read)
        .Times(successfulTransfers)
        .InSequence(seq)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return client.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(client.transfer, read)
        .InSequence(seq)
        .WillOnce([&](google::protobuf::MessageLite &, const std::chrono::milliseconds) {
            return false;
        });
    EXPECT_CALL(client.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return client.transfer.defaultWrite(message);
        });

    EXPECT_CALL(remote.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return remote.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(remote.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return remote.transfer.defaultWrite(message);
        });

    startRemote();

    auto param = client::stream::Parameter<const integration::service::ParameterMsg>{};
    auto result = client.service.intToString(param, timeoutTime);
    ASSERT_TRUE(result.has_value());

    integration::service::ParameterMsg integer;
    integration::service::ResultMsg string;
    for (auto i = 0; i < (successfulTransfers - 1); ++i) {
        integer.set_value(i + 42);
        ASSERT_TRUE(param.write(integer));

        ASSERT_TRUE(result->read(string, timeoutTime));
        ASSERT_EQ(string.value(), std::to_string(integer.value()));
    }

    integer.set_value(1337);
    ASSERT_TRUE(param.write(integer, psyrpc::eot));
    ASSERT_FALSE(result->read(string, timeoutTime));

    stopRemote();
    ASSERT_TRUE(remote.service.success);
}

TEST_F(Integration, callButErrorWhileRemoteTransfer)
{
    testing::Sequence seq;
    EXPECT_CALL(client.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return client.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(client.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return client.transfer.defaultWrite(message);
        });

    EXPECT_CALL(remote.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return remote.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(remote.transfer, write)
        .Times(successfulTransfers)
        .InSequence(seq)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return remote.transfer.defaultWrite(message);
        });
    EXPECT_CALL(remote.transfer, write)
        .InSequence(seq)
        .WillRepeatedly([&](const google::protobuf::MessageLite &) { return false; });

    startRemote();

    auto param = client::stream::Parameter<const integration::service::ParameterMsg>{};
    auto result = client.service.intToString(param, timeoutTime);
    ASSERT_TRUE(result.has_value());

    integration::service::ParameterMsg integer;
    integration::service::ResultMsg string;
    for (auto i = 0; i < (successfulTransfers - 1); ++i) {
        integer.set_value(1993 + i);
        ASSERT_TRUE(param.write(integer));

        ASSERT_TRUE(result->read(string, timeoutTime));
        ASSERT_EQ(string.value(), std::to_string(integer.value()));
    }

    integer.set_value(1337);
    ASSERT_TRUE(param.write(integer, psyrpc::eot));
    ASSERT_FALSE(result->read(string, timeoutTime));

    stopRemote();
    ASSERT_FALSE(remote.service.success);
}

TEST_F(Integration, callButErrorWhileRemoteReceive)
{
    testing::Sequence seq;
    EXPECT_CALL(client.transfer, read)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return client.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(client.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return client.transfer.defaultWrite(message);
        });

    EXPECT_CALL(remote.transfer, read)
        .Times(successfulTransfers)
        .InSequence(seq)
        .WillRepeatedly(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor) {
                return remote.transfer.defaultRead(message, waitFor);
            });
    EXPECT_CALL(remote.transfer, read)
        .InSequence(seq)
        .WillRepeatedly([&](google::protobuf::MessageLite &, const std::chrono::milliseconds) {
            return false;
        });

    EXPECT_CALL(remote.transfer, write)
        .WillRepeatedly([&](const google::protobuf::MessageLite &message) {
            return remote.transfer.defaultWrite(message);
        });

    startRemote();

    auto param = client::stream::Parameter<const integration::service::ParameterMsg>{};
    auto result = client.service.intToString(param, timeoutTime);
    ASSERT_TRUE(result.has_value());

    integration::service::ParameterMsg integer;
    integration::service::ResultMsg string;
    for (auto i = 0; i < (successfulTransfers - 1); ++i) {
        integer.set_value(1993 + i);
        ASSERT_TRUE(param.write(integer));

        ASSERT_TRUE(result->read(string, timeoutTime));
        ASSERT_EQ(string.value(), std::to_string(integer.value()));
    }

    integer.set_value(1337);
    ASSERT_TRUE(param.write(integer, psyrpc::eot));
    ASSERT_FALSE(result->read(string, timeoutTime));

    stopRemote();
    ASSERT_FALSE(remote.service.success);
}

} // namespace psyrpc

int main(int argc, char **argv)
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
