#include <gmock/gmock.h>
#include <google/protobuf/wrappers.pb.h>
#include <gtest/gtest.h>

#include <mock/call.h>
#include <psyrpc/common/stream/writer.h>
#include <psyrpc/proto/operators.h>
#include <psyrpc/random.h>

namespace psyrpc {

using Message = google::protobuf::Int32Value;

struct CommonStreamWriterWrite : public testing::Test {
    struct {
        testing::StrictMock<mock::Call> call;
    } mock;
    common::stream::Writer<Message> writer{mock.call};
    Random rand;
};

constexpr auto packageCount = 10;

MATCHER_P(PackageEq, param, "Matches psyrpc::proto::Package objects")
{
    return proto::matcherImpl<proto::Package>(arg, param);
}

TEST_F(CommonStreamWriterWrite, single)
{
    auto message = Message{};
    message.set_value(rand());

    auto expectedPackage = proto::Package{};
    expectedPackage.set_index(1);
    expectedPackage.mutable_data()->set_value(message.SerializeAsString());
    expectedPackage.set_eot(false);

    EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(true));

    ASSERT_TRUE(writer.write(message));
}

TEST_F(CommonStreamWriterWrite, singleClose)
{
    auto message = Message{};
    message.set_value(rand());

    auto expectedPackage = proto::Package{};
    expectedPackage.set_index(1);
    expectedPackage.mutable_data()->set_value(message.SerializeAsString());
    expectedPackage.set_eot(true);

    EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(true));

    ASSERT_TRUE(writer.write(message, Eot{}));
}

TEST_F(CommonStreamWriterWrite, multiple)
{
    testing::InSequence seq;

    auto message = Message{};
    message.set_value(rand());

    auto expectedPackage = proto::Package{};
    for (std::size_t i = 1; i <= packageCount; ++i) {
        expectedPackage.set_index(i);
        expectedPackage.mutable_data()->set_value(message.SerializeAsString());
        expectedPackage.set_eot(false);
        EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(true));
    }

    for (std::size_t i = 0; i < packageCount; ++i) {
        ASSERT_TRUE(writer.write(message));
    }
}

TEST_F(CommonStreamWriterWrite, multipleClose)
{
    testing::InSequence seq;

    auto message = Message{};
    message.set_value(rand());

    auto expectedPackage = proto::Package{};
    for (std::size_t i = 1; i <= packageCount; ++i) {
        expectedPackage.set_index(i);
        expectedPackage.mutable_data()->set_value(message.SerializeAsString());
        expectedPackage.set_eot(false);
        EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(true));
    }
    expectedPackage.set_index(packageCount + 1);
    expectedPackage.mutable_data()->set_value(message.SerializeAsString());
    expectedPackage.set_eot(true);
    EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(true));

    for (std::size_t i = 0; i < packageCount; ++i) {
        ASSERT_TRUE(writer.write(message));
    }
    ASSERT_TRUE(writer.write(message, Eot{}));
}

TEST_F(CommonStreamWriterWrite, callWriteFailed)
{
    auto message = Message{};
    message.set_value(rand());

    auto expectedPackage = proto::Package{};
    expectedPackage.set_index(1);
    expectedPackage.mutable_data()->set_value(message.SerializeAsString());
    expectedPackage.set_eot(false);

    EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(false));

    ASSERT_FALSE(writer.write(message));
    ASSERT_FALSE(writer.write(message));
}

TEST_F(CommonStreamWriterWrite, writeAfterClose)
{
    auto message = Message{};
    message.set_value(rand());

    auto expectedPackage = proto::Package{};
    expectedPackage.set_index(1);
    expectedPackage.mutable_data()->set_value(message.SerializeAsString());
    expectedPackage.set_eot(true);

    EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(true));

    ASSERT_TRUE(writer.write(message, Eot{}));
    ASSERT_FALSE(writer.write(message));
}

TEST_F(CommonStreamWriterWrite, close)
{
    auto expectedPackage = proto::Package{};
    expectedPackage.set_index(1);
    expectedPackage.set_eot(true);

    EXPECT_CALL(mock.call, write(PackageEq(expectedPackage))).WillOnce(testing::Return(true));

    ASSERT_TRUE(writer.write(Eot{}));
    ASSERT_FALSE(writer.write(Message{}));
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
