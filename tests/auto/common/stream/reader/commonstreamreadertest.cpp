#include <gmock/gmock.h>
#include <google/protobuf/wrappers.pb.h>
#include <gtest/gtest.h>

#include <mock/ascendingnumbergenerator.h>
#include <mock/call.h>
#include <psyrpc/common/stream/reader.h>
#include <psyrpc/impl/common/stream/reader.h>
#include <psyrpc/random.h>

namespace psyrpc {

using Message = google::protobuf::Int32Value;

struct CommonStreamReader : public testing::Test {
    std::unique_ptr<impl::common::stream::Reader> makeD()
    {
        auto d = std::unique_ptr<impl::common::stream::Reader>{
            new impl::common::stream::Reader{mock.ascendingnumbergenerator}};
        d->call = &mock.call;
        this->d = d.get();
        return d;
    }

    struct {
        testing::StrictMock<mock::Call> call;
        testing::StrictMock<mock::AscendingNumberGenerator> ascendingnumbergenerator;
    } mock;
    impl::common::stream::Reader *d = nullptr;
    common::stream::Reader<Message> reader{makeD()};
    std::chrono::milliseconds waitFor{std::rand()};
};

constexpr auto packageCount = 10;

TEST_F(CommonStreamReader, read)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex);
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    package.set_eot(false);
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
    EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_TRUE(reader.read(message, waitFor));
    ASSERT_EQ(message.value(), expectedMessage.value());
    ASSERT_FALSE(reader.eot());
    ASSERT_NE(d->call, nullptr);
}

TEST_F(CommonStreamReader, readMultipleTimes)
{
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    package.set_eot(false);

    testing::InSequence seq;

    for (std::size_t packageIndex = 1; packageIndex <= packageCount; ++packageIndex) {
        package.set_index(packageIndex);
        EXPECT_CALL(mock.call, read(testing::_, waitFor))
            .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
        EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));
    }

    auto message = Message{};
    for (std::size_t i = 0; i < packageCount; ++i) {
        ASSERT_TRUE(reader.read(message, waitFor));
        ASSERT_EQ(message.value(), expectedMessage.value());
        ASSERT_FALSE(reader.eot());
    }
    ASSERT_NE(d->call, nullptr);
}

TEST_F(CommonStreamReader, readAndEot)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex);
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    package.set_eot(true);
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
    EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_TRUE(reader.read(message, waitFor));
    ASSERT_EQ(message.value(), expectedMessage.value());
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_TRUE(reader.eot());
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(CommonStreamReader, readMultipleTimesAndEot)
{
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    package.set_eot(false);

    testing::InSequence seq;

    for (std::size_t packageIndex = 1; packageIndex <= packageCount; ++packageIndex) {
        package.set_index(packageIndex);
        EXPECT_CALL(mock.call, read(testing::_, waitFor))
            .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
        EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));
    }

    auto message = Message{};
    for (std::size_t i = 0; i < packageCount; ++i) {
        ASSERT_TRUE(reader.read(message, waitFor));
        ASSERT_EQ(message.value(), expectedMessage.value());
        ASSERT_FALSE(reader.eot());
    }

    package.set_eot(true);
    package.set_index(packageCount + 1);
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
    EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageCount + 1));
    ASSERT_TRUE(reader.read(message, waitFor));
    ASSERT_EQ(message.value(), expectedMessage.value());
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_TRUE(reader.eot());
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(CommonStreamReader, readAfterEot)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex);
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    package.set_eot(true);
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
    EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_TRUE(reader.read(message, waitFor));
    ASSERT_EQ(message.value(), expectedMessage.value());
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_TRUE(reader.eot());
    ASSERT_EQ(d->call, nullptr);

    ASSERT_FALSE(reader.read(message, waitFor));
}

TEST_F(CommonStreamReader, readButCallReadFails)
{
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(false)));

    auto message = Message{};
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_FALSE(reader.eot());
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(CommonStreamReader, readMultipleTimesUntilInvalidPackageIndex)
{
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    package.set_eot(false);

    testing::InSequence seq;

    for (std::size_t packageIndex = 1; packageIndex < packageCount; ++packageIndex) {
        package.set_index(packageIndex);
        EXPECT_CALL(mock.call, read(testing::_, waitFor))
            .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
        EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));
    }
    static constexpr auto expectedPackageIndex = 1;
    package.set_index(expectedPackageIndex - 1);
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
    EXPECT_CALL(mock.ascendingnumbergenerator, next)
        .WillOnce(testing::Return(expectedPackageIndex));

    auto message = Message{};
    for (std::size_t i = 0; i < packageCount - 1; ++i) {
        ASSERT_TRUE(reader.read(message, waitFor));
        ASSERT_EQ(message.value(), expectedMessage.value());
        ASSERT_FALSE(reader.eot());
    }
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_FALSE(reader.eot());
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(CommonStreamReader, readAndEotWithInvalidData)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex);
    package.set_eot(true);
    package.mutable_data()->set_value("Some Bad Data");
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
    EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_FALSE(reader.eot());
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(CommonStreamReader, readAndEotWithoutData)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex);
    package.set_eot(true);
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce(testing::DoAll(testing::SetArgReferee<0>(package), testing::Return(true)));
    EXPECT_CALL(mock.ascendingnumbergenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_TRUE(reader.eot());
    ASSERT_EQ(d->call, nullptr);
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
