#include <random>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <mock/remote/channel.h>
#include <psyrpc/proto/operators.h>
#include <psyrpc/random.h>
#include <psyrpc/remote/call.h>

namespace psyrpc {

struct RemoteCall : public testing::Test {
    struct {
        testing::StrictMock<mock::remote::Channel> channel;
    } mock;
    Random rand;
    const std::size_t callId = static_cast<std::size_t>(rand());
    remote::Call call{callId, mock.channel};
    const uint64_t parameterIndex = rand();
    const std::string data{"Hello World"};
    std::chrono::milliseconds waitFor{std::rand()};
};

MATCHER_P(CallEq, param, "Matches psyrpc::proto::Call objects")
{
    return proto::matcherImpl<proto::Call>(arg, param);
}

MATCHER_P(ResultEq, param, "Matches psyrpc::proto::Result objects")
{
    return proto::matcherImpl<proto::Result>(arg, param);
}

MATCHER_P(PackageEq, param, "Matches psyrpc::proto::Package objects")
{
    return proto::matcherImpl<proto::Package>(arg, param);
}

TEST_F(RemoteCall, write)
{
    auto resultMessage = proto::Result{};
    resultMessage.set_callid(callId);
    resultMessage.mutable_result()->set_index(parameterIndex);
    resultMessage.mutable_result()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, write(testing::Matcher<proto::Result &&>(ResultEq(resultMessage))))
        .WillOnce(testing::Return(true));

    ASSERT_TRUE(call.write(proto::Package{resultMessage.result()}));
}

TEST_F(RemoteCall, writeButWriteChannelFails)
{
    auto resultMessage = proto::Result{};
    resultMessage.set_callid(callId);
    resultMessage.mutable_result()->set_index(parameterIndex);
    resultMessage.mutable_result()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, write(testing::Matcher<proto::Result &&>(ResultEq(resultMessage))))
        .WillOnce(testing::Return(false));

    ASSERT_FALSE(call.write(proto::Package{resultMessage.result()}));
}

TEST_F(RemoteCall, read)
{
    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    callMessage.mutable_parameter()->set_index(parameterIndex);
    callMessage.mutable_parameter()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([&callMessage](proto::Call &call, const std::chrono::microseconds) {
            call = callMessage;
            return true;
        });

    auto package = proto::Package{};
    ASSERT_TRUE(call.read(package, waitFor));
    ASSERT_THAT(package, PackageEq(callMessage.parameter()));
}

TEST_F(RemoteCall, readButReadChannelFails)
{
    auto callMessage = proto::Call{};
    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([&callMessage](proto::Call &call, const std::chrono::microseconds) {
            call = callMessage;
            return false;
        });

    auto package = proto::Package{};
    ASSERT_FALSE(call.read(package, waitFor));
}

TEST_F(RemoteCall, readButWrongCallId)
{
    auto callMessage = proto::Call{};
    callMessage.set_id(callId + 1);
    callMessage.mutable_parameter()->set_index(parameterIndex);
    callMessage.mutable_parameter()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([&callMessage](proto::Call &call, const std::chrono::microseconds) {
            call = callMessage;
            return true;
        });

    auto package = proto::Package{};
    ASSERT_FALSE(call.read(package, waitFor));
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
