#include <google/protobuf/empty.pb.h>
#include <gtest/gtest.h>

#include <mock/call.h>
#include <psyrpc/remote/channelinterface.h>
#include <psyrpc/remote/servicebase.h>
#include <psyrpc/remote/stream/parameter.h>
#include <psyrpc/remote/stream/result.h>

namespace psyrpc {

using Message = google::protobuf::Empty;

namespace stub {

constexpr ServiceId serviceId = 1337;
constexpr ServiceId customId = 4242;

class Service final : public remote::ServiceBase<serviceId, customId> {
    bool has(const proto::XxHashedSignature &) override
    {
        return true;
    }
    bool call(const proto::XxHashedSignature &, common::call::Interface &,
              const std::chrono::milliseconds) override
    {
        return true;
    }
};

namespace stream {

struct Base {
    explicit Base(psyrpc::common::call::Interface &call) : call{&call} {}
    psyrpc::common::call::Interface *call;
};

template <class>
using Parameter = Base;

template <class>
using Result = Base;

} // namespace stream
} // namespace stub

struct RemoteServiceGetId : public testing::Test {
    stub::Service service;
};

struct RemoteServiceMake : public testing::Test {
    static constexpr auto stubCallId = 1;

    struct {
        testing::StrictMock<mock::Call> call;
    } mock;
};

TEST_F(RemoteServiceGetId, id)
{
    ASSERT_EQ(service.id(), stub::serviceId);
}

TEST_F(RemoteServiceGetId, customId)
{
    ASSERT_EQ(service.customId(), stub::customId);
}

TEST_F(RemoteServiceMake, result)
{
    auto result = remote::detail::stream::makeResult<Message, stub::stream::Result>(mock.call);
    ASSERT_EQ(result.call, &mock.call);
}

TEST_F(RemoteServiceMake, param)
{
    auto result =
        remote::detail::stream::makeParameter<Message, stub::stream::Parameter>(mock.call);
    ASSERT_EQ(result.call, &mock.call);
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
