#include <gmock/gmock.h>

#include <mock/remote/calldispatcher.h>
#include <mock/remote/service.h>
#include <mock/remote/transfer.h>
#include <psyrpc/impl/remote/server.h>
#include <psyrpc/remote/server.h>

namespace psyrpc {

struct RemoteServer : public testing::Test {
    struct {
        testing::StrictMock<mock::remote::Transfer> transfer;
        testing::StrictMock<mock::remote::Service> service;
    } mock;
    struct {
        testing::StrictMock<mock::remote::CallDispatcher> callDispatcher;
    } stub;

    impl::remote::Server *pimpl = nullptr;
    remote::Server server{createPimpl()};
    remote::ChannelManagerInterface &channelManager = server;

private:
    std::unique_ptr<impl::remote::Server> createPimpl()
    {
        pimpl = new impl::remote::Server{stub.callDispatcher};
        return std::unique_ptr<impl::remote::Server>{pimpl};
    }
};

using RemoteServer_TransferNotInList = RemoteServer;

TEST_F(RemoteServer_TransferNotInList, onOpened)
{
    EXPECT_CALL(mock.transfer, attach(&stub.callDispatcher));
    channelManager.onOpened(mock.transfer);
    ASSERT_EQ(pimpl->channels.size(), static_cast<std::size_t>(1));
    ASSERT_FALSE(pimpl->channels.front().hasActiveCall);
    ASSERT_EQ(&pimpl->channels.front().channel.transfer(), &mock.transfer);
}

TEST_F(RemoteServer_TransferNotInList, onClosed)
{
    mock::remote::Transfer dummy;
    pimpl->channels.emplace_back(dummy);
    EXPECT_CALL(mock.transfer, attach(nullptr));
    channelManager.onClosed(mock.transfer);
    ASSERT_EQ(pimpl->channels.size(), static_cast<std::size_t>(1));
}

struct RemoteServer_TransferInList : public RemoteServer {
    void SetUp() override
    {
        pimpl->channels.emplace_back(mock.transfer);
    }
};

TEST_F(RemoteServer_TransferInList, onClosedAndChannelIsInList)
{
    EXPECT_CALL(mock.transfer, attach(nullptr));
    channelManager.onClosed(mock.transfer);
    ASSERT_TRUE(pimpl->channels.empty());
}

using RemoteServer_ServiceNotInList = RemoteServer;

TEST_F(RemoteServer_ServiceNotInList, add)
{
    server.add(mock.service);
    ASSERT_EQ(pimpl->services.size(), static_cast<std::size_t>(1));
    ASSERT_EQ(pimpl->services.front(), &mock.service);
}

TEST_F(RemoteServer_ServiceNotInList, remove)
{
    server.remove(mock.service);
    ASSERT_TRUE(pimpl->services.empty());
}

struct RemoteServer_ServiceInList : public RemoteServer {
    void SetUp() override
    {
        pimpl->services.emplace_back(&mock.service);
    }
};

TEST_F(RemoteServer_ServiceInList, add)
{
    ASSERT_DEATH(server.add(mock.service), "");
}

TEST_F(RemoteServer_ServiceInList, remove)
{
    server.remove(mock.service);
    ASSERT_TRUE(pimpl->services.empty());
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
