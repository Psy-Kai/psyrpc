#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <mock/ascendingnumbergenerator.h>
#include <mock/remote/transfer.h>
#include <psyrpc/impl/magic.h>
#include <psyrpc/impl/remote/channel.h>
#include <psyrpc/proto/operators.h>
#include <psyrpc/random.h>
#include <psyrpc/remote/channel.h>

namespace psyrpc {

struct RemoteChannel : public testing::Test {
    struct {
        testing::StrictMock<mock::remote::Transfer> transfer;
        testing::StrictMock<mock::AscendingNumberGenerator> flowControlGenerator;
    } mock;
    Random rand;
    const uint64_t expectedFlowControlValue = rand();
    psyrpc::impl::remote::Channel *impl =
        new psyrpc::impl::remote::Channel{mock.flowControlGenerator, mock.transfer};
    remote::Channel channel{std::unique_ptr<psyrpc::impl::remote::Channel>{impl}};
    std::chrono::milliseconds waitFor{std::rand()};
};

MATCHER_P(RpcEq, param, "Matches psyrpc::proto::Rpc objects")
{
    return proto::matcherImpl<proto::Rpc>(arg, param);
}

TEST_F(RemoteChannel, writeWithCallMessage)
{
    auto resultMessage = proto::Result{};

    resultMessage.set_callid(rand());

    auto expectedRpc = proto::Rpc{};
    expectedRpc.set_magicconst(magicConst);
    expectedRpc.set_flowcontrol(expectedFlowControlValue);
    *expectedRpc.mutable_result() = resultMessage;

    testing::InSequence seq;
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));
    EXPECT_CALL(mock.transfer, write(RpcEq(expectedRpc))).WillOnce(testing::Return(true));

    ASSERT_TRUE(channel.write(std::move(resultMessage)));
}

TEST_F(RemoteChannel, writeWithCallMessageButTransferFails)
{
    auto resultMessage = proto::Result{};

    resultMessage.set_callid(rand());

    auto expectedRpc = proto::Rpc{};
    expectedRpc.set_magicconst(magicConst);
    expectedRpc.set_flowcontrol(expectedFlowControlValue);
    *expectedRpc.mutable_result() = resultMessage;

    testing::InSequence seq;
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));
    EXPECT_CALL(mock.transfer, write(RpcEq(expectedRpc))).WillOnce(testing::Return(false));

    ASSERT_FALSE(channel.write(std::move(resultMessage)));
}

TEST_F(RemoteChannel, writeWithError)
{
    static constexpr auto expectedError = proto::Rpc::Error::Rpc_Error_NoActiveCall;

    auto expectedRpc = proto::Rpc{};
    expectedRpc.set_magicconst(magicConst);
    expectedRpc.set_flowcontrol(expectedFlowControlValue);
    expectedRpc.set_error(expectedError);

    testing::InSequence seq;
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));
    EXPECT_CALL(mock.transfer, write(RpcEq(expectedRpc))).WillOnce(testing::Return(true));

    channel.write(expectedError);
}

TEST_F(RemoteChannel, read)
{
    auto callMessage = proto::Call{};

    callMessage.set_id(rand());
    callMessage.mutable_parameter()->set_index(1);
    callMessage.mutable_parameter()->mutable_data()->set_value("Hello World");
    const auto expectedResultMessage = callMessage;

    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(expectedFlowControlValue);
    *rpc.mutable_call() = std::move(callMessage);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));

    callMessage = {};
    ASSERT_TRUE(channel.read(callMessage, waitFor));
    ASSERT_TRUE(callMessage == expectedResultMessage);
}

TEST_F(RemoteChannel, readButTransferFails)
{
    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor)).WillOnce(testing::Return(false));

    auto callMessage = proto::Call{};
    ASSERT_FALSE(channel.read(callMessage, waitFor));
}

TEST_F(RemoteChannel, readButWrongMagicConst)
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(0);
    rpc.set_flowcontrol(expectedFlowControlValue);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));

    auto callMessage = proto::Call{};
    ASSERT_FALSE(channel.read(callMessage, waitFor));
}

TEST_F(RemoteChannel, readButWrongFlowControlValue)
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(expectedFlowControlValue + 1);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));

    auto callMessage = proto::Call{};
    ASSERT_FALSE(channel.read(callMessage, waitFor));
}

TEST_F(RemoteChannel, readButContainsNoCallMessage)
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(expectedFlowControlValue);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue + 1));

    auto callMessage = proto::Call{};
    ASSERT_FALSE(channel.read(callMessage, waitFor));
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
