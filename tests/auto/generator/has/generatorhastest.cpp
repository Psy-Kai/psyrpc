#include <cinttypes>

#include <xxhash.h>
#include <gmock/gmock.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/io/printer.h>
#include <gtest/gtest.h>
#include <nonstd/string_view.hpp>

#include <psyrpc/generator/has.h>

namespace psyrpc {

constexpr auto packageName = nonstd::string_view{"a.b.c"};
constexpr auto serviceName = nonstd::string_view{"Foobar"};
constexpr auto bufferSize = 0x1000;

class GeneratorHasBase : public testing::Test {
protected:
    void SetUp() override
    {
        descriptorPool.AllowUnknownDependencies();

        google::protobuf::FileDescriptorProto file;
        file.set_name("");
        file.set_package(packageName.data());
        auto &service = *file.mutable_service()->Add();
        service.set_name(serviceName.data());

        auto *method = service.mutable_method()->Add();
        method->set_name("foo");
        method->set_input_type("Param");
        method->set_output_type("Result");
        method = service.mutable_method()->Add();
        method->set_name("bar");
        method->set_input_type("Param");
        method->set_output_type("Result");
        method = service.mutable_method()->Add();
        method->set_name("foobar");
        method->set_input_type("Param");
        method->set_output_type("Result");

        this->file = descriptorPool.BuildFile(file);
        this->service = this->file->service(0);
    }

    std::unique_ptr<google::protobuf::io::Printer> makePrinter()
    {
        return std::unique_ptr<google::protobuf::io::Printer>(
            new google::protobuf::io::Printer{&stream, '%'});
    }

    std::string text;
    google::protobuf::io::StringOutputStream stream{&text};
    google::protobuf::DescriptorPool descriptorPool;
    const google::protobuf::FileDescriptor *file = nullptr;
    const google::protobuf::ServiceDescriptor *service = nullptr;
    const XXH32_hash_t serviceId = makeServiceId();

private:
    static XXH32_hash_t makeServiceId()
    {
        const auto serviceNameWithPackage = std::string{packageName} + '.' + serviceName.data();
        return XXH32(serviceNameWithPackage.data(), serviceNameWithPackage.size(), 0);
    }
};

class GeneratorHasRemote : public GeneratorHasBase {
protected:
    generator::has::Remote remote;
};

TEST_F(GeneratorHasRemote, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "bool %sBase::has(const psyrpc::proto::XxHashedSignature &method)\n"
                  "{\n"
                  "    switch (method.value()) {\n"
                  "        case %" PRIu32 ":\n"
                  "            return method.customidtoavoidcollision() == %" PRIu32 ";\n"
                  "        case %" PRIu32 ":\n"
                  "            return method.customidtoavoidcollision() == %" PRIu32 ";\n"
                  "        case %" PRIu32 ":\n"
                  "            return method.customidtoavoidcollision() == %" PRIu32 ";\n"
                  "    }\n"
                  "    return false;\n"
                  "}\n",
                  service->name().data(),
                  XXH32(service->method(0)->name().data(), service->method(0)->name().size(), 0), 0,
                  XXH32(service->method(1)->name().data(), service->method(1)->name().size(), 0), 0,
                  XXH32(service->method(2)->name().data(), service->method(2)->name().size(), 0),
                  0);
    remote.generate(*service, *makePrinter());
    ASSERT_STREQ(text.data(), expected.data());
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
