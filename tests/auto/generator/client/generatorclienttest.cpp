#include <gmock/gmock.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/io/printer.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <gtest/gtest.h>

#include <psyrpc/generator/client.h>
#include <psyrpc/generator/utilities.h>

namespace psyrpc {
namespace mock {
namespace section {

class File : public generator::section::FileInterface {
public:
    MOCK_METHOD(void, generate,
                (const google::protobuf::FileDescriptor &, google::protobuf::io::Printer &),
                (const, override));
};

} // namespace section

class Context : public google::protobuf::compiler::GeneratorContext {
public:
    MOCK_METHOD(google::protobuf::io::ZeroCopyOutputStream *, Open, (const std::string &filename),
                (override));
};

} // namespace mock

class GeneratorClient : public testing::Test {
protected:
    void SetUp() override
    {
        descriptorPool.AllowUnknownDependencies();

        google::protobuf::FileDescriptorProto protoFile;
        protoFile.set_name("Hello World.proto");

        this->file = descriptorPool.BuildFile(protoFile);
    }

    testing::StrictMock<mock::section::File> header;
    testing::StrictMock<mock::section::File> source;
    generator::Client client{header, source};
    std::string text;
    google::protobuf::DescriptorPool descriptorPool;
    const google::protobuf::FileDescriptor *file = nullptr;
    testing::StrictMock<mock::Context> context;
};

TEST_F(GeneratorClient, generate)
{
    testing::InSequence seq;
    const auto baseName = std::string{generator::utilities::baseName(file->name())};
    EXPECT_CALL(context, Open(baseName + ".psyrpc_client.h"))
        .WillOnce(testing::Return(new google::protobuf::io::StringOutputStream{&text}));
    EXPECT_CALL(header, generate(testing::Ref(*file), testing::_));
    EXPECT_CALL(context, Open(baseName + ".psyrpc_client.cpp"))
        .WillOnce(testing::Return(new google::protobuf::io::StringOutputStream{&text}));
    EXPECT_CALL(source, generate(testing::Ref(*file), testing::_));

    std::string error;
    ASSERT_TRUE(client.Generate(file, {}, &context, &error));
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
