#include <gmock/gmock.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/io/printer.h>
#include <gtest/gtest.h>
#include <nonstd/string_view.hpp>

#include <psyrpc/generator/file.h>

namespace psyrpc {
namespace mock {
namespace section {

class File : public generator::section::FileInterface {
public:
    MOCK_METHOD(void, generate,
                (const google::protobuf::FileDescriptor &file,
                 google::protobuf::io::Printer &printer),
                (const, override));
};

} // namespace section
} // namespace mock

constexpr auto fileName = nonstd::string_view{"foo"};
namespace dependency {
constexpr auto simple = nonstd::string_view{"simple"};
constexpr auto complex = nonstd::string_view{"comp/lex"};
} // namespace dependency
namespace extension {
constexpr auto proto = nonstd::string_view{".proto"};
constexpr auto pbHeader = nonstd::string_view{".pb.h"};
constexpr auto psyrpcClientHeader = nonstd::string_view{".psyrpc_client.h"};
constexpr auto psyrpcRemoteHeader = nonstd::string_view{".psyrpc_remote.h"};
} // namespace extension
constexpr auto bufferSize = 0x1000;

class GeneratorFile : public testing::Test {
protected:
    void SetUp() override
    {
        descriptorPool.AllowUnknownDependencies();

        google::protobuf::FileDescriptorProto file;
        file.set_name(std::string{fileName.data()} + extension::proto.data());
        file.mutable_dependency()->Add(std::string{dependency::simple.data()} +
                                       extension::proto.data());
        file.mutable_dependency()->Add(std::string{dependency::complex.data()} +
                                       extension::proto.data());
        this->file = descriptorPool.BuildFile(file);
    }

    std::unique_ptr<google::protobuf::io::Printer> makePrinter()
    {
        return std::unique_ptr<google::protobuf::io::Printer>(
            new google::protobuf::io::Printer{&stream, '%'});
    }

    std::string text;
    google::protobuf::io::StringOutputStream stream{&text};
    google::protobuf::DescriptorPool descriptorPool;
    const google::protobuf::FileDescriptor *file = nullptr;
    testing::StrictMock<mock::section::File> package;
};

class GeneratorFileClientHeader : public GeneratorFile {
protected:
    generator::file::client::Header header{package};
};

TEST_F(GeneratorFileClientHeader, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "#include <psyrpc/client/servicebase.h>\n"
                  "\n"
                  "#include <chrono>\n"
                  "\n"
                  "#include <%s%s>\n"
                  "#include <%s%s>\n"
                  "\n"
                  "#include \"%s%s\"\n"
                  "\n",
                  dependency::simple.data(), extension::pbHeader.data(), dependency::complex.data(),
                  extension::pbHeader.data(), fileName.data(), extension::pbHeader.data());
    auto printer = makePrinter();
    EXPECT_CALL(package, generate(testing::Ref(*file), testing::Ref(*printer)));
    header.generate(*file, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorFileClientSource : public GeneratorFile {
protected:
    generator::file::client::Source source{package};
};

TEST_F(GeneratorFileClientSource, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "#include \"%s%s\"\n"
                  "\n",
                  fileName.data(), extension::psyrpcClientHeader.data());
    auto printer = makePrinter();
    EXPECT_CALL(package, generate(testing::Ref(*file), testing::Ref(*printer)));
    source.generate(*file, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorFileRemoteHeader : public GeneratorFile {
protected:
    generator::file::remote::Header header{package};
};

TEST_F(GeneratorFileRemoteHeader, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "#include <psyrpc/remote/servicebase.h>\n"
                  "#include <psyrpc/proto/types%s>\n"
                  "\n"
                  "#include <%s%s>\n"
                  "#include <%s%s>\n"
                  "\n"
                  "#include \"%s%s\"\n"
                  "\n",
                  extension::pbHeader.data(), dependency::simple.data(), extension::pbHeader.data(),
                  dependency::complex.data(), extension::pbHeader.data(), fileName.data(),
                  extension::pbHeader.data());
    auto printer = makePrinter();
    EXPECT_CALL(package, generate(testing::Ref(*file), testing::Ref(*printer)));
    header.generate(*file, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorFileRemoteSource : public GeneratorFile {
protected:
    generator::file::remote::Source source{package};
};

TEST_F(GeneratorFileRemoteSource, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "#include \"%s%s\"\n"
                  "\n",
                  fileName.data(), extension::psyrpcRemoteHeader.data());
    auto printer = makePrinter();
    EXPECT_CALL(package, generate(testing::Ref(*file), testing::Ref(*printer)));
    source.generate(*file, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
