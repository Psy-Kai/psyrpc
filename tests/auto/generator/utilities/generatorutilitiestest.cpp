#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <psyrpc/generator/utilities.h>

namespace psyrpc {

class GeneratorUtilitiesNormalize : public testing::Test {};

TEST_F(GeneratorUtilitiesNormalize, globalMessage)
{
    ASSERT_STREQ(generator::utilities::normalized("A").data(), "A");
}

TEST_F(GeneratorUtilitiesNormalize, packagedMassage)
{
    auto proto = std::string{"A"};
    auto cpp = std::string{"A"};
    for (std::size_t i = 0; i < 100; ++i) {
        proto = std::string{"a."} + proto;
        cpp = std::string{"a::"} + cpp;
        ASSERT_EQ(generator::utilities::normalized(proto).data(), cpp);
    }
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
