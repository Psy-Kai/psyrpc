#include <array>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/io/printer.h>
#include <psyrpc/generator/package.h>
#include <nonstd/string_view.hpp>

namespace psyrpc {
namespace mock {
namespace section {

class File : public generator::section::FileInterface {
public:
    MOCK_METHOD(void, generate,
                (const google::protobuf::FileDescriptor &file,
                 google::protobuf::io::Printer &printer),
                (const, override));
};

class Service : public generator::section::ServiceInterface {
public:
    MOCK_METHOD(void, generate,
                (const google::protobuf::ServiceDescriptor &file,
                 google::protobuf::io::Printer &printer),
                (const, override));
};

} // namespace section
} // namespace mock

constexpr auto package = std::array<nonstd::string_view, 3>{
    nonstd::string_view{"a"}, nonstd::string_view{"b"}, nonstd::string_view{"c"}};
constexpr auto bufferSize = 0x1000;

class GeneratorPackageBase : public testing::Test {
protected:
    void SetUp() override
    {
        descriptorPool.AllowUnknownDependencies();

        google::protobuf::FileDescriptorProto file;
        file.set_name("");
        std::for_each(std::begin(package), std::end(package), [&file](nonstd::string_view item) {
            if (file.package().empty())
                file.mutable_package()->reserve(package.size() * 2);
            else
                *file.mutable_package() += '.';
            file.mutable_package()->append({std::begin(item), std::end(item)});
        });

        this->file = descriptorPool.BuildFile(file);
    }

    std::string text;
    google::protobuf::io::StringOutputStream stream{&text};
    google::protobuf::DescriptorPool descriptorPool;
    const google::protobuf::FileDescriptor *file = nullptr;

    std::unique_ptr<google::protobuf::io::Printer> makePrinter()
    {
        return std::unique_ptr<google::protobuf::io::Printer>(
            new google::protobuf::io::Printer{&stream, '%'});
    }
};

class GeneratorPackageNamespaceStart : public GeneratorPackageBase {
protected:
    generator::package::Start start;
};

TEST_F(GeneratorPackageNamespaceStart, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "namespace %s {\n"
                  "namespace %s {\n"
                  "namespace %s {\n",
                  package.at(0).data(), package.at(1).data(), package.at(2).data());
    start.generate(*file, *makePrinter());
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorPackageNamespaceEnd : public GeneratorPackageBase {
protected:
    generator::package::End end;
};

TEST_F(GeneratorPackageNamespaceEnd, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "}\n"
                  "}\n"
                  "}\n");
    end.generate(*file, *makePrinter());
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorPackage : public GeneratorPackageBase {
protected:
    testing::StrictMock<mock::section::File> start;
    testing::StrictMock<mock::section::Service> service;
    testing::StrictMock<mock::section::File> end;
    generator::package::Package package{start, service, end};
};

TEST_F(GeneratorPackage, generate)
{
    testing::InSequence seq;
    auto printer = makePrinter();
    EXPECT_CALL(start, generate(testing::Ref(*file), testing::Ref(*printer)));
    for (int i = 0; i < file->service_count(); ++i) {
        const auto *service = file->service(i);
        EXPECT_CALL(this->service, generate(testing::Ref(*service), testing::Ref(*printer)));
    }
    EXPECT_CALL(end, generate(testing::Ref(*file), testing::Ref(*printer)));

    package.generate(*file, *printer);
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
