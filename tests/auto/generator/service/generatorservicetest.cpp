#include <cinttypes>

#include <xxhash.h>
#include <gmock/gmock.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/io/printer.h>
#include <gtest/gtest.h>
#include <nonstd/string_view.hpp>

#include <psyrpc/generator/service.h>

namespace psyrpc {
namespace mock {
namespace section {

class Service : public generator::section::ServiceInterface {
public:
    MOCK_METHOD(void, generate,
                (const google::protobuf::ServiceDescriptor &method,
                 google::protobuf::io::Printer &printer),
                (const, override));
};

class Method : public generator::section::MethodInterface {
public:
    MOCK_METHOD(void, generate,
                (const google::protobuf::MethodDescriptor &method,
                 google::protobuf::io::Printer &printer),
                (const, override));
};

} // namespace section
} // namespace mock

constexpr auto packageName = nonstd::string_view{"a.b.c"};
constexpr auto serviceName = nonstd::string_view{"Foobar"};
constexpr auto bufferSize = 0x1000;

class GeneratorServiceBase : public testing::Test {
protected:
    void SetUp() override
    {
        descriptorPool.AllowUnknownDependencies();

        google::protobuf::FileDescriptorProto file;
        file.set_name("");
        file.set_package(packageName.data());
        auto &service = *file.mutable_service()->Add();
        service.set_name(serviceName.data());

        auto *method = service.mutable_method()->Add();
        method->set_name("foo");
        method->set_input_type("Param");
        method->set_output_type("Result");
        method = service.mutable_method()->Add();
        method->set_name("bar");
        method->set_input_type("Param");
        method->set_output_type("Result");
        method = service.mutable_method()->Add();
        method->set_name("foobar");
        method->set_input_type("Param");
        method->set_output_type("Result");

        this->file = descriptorPool.BuildFile(file);
        this->service = this->file->service(0);
    }

    std::unique_ptr<google::protobuf::io::Printer> makePrinter()
    {
        return std::unique_ptr<google::protobuf::io::Printer>(
            new google::protobuf::io::Printer{&stream, '%'});
    }

    std::string text;
    google::protobuf::io::StringOutputStream stream{&text};
    google::protobuf::DescriptorPool descriptorPool;
    const google::protobuf::FileDescriptor *file = nullptr;
    const google::protobuf::ServiceDescriptor *service = nullptr;
    const XXH32_hash_t serviceId = makeServiceId();

private:
    static XXH32_hash_t makeServiceId()
    {
        const auto serviceNameWithPackage = std::string{packageName} + '.' + serviceName.data();
        return XXH32(serviceNameWithPackage.data(), serviceNameWithPackage.size(), 0);
    }
};

class GeneratorServiceClientHeaderStart : public GeneratorServiceBase {
protected:
    generator::service::client::header::Start start;
};

TEST_F(GeneratorServiceClientHeaderStart, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "class %s final : public psyrpc::client::ServiceBase {\n"
                  "public:\n"
                  "    explicit %s(psyrpc::client::ChannelInterface &channel);\n",
                  service->name().data(), service->name().data());
    start.generate(*service, *makePrinter());
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorServiceClientHeaderEnd : public GeneratorServiceBase {
protected:
    generator::service::client::header::End end;
};

TEST_F(GeneratorServiceClientHeaderEnd, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(),
                  "private:\n"
                  "    static constexpr auto serviceId = %" PRIu32 ";\n"
                  "    static constexpr auto "
                  "customServiceId = %" PRIu32 ";\n"
                  "};\n",
                  serviceId, 0);
    end.generate(*service, *makePrinter());
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorServiceClientHeader : public GeneratorServiceBase {
protected:
    testing::StrictMock<mock::section::Service> start;
    testing::StrictMock<mock::section::Method> method;
    testing::StrictMock<mock::section::Service> end;
    generator::service::client::Header header{start, method, end};
};

TEST_F(GeneratorServiceClientHeader, generate)
{
    testing::InSequence seq;
    auto printer = makePrinter();
    EXPECT_CALL(start, generate(testing::Ref(*service), testing::Ref(*printer)));
    for (int i = 0; i < service->method_count(); ++i) {
        const auto *method = service->method(i);
        EXPECT_CALL(this->method, generate(testing::Ref(*method), testing::Ref(*printer)));
    }
    EXPECT_CALL(end, generate(testing::Ref(*service), testing::Ref(*printer)));
    header.generate(*service, *printer);
}

class GeneratorServiceClientSource : public GeneratorServiceBase {
protected:
    testing::StrictMock<mock::section::Method> method;
    generator::service::client::Source source{method};
};

TEST_F(GeneratorServiceClientSource, generate)
{
    std::string expected(bufferSize, 0);
    auto bufferWritten = std::snprintf(&expected.front(), expected.size(),
                                       "%s::%s(psyrpc::client::ChannelInterface &channel) :\n"
                                       "    psyrpc::client::ServiceBase{channel}\n"
                                       "{}\n\n",
                                       service->name().data(), service->name().data());
    auto printer = makePrinter();
    testing::InSequence seq;
    for (int i = 0; i < service->method_count(); ++i) {
        const auto *method = service->method(i);
        EXPECT_CALL(this->method, generate(testing::Ref(*method), testing::Ref(*printer)));
        bufferWritten +=
            std::snprintf(&expected.front() + bufferWritten, expected.size() - bufferWritten, "\n");
    }
    source.generate(*service, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorServiceRemoteHeaderStart : public GeneratorServiceBase {
protected:
    generator::service::remote::header::Start start;
};

TEST_F(GeneratorServiceRemoteHeaderStart, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(
        &expected.front(), expected.size(),
        "class %sBase : public psyrpc::remote::ServiceBase<%" PRIu32 ", %" PRIu32 "> {\n"
        "private:\n"
        "    bool has(const psyrpc::proto::XxHashedSignature &method) "
        "override;\n"
        "    bool call(const psyrpc::proto::XxHashedSignature "
        "&method, psyrpc::common::call::Interface &call, const std::chrono::milliseconds waitFor) "
        "override;\n",
        service->name().data(), serviceId, 0);
    start.generate(*service, *makePrinter());
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorServiceRemoteHeaderEnd : public GeneratorServiceBase {
protected:
    generator::service::remote::header::End end;
};

TEST_F(GeneratorServiceRemoteHeaderEnd, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(&expected.front(), expected.size(), "};\n");
    end.generate(*service, *makePrinter());
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorServiceRemoteHeader : public GeneratorServiceBase {
protected:
    testing::StrictMock<mock::section::Service> start;
    testing::StrictMock<mock::section::Method> method;
    testing::StrictMock<mock::section::Service> end;
    generator::service::remote::Header header{start, method, end};
};

TEST_F(GeneratorServiceRemoteHeader, generate)
{
    testing::InSequence seq;
    auto printer = makePrinter();
    EXPECT_CALL(start, generate(testing::Ref(*service), testing::Ref(*printer)));
    for (int i = 0; i < service->method_count(); ++i) {
        const auto *method = service->method(i);
        EXPECT_CALL(this->method, generate(testing::Ref(*method), testing::Ref(*printer)));
    }
    EXPECT_CALL(end, generate(testing::Ref(*service), testing::Ref(*printer)));
    header.generate(*service, *printer);
}

class GeneratorServiceRemoteSource : public GeneratorServiceBase {
protected:
    testing::StrictMock<mock::section::Service> has;
    testing::StrictMock<mock::section::Service> call;
    generator::service::remote::Source source{has, call};
};

TEST_F(GeneratorServiceRemoteSource, generate)
{
    testing::InSequence seq;
    auto printer = makePrinter();
    EXPECT_CALL(has, generate(testing::Ref(*service), testing::Ref(*printer)));
    EXPECT_CALL(call, generate(testing::Ref(*service), testing::Ref(*printer)));
    source.generate(*service, *printer);
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
