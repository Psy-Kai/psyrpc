#include <cinttypes>

#include <xxhash.h>
#include <gmock/gmock.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/io/printer.h>
#include <gtest/gtest.h>
#include <nonstd/string_view.hpp>

#include <psyrpc/generator/call.h>

namespace psyrpc {

struct Method final {
    struct Type final {
        nonstd::string_view name;
        nonstd::string_view nameCpp;
        bool stream;
    };

    nonstd::string_view name;
    Type param;
    Type result;
};

namespace method {
constexpr auto simple =
    Method{"simple", {"msg.Param", "msg::Param", false}, {".msg.Result", "msg::Result", false}};
constexpr auto streamParam = Method{"streamParam",
                                    {"StreamingParam", "StreamingParam", true},
                                    {".msg.Result", "msg::Result", false}};
constexpr auto streamResult = Method{"streamResult",
                                     {"msg.Param", "msg::Param", false},
                                     {".msg.StreamingResult", "msg::StreamingResult", true}};
constexpr auto streamAll = Method{"streamAll",
                                  {"msg.Param", "msg::Param", true},
                                  {".msg.StreamingResult", "msg::StreamingResult", true}};
} // namespace method

constexpr auto bufferSize = 0x1000;

class GeneratorCall : public testing::Test {
protected:
    void SetUp() override
    {
        descriptorPool.AllowUnknownDependencies();

        google::protobuf::FileDescriptorProto file;
        file.set_name("");
        auto &service = *file.mutable_service()->Add();
        service.set_name("DummyName");

        build(*service.mutable_method()->Add(), method::simple);
        build(*service.mutable_method()->Add(), method::streamParam);
        build(*service.mutable_method()->Add(), method::streamResult);
        build(*service.mutable_method()->Add(), method::streamAll);

        this->file = descriptorPool.BuildFile(file);
        this->service = this->file->service(0);
    }

    std::unique_ptr<google::protobuf::io::Printer> makePrinter()
    {
        return std::unique_ptr<google::protobuf::io::Printer>(
            new google::protobuf::io::Printer{&stream, '%'});
    }

    std::string text;
    google::protobuf::io::StringOutputStream stream{&text};
    google::protobuf::DescriptorPool descriptorPool;
    const google::protobuf::FileDescriptor *file = nullptr;
    const google::protobuf::ServiceDescriptor *service = nullptr;

private:
    static void build(google::protobuf::MethodDescriptorProto &protoMethod, const Method &method)
    {
        protoMethod.set_name(method.name.data());
        protoMethod.set_input_type(method.param.name.data());
        protoMethod.set_client_streaming(method.param.stream);
        protoMethod.set_output_type(method.result.name.data());
        protoMethod.set_server_streaming(method.result.stream);
    }
};

class GeneratorCallRemote : public GeneratorCall {
protected:
    generator::call::Remote remote;
};

TEST_F(GeneratorCallRemote, generate)
{
    std::string expected(bufferSize, 0);
    std::snprintf(
        &expected.front(), expected.size(),
        "bool %sBase::call(const psyrpc::proto::XxHashedSignature &method,\n"
        "    psyrpc::common::call::Interface &call, const std::chrono::milliseconds waitFor)\n"
        "{\n"
        "    assert(has(method));\n"
        "    (void)waitFor;\n"
        "\n"
        "    if (method.value() == %" PRIu32 ") {\n"
        "        auto parameterStream =\n"
        "            psyrpc::remote::detail::stream::makeParameter<%s>(call);\n"
        "        auto resultStream = psyrpc::remote::detail::stream::makeResult<%s>(call);\n"
        "        auto parameter = %s{};\n"
        "        if (!parameterStream.read(parameter, waitFor))\n"
        "            return false;\n"
        "        auto result = %s{};\n"
        "        if (%s(parameter, result) != Rpc::Success)\n"
        "            return false;\n"
        "        return resultStream.write(result, Eot{});\n"
        "    }\n"
        "    if (method.value() == %" PRIu32 ") {\n"
        "        auto result = %s{};\n"
        "        auto resultStream = psyrpc::remote::detail::stream::makeResult<%s>(call);\n"
        "        if (%s(psyrpc::remote::detail::stream::makeParameter<%s>(call), result) != "
        "Rpc::Success)\n"
        "            return false;\n"
        "        return resultStream.write(result, Eot{});\n"
        "    }\n"
        "    if (method.value() == %" PRIu32 ") {\n"
        "        auto parameterStream =\n"
        "            psyrpc::remote::detail::stream::makeParameter<%s>(call);\n"
        "        auto resultStream = psyrpc::remote::detail::stream::makeResult<%s>(call);\n"
        "        auto parameter = %s{};\n"
        "        if (!parameterStream.read(parameter, waitFor))\n"
        "            return false;\n"
        "        return %s(parameter, std::move(resultStream)) == Rpc::Success;\n"
        "    }\n"
        "    if (method.value() == %" PRIu32 ") {\n"
        "        return %s(psyrpc::remote::detail::stream::makeParameter<%s>(call),\n"
        "            psyrpc::remote::detail::stream::makeResult<%s>(call)) == Rpc::Success;\n"
        "    }\n"
        "\n"
        "    return false;\n"
        "}\n",
        service->name().data(), XXH32(method::simple.name.data(), method::simple.name.size(), 0),
        method::simple.param.nameCpp.data(), method::simple.result.nameCpp.data(),
        method::simple.param.nameCpp.data(), method::simple.result.nameCpp.data(),
        method::simple.name.data(),
        XXH32(method::streamParam.name.data(), method::streamParam.name.size(), 0),
        method::streamParam.result.nameCpp.data(), method::streamParam.result.nameCpp.data(),
        method::streamParam.name.data(), method::streamParam.param.nameCpp.data(),
        XXH32(method::streamResult.name.data(), method::streamResult.name.size(), 0),
        method::streamResult.param.nameCpp.data(), method::streamResult.result.nameCpp.data(),
        method::streamResult.param.nameCpp.data(), method::streamResult.name.data(),
        XXH32(method::streamAll.name.data(), method::streamAll.name.size(), 0),
        method::streamAll.name.data(), method::streamAll.param.nameCpp.data(),
        method::streamAll.result.nameCpp.data());
    auto printer = makePrinter();
    remote.generate(*service, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
