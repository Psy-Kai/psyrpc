#include <cinttypes>

#include <xxhash.h>
#include <gmock/gmock.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/io/printer.h>
#include <gtest/gtest.h>
#include <nonstd/string_view.hpp>

#include <psyrpc/generator/method.h>

namespace psyrpc {

struct Method final {
    struct Type final {
        nonstd::string_view name;
        nonstd::string_view nameCpp;
        bool stream;
    };

    nonstd::string_view name;
    Type param;
    Type result;
};

namespace method {
constexpr auto simple =
    Method{"simple", {"msg.Param", "msg::Param", false}, {".msg.Result", "msg::Result", false}};
constexpr auto streamParam = Method{"streamParam",
                                    {"StreamingParam", "StreamingParam", true},
                                    {".msg.Result", "msg::Result", false}};
constexpr auto streamResult = Method{"streamResult",
                                     {"msg.Param", "msg::Param", false},
                                     {".msg.StreamingResult", "msg::StreamingResult", true}};
constexpr auto streamAll = Method{"streamAll",
                                  {"msg.Param", "msg::Param", true},
                                  {".msg.StreamingResult", "msg::StreamingResult", true}};
} // namespace method

constexpr auto bufferSize = 0x1000;

class GeneratorMethod : public testing::Test {
protected:
    void SetUp() override
    {
        descriptorPool.AllowUnknownDependencies();
    }

    const google::protobuf::MethodDescriptor *build(const Method &method)
    {
        google::protobuf::FileDescriptorProto file;
        file.set_name("");
        auto &service = *file.mutable_service()->Add();
        service.set_name("DummyName");

        auto &protoMethod = *service.mutable_method()->Add();
        protoMethod.set_name(method.name.data());
        protoMethod.set_input_type(method.param.name.data());
        protoMethod.set_client_streaming(method.param.stream);
        protoMethod.set_output_type(method.result.name.data());
        protoMethod.set_server_streaming(method.result.stream);

        this->file = descriptorPool.BuildFile(file);
        this->service = this->file->service(0);
        return this->service->method(0);
    }

    std::unique_ptr<google::protobuf::io::Printer> makePrinter()
    {
        return std::unique_ptr<google::protobuf::io::Printer>(
            new google::protobuf::io::Printer{&stream, '%'});
    }

    std::string text;
    google::protobuf::io::StringOutputStream stream{&text};
    google::protobuf::DescriptorPool descriptorPool;
    const google::protobuf::FileDescriptor *file = nullptr;
    const google::protobuf::ServiceDescriptor *service = nullptr;
};

class GeneratorMethodClientHeader : public GeneratorMethod {
protected:
    generator::method::client::Header header;
};

TEST_F(GeneratorMethodClientHeader, simple)
{
    std::string expected(bufferSize, 0);
    const auto &simple = method::simple;
    const auto *method = build(simple);
    std::snprintf(
        &expected.front(), expected.size(),
        "    nonstd::optional<psyrpc::client::Result<%s>>\n"
        "    %s(const %s &paramMsg,\n"
        "        const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max());\n",
        simple.result.nameCpp.data(), simple.name.data(), simple.param.nameCpp.data());
    auto printer = makePrinter();
    header.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodClientHeader, streamParam)
{
    std::string expected(bufferSize, 0);
    const auto &streamParam = method::streamParam;
    const auto *method = build(streamParam);
    std::snprintf(&expected.front(), expected.size(),
                  "    nonstd::optional<psyrpc::client::Result<%s>>\n"
                  "    %s(psyrpc::client::stream::Parameter<const %s> &paramStream,\n"
                  "        const std::chrono::milliseconds waitFor = "
                  "std::chrono::milliseconds::max());\n",
                  streamParam.result.nameCpp.data(), streamParam.name.data(),
                  streamParam.param.nameCpp.data());
    auto printer = makePrinter();
    header.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodClientHeader, streamResult)
{
    std::string expected(bufferSize, 0);
    const auto &streamResult = method::streamResult;
    const auto *method = build(streamResult);
    std::snprintf(
        &expected.front(), expected.size(),
        "    nonstd::optional<psyrpc::client::stream::Result<%s>>\n"
        "    %s(const %s &paramMsg,\n"
        "        const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max());\n",
        streamResult.result.nameCpp.data(), streamResult.name.data(),
        streamResult.param.nameCpp.data());
    auto printer = makePrinter();
    header.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodClientHeader, streamAll)
{
    std::string expected(bufferSize, 0);
    const auto &streamAll = method::streamAll;
    const auto *method = build(streamAll);
    std::snprintf(&expected.front(), expected.size(),
                  "    nonstd::optional<psyrpc::client::stream::Result<%s>>\n"
                  "    %s(psyrpc::client::stream::Parameter<const %s> &paramStream,\n"
                  "        const std::chrono::milliseconds waitFor = "
                  "std::chrono::milliseconds::max());\n",
                  streamAll.result.nameCpp.data(), streamAll.name.data(),
                  streamAll.param.nameCpp.data());
    auto printer = makePrinter();
    header.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorMethodClientSource : public GeneratorMethod {
protected:
    generator::method::client::Source source;
};

TEST_F(GeneratorMethodClientSource, simple)
{
    std::string expected(bufferSize, 0);
    const auto &simple = method::simple;
    const auto *method = build(simple);
    std::snprintf(&expected.front(), expected.size(),
                  "nonstd::optional<psyrpc::client::Result<%s>>\n"
                  "%s::%s(const %s &paramMsg,\n"
                  "    const std::chrono::milliseconds waitFor)\n"
                  "{\n"
                  "    static constexpr auto methodId = %" PRIu32 ";\n"
                  "    static constexpr auto customMethodId = %" PRIu32 ";\n"
                  "\n"
                  "    auto paramStream = psyrpc::client::stream::Parameter<const %s>{};\n"
                  "    auto resultStream =\n"
                  "        call<%s, psyrpc::client::servicebase::result::unary>(serviceId, "
                  "customServiceId, methodId, customMethodId, "
                  "paramStream, waitFor);\n"
                  "    if (!resultStream || !paramStream.write(paramMsg, psyrpc::Eot{}))\n"
                  "        return {};\n"
                  "    return resultStream;\n"
                  "}\n",
                  simple.result.nameCpp.data(), service->name().data(), simple.name.data(),
                  simple.param.nameCpp.data(), XXH32(simple.name.data(), simple.name.size(), 0), 0,
                  simple.param.nameCpp.data(), simple.result.nameCpp.data());
    auto printer = makePrinter();
    source.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodClientSource, streamParam)
{
    std::string expected(bufferSize, 0);
    const auto &streamParam = method::streamParam;
    const auto *method = build(streamParam);
    std::snprintf(&expected.front(), expected.size(),
                  "nonstd::optional<psyrpc::client::Result<%s>>\n"
                  "%s::%s(psyrpc::client::stream::Parameter<const %s> &paramStream,\n"
                  "    const std::chrono::milliseconds waitFor)\n"
                  "{\n"
                  "    static constexpr auto methodId = %" PRIu32 ";\n"
                  "    static constexpr auto customMethodId = %" PRIu32 ";\n"
                  "\n"
                  "    return call<%s, psyrpc::client::servicebase::result::unary>(serviceId, "
                  "customServiceId, methodId, customMethodId, "
                  "paramStream, waitFor);\n"
                  "}\n",
                  streamParam.result.nameCpp.data(), service->name().data(),
                  streamParam.name.data(), streamParam.param.nameCpp.data(),
                  XXH32(streamParam.name.data(), streamParam.name.size(), 0), 0,
                  streamParam.result.nameCpp.data());
    auto printer = makePrinter();
    source.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodClientSource, streamResult)
{
    std::string expected(bufferSize, 0);
    const auto &streamResult = method::streamResult;
    const auto *method = build(streamResult);
    std::snprintf(&expected.front(), expected.size(),
                  "nonstd::optional<psyrpc::client::stream::Result<%s>>\n"
                  "%s::%s(const %s &paramMsg,\n"
                  "    const std::chrono::milliseconds waitFor)\n"
                  "{\n"
                  "    static constexpr auto methodId = %" PRIu32 ";\n"
                  "    static constexpr auto customMethodId = %" PRIu32 ";\n"
                  "\n"
                  "    auto paramStream = psyrpc::client::stream::Parameter<const %s>{};\n"
                  "    auto resultStream =\n"
                  "        call<%s, psyrpc::client::servicebase::result::stream>(serviceId, "
                  "customServiceId, methodId, customMethodId, "
                  "paramStream, waitFor);\n"
                  "    if (!resultStream || !paramStream.write(paramMsg, psyrpc::Eot{}))\n"
                  "        return {};\n"
                  "    return resultStream;\n"
                  "}\n",
                  streamResult.result.nameCpp.data(), service->name().data(),
                  streamResult.name.data(), streamResult.param.nameCpp.data(),
                  XXH32(streamResult.name.data(), streamResult.name.size(), 0), 0,
                  streamResult.param.nameCpp.data(), streamResult.result.nameCpp.data());
    auto printer = makePrinter();
    source.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodClientSource, streamAll)
{
    std::string expected(bufferSize, 0);
    const auto &streamAll = method::streamAll;
    const auto *method = build(streamAll);
    std::snprintf(&expected.front(), expected.size(),
                  "nonstd::optional<psyrpc::client::stream::Result<%s>>\n"
                  "%s::%s(psyrpc::client::stream::Parameter<const %s> &paramStream,\n"
                  "    const std::chrono::milliseconds waitFor)\n"
                  "{\n"
                  "    static constexpr auto methodId = %" PRIu32 ";\n"
                  "    static constexpr auto customMethodId = %" PRIu32 ";\n"
                  "\n"
                  "    return call<%s, psyrpc::client::servicebase::result::stream>(serviceId, "
                  "customServiceId, methodId, customMethodId, "
                  "paramStream, waitFor);\n"
                  "}\n",
                  streamAll.result.nameCpp.data(), service->name().data(), streamAll.name.data(),
                  streamAll.param.nameCpp.data(),
                  XXH32(streamAll.name.data(), streamAll.name.size(), 0), 0,
                  streamAll.result.nameCpp.data());
    auto printer = makePrinter();
    source.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

class GeneratorMethodRemote : public GeneratorMethod {
protected:
    generator::method::Remote remote;
};

TEST_F(GeneratorMethodRemote, simple)
{
    std::string expected(bufferSize, 0);
    const auto &simple = method::simple;
    const auto *method = build(simple);
    std::snprintf(&expected.front(), expected.size(),
                  "    virtual Rpc %s(const %s &paramMsg, %s &resultMsg) = 0;\n",
                  simple.name.data(), simple.param.nameCpp.data(), simple.result.nameCpp.data());
    auto printer = makePrinter();
    remote.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodRemote, streamParam)
{
    std::string expected(bufferSize, 0);
    const auto &streamParam = method::streamParam;
    const auto *method = build(streamParam);
    std::snprintf(&expected.front(), expected.size(),
                  "    virtual Rpc %s(psyrpc::remote::stream::Parameter<%s> paramStream,\n"
                  "        %s &resultMsg) = 0;\n",
                  streamParam.name.data(), streamParam.param.nameCpp.data(),
                  streamParam.result.nameCpp.data());
    auto printer = makePrinter();
    remote.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodRemote, streamResult)
{
    std::string expected(bufferSize, 0);
    const auto &streamResult = method::streamResult;
    const auto *method = build(streamResult);
    std::snprintf(&expected.front(), expected.size(),
                  "    virtual Rpc %s(const %s &paramMsg,\n"
                  "        psyrpc::remote::stream::Result<%s> resultStream) = 0;\n",
                  streamResult.name.data(), streamResult.param.nameCpp.data(),
                  streamResult.result.nameCpp.data());
    auto printer = makePrinter();
    remote.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

TEST_F(GeneratorMethodRemote, streamAll)
{
    std::string expected(bufferSize, 0);
    const auto &streamAll = method::streamAll;
    const auto *method = build(streamAll);
    std::snprintf(&expected.front(), expected.size(),
                  "    virtual Rpc %s(psyrpc::remote::stream::Parameter<%s> paramStream,\n"
                  "        psyrpc::remote::stream::Result<%s> resultStream) = 0;\n",
                  streamAll.name.data(), streamAll.param.nameCpp.data(),
                  streamAll.result.nameCpp.data());
    auto printer = makePrinter();
    remote.generate(*method, *printer);
    ASSERT_STREQ(text.data(), expected.data());
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
