#include <random>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <mock/client/channel.h>
#include <psyrpc/client/call.h>
#include <psyrpc/proto/operators.h>
#include <psyrpc/random.h>

namespace psyrpc {

struct ClientCall : public testing::Test {
    struct {
        testing::StrictMock<mock::client::Channel> channel;
    } mock;
    const std::size_t callId = static_cast<std::size_t>(std::rand());
    client::Call call{callId, mock.channel};
    const uint32_t serviceId = std::rand();
    const uint32_t customServiceId = std::rand();
    const uint32_t methodId = std::rand();
    const uint32_t customMethodId = std::rand();
    const uint64_t parameterIndex = std::rand();
    const std::string data{"Hello World"};
    const std::chrono::milliseconds waitFor{std::rand()};
};

MATCHER_P(CallEq, param, "Matches psyrpc::proto::Call objects")
{
    return proto::matcherImpl<proto::Call>(arg, param);
}

MATCHER_P(ResultEq, param, "Matches psyrpc::proto::Result objects")
{
    return proto::matcherImpl<proto::Result>(arg, param);
}

MATCHER_P(PackageEq, param, "Matches psyrpc::proto::Package objects")
{
    return proto::matcherImpl<proto::Package>(arg, param);
}

TEST_F(ClientCall, init)
{
    testing::InSequence seq;

    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    auto &init = *callMessage.mutable_init();
    init.mutable_service()->set_value(serviceId);
    init.mutable_service()->set_customidtoavoidcollision(customServiceId);
    init.mutable_method()->set_value(methodId);
    init.mutable_method()->set_customidtoavoidcollision(customMethodId);

    EXPECT_CALL(mock.channel, write(CallEq(callMessage))).WillOnce(testing::Return(true));

    auto result = proto::Result{};
    result.set_callid(callId);
    result.set_init(true);
    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([&result](proto::Result &resultMessage, const std::chrono::milliseconds) {
            resultMessage = result;
            return true;
        });

    ASSERT_TRUE(call.init(serviceId, customServiceId, methodId, customMethodId, waitFor));
}

TEST_F(ClientCall, initButChannelWriteFails)
{
    testing::InSequence seq;

    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    auto &init = *callMessage.mutable_init();
    init.mutable_service()->set_value(serviceId);
    init.mutable_service()->set_customidtoavoidcollision(customServiceId);
    init.mutable_method()->set_value(methodId);
    init.mutable_method()->set_customidtoavoidcollision(customMethodId);

    EXPECT_CALL(mock.channel, write(CallEq(callMessage))).WillOnce(testing::Return(false));

    ASSERT_FALSE(call.init(serviceId, customServiceId, methodId, customMethodId, waitFor));
}

TEST_F(ClientCall, initButChannelReadFails)
{
    testing::InSequence seq;

    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    auto &init = *callMessage.mutable_init();
    init.mutable_service()->set_value(serviceId);
    init.mutable_service()->set_customidtoavoidcollision(customServiceId);
    init.mutable_method()->set_value(methodId);
    init.mutable_method()->set_customidtoavoidcollision(customMethodId);

    EXPECT_CALL(mock.channel, write(CallEq(callMessage))).WillOnce(testing::Return(true));

    EXPECT_CALL(mock.channel, read(testing::_, waitFor)).WillOnce(testing::Return(false));

    ASSERT_FALSE(call.init(serviceId, customServiceId, methodId, customMethodId, waitFor));
}

TEST_F(ClientCall, initButWrongCallId)
{
    testing::InSequence seq;

    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    auto &init = *callMessage.mutable_init();
    init.mutable_service()->set_value(serviceId);
    init.mutable_service()->set_customidtoavoidcollision(customServiceId);
    init.mutable_method()->set_value(methodId);
    init.mutable_method()->set_customidtoavoidcollision(customMethodId);

    EXPECT_CALL(mock.channel, write(CallEq(callMessage))).WillOnce(testing::Return(true));

    auto result = proto::Result{};
    result.set_callid(callId + 1);
    result.set_init(true);
    EXPECT_CALL(mock.channel, read(testing::_, waitFor)).WillOnce(testing::Return(false));

    ASSERT_FALSE(call.init(serviceId, customServiceId, methodId, customMethodId, waitFor));
}

TEST_F(ClientCall, initButReceivedInitFalse)
{
    testing::InSequence seq;

    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    auto &init = *callMessage.mutable_init();
    init.mutable_service()->set_value(serviceId);
    init.mutable_service()->set_customidtoavoidcollision(customServiceId);
    init.mutable_method()->set_value(methodId);
    init.mutable_method()->set_customidtoavoidcollision(customMethodId);

    EXPECT_CALL(mock.channel, write(CallEq(callMessage))).WillOnce(testing::Return(true));

    auto result = proto::Result{};
    result.set_callid(callId);
    result.set_init(false);
    EXPECT_CALL(mock.channel, read(testing::_, waitFor)).WillOnce(testing::Return(false));

    ASSERT_FALSE(call.init(serviceId, customServiceId, methodId, customMethodId, waitFor));
}

TEST_F(ClientCall, write)
{
    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    callMessage.mutable_parameter()->set_index(parameterIndex);
    callMessage.mutable_parameter()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, write(CallEq(callMessage))).WillOnce(testing::Return(true));

    ASSERT_TRUE(call.write(proto::Package{callMessage.parameter()}));
}

TEST_F(ClientCall, writeButChannelWriteFails)
{
    auto callMessage = proto::Call{};
    callMessage.set_id(callId);
    callMessage.mutable_parameter()->set_index(parameterIndex);
    callMessage.mutable_parameter()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, write(CallEq(callMessage))).WillOnce(testing::Return(false));

    ASSERT_FALSE(call.write(proto::Package{callMessage.parameter()}));
}

TEST_F(ClientCall, read)
{
    auto resultMessage = proto::Result{};
    resultMessage.set_callid(callId);
    resultMessage.mutable_result()->set_index(parameterIndex);
    resultMessage.mutable_result()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([&resultMessage](proto::Result &result, const std::chrono::milliseconds) {
            result = resultMessage;
            return true;
        });

    auto package = proto::Package{};
    ASSERT_TRUE(call.read(package, waitFor));
    ASSERT_THAT(package, PackageEq(resultMessage.result()));
}

TEST_F(ClientCall, readButChannelReadFails)
{
    auto resultMessage = proto::Result{};
    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([&resultMessage](proto::Result &result, const std::chrono::milliseconds) {
            result = resultMessage;
            return false;
        });

    auto package = proto::Package{};
    ASSERT_FALSE(call.read(package, waitFor));
}

TEST_F(ClientCall, readButWrongCallId)
{
    auto resultMessage = proto::Result{};
    resultMessage.set_callid(callId + 1);
    resultMessage.mutable_result()->set_index(parameterIndex);
    resultMessage.mutable_result()->mutable_data()->set_value(data);

    EXPECT_CALL(mock.channel, read(testing::_, waitFor))
        .WillOnce([&resultMessage](proto::Result &result, const std::chrono::milliseconds) {
            result = resultMessage;
            return true;
        });

    auto package = proto::Package{};
    ASSERT_FALSE(call.read(package, waitFor));
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
