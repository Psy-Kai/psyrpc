#include <gmock/gmock.h>
#include <google/protobuf/empty.pb.h>
#include <gtest/gtest.h>

#include <mock/ascendingnumbergenerator.h>
#include <mock/client/call.h>
#include <mock/client/channel.h>
#include <psyrpc/client/servicebase.h>
#include <psyrpc/impl/client/servicebase.h>
#include <psyrpc/random.h>

namespace psyrpc {

using Message = google::protobuf::Empty;

namespace mock {

class ServiceBase : public impl::client::ServiceBase {
public:
    using impl::client::ServiceBase::ServiceBase;

    MOCK_METHOD(psyrpc::client::CallInterface &, createCall,
                (const std::size_t id, psyrpc::client::ChannelInterface &channel), (override));
};

} // namespace mock

struct ClientServiceCall : public testing::Test {
    struct {
        testing::StrictMock<::mock::AscendingNumberGenerator> ascendingNumberGenerator;
        testing::StrictMock<::mock::client::Channel> channel;
        testing::StrictMock<mock::ServiceBase> *impl = nullptr;
    } mock;

    const ServiceId serviceId = std::rand();
    const ServiceId customServiceId = std::rand();
    const MethodId methodId = std::rand();
    const MethodId customMethodId = std::rand();
    const std::chrono::milliseconds waitFor{std::rand()};

    psyrpc::client::ServiceBase service{createimpl()};

private:
    std::unique_ptr<psyrpc::impl::client::ServiceBase> createimpl()
    {
        mock.impl =
            new testing::StrictMock<mock::ServiceBase>{mock.ascendingNumberGenerator, mock.channel};
        return std::unique_ptr<psyrpc::impl::client::ServiceBase>{mock.impl};
    }
};

TEST_F(ClientServiceCall, callWithResultStream)
{
    testing::InSequence seq;

    constexpr auto nextCallId = 1;

    EXPECT_CALL(mock.ascendingNumberGenerator, next).WillOnce(testing::Return(nextCallId));
    testing::StrictMock<::mock::client::Call> call;
    EXPECT_CALL(*mock.impl, createCall(nextCallId, testing::Ref(mock.channel)))
        .WillOnce(testing::ReturnRef(call));
    EXPECT_CALL(call, init(serviceId, customServiceId, methodId, customMethodId, waitFor))
        .WillOnce(testing::Return(true));
    auto parameterStream = psyrpc::client::stream::Parameter<Message>{};
    nonstd::optional<client::stream::Result<Message>> result =
        service.call<Message, client::servicebase::result::stream>(
            serviceId, customServiceId, methodId, customMethodId, parameterStream, waitFor);
    ASSERT_TRUE(result.has_value());

    EXPECT_CALL(call, write);
    parameterStream.write(Message{});
    EXPECT_CALL(call, read(testing::_, waitFor));
    Message message;
    result->read(message, waitFor);
}

TEST_F(ClientServiceCall, callWithResultStreamButInitFailed)
{
    testing::InSequence seq;

    constexpr auto nextCallId = 1;

    EXPECT_CALL(mock.ascendingNumberGenerator, next).WillOnce(testing::Return(nextCallId));
    testing::StrictMock<::mock::client::Call> call;
    EXPECT_CALL(*mock.impl, createCall(nextCallId, testing::Ref(mock.channel)))
        .WillOnce(testing::ReturnRef(call));
    EXPECT_CALL(call, init(serviceId, customServiceId, methodId, customMethodId, waitFor))
        .WillOnce(testing::Return(false));
    auto parameterStream = psyrpc::client::stream::Parameter<Message>{};
    nonstd::optional<client::stream::Result<Message>> result =
        service.call<Message, client::servicebase::result::stream>(
            serviceId, customServiceId, methodId, customMethodId, parameterStream, waitFor);
    ASSERT_FALSE(result.has_value());
}

TEST_F(ClientServiceCall, callWithResultUnary)
{
    testing::InSequence seq;

    constexpr auto nextCallId = 1;

    EXPECT_CALL(mock.ascendingNumberGenerator, next).WillOnce(testing::Return(nextCallId));
    testing::StrictMock<::mock::client::Call> call;
    EXPECT_CALL(*mock.impl, createCall(nextCallId, testing::Ref(mock.channel)))
        .WillOnce(testing::ReturnRef(call));
    EXPECT_CALL(call, init(serviceId, customServiceId, methodId, customMethodId, waitFor))
        .WillOnce(testing::Return(true));
    auto parameterStream = psyrpc::client::stream::Parameter<Message>{};
    nonstd::optional<client::Result<Message>> result =
        service.call<Message, client::servicebase::result::unary>(
            serviceId, customServiceId, methodId, customMethodId, parameterStream, waitFor);
    ASSERT_TRUE(result.has_value());

    EXPECT_CALL(call, write);
    parameterStream.write(Message{});
    EXPECT_CALL(call, read(testing::_, waitFor));
    Message message;
    result->read(message, waitFor);
}

TEST_F(ClientServiceCall, callWithResultUnaryButInitFailed)
{
    testing::InSequence seq;

    constexpr auto nextCallId = 1;

    EXPECT_CALL(mock.ascendingNumberGenerator, next).WillOnce(testing::Return(nextCallId));
    testing::StrictMock<::mock::client::Call> call;
    EXPECT_CALL(*mock.impl, createCall(nextCallId, testing::Ref(mock.channel)))
        .WillOnce(testing::ReturnRef(call));
    EXPECT_CALL(call, init(serviceId, customServiceId, methodId, customMethodId, waitFor))
        .WillOnce(testing::Return(false));
    auto parameterStream = psyrpc::client::stream::Parameter<Message>{};
    nonstd::optional<client::Result<Message>> result =
        service.call<Message, client::servicebase::result::unary>(
            serviceId, customServiceId, methodId, customMethodId, parameterStream, waitFor);
    ASSERT_FALSE(result.has_value());
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
