#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <mock/ascendingnumbergenerator.h>
#include <mock/client/transfer.h>
#include <psyrpc/client/channel.h>
#include <psyrpc/impl/client/channel.h>
#include <psyrpc/impl/magic.h>
#include <psyrpc/proto/operators.h>
#include <psyrpc/random.h>

namespace psyrpc {

struct ClientChannel : public testing::Test {
    struct {
        testing::StrictMock<mock::client::Transfer> transfer;
        testing::StrictMock<mock::AscendingNumberGenerator> flowControlGenerator;
    } mock;
    const uint64_t expectedFlowControlValue = std::rand();
    client::Channel channel{std::unique_ptr<impl::client::Channel>{
        new impl::client::Channel{mock.transfer, mock.flowControlGenerator}}};
    std::chrono::milliseconds waitFor{std::rand()};
};

MATCHER_P(RpcEq, param, "Matches psyrpc::proto::Rpc objects")
{
    return proto::matcherImpl<proto::Rpc>(arg, param);
}

TEST_F(ClientChannel, write)
{
    auto callMessage = proto::Call{};

    callMessage.set_id(std::rand());
    callMessage.set_cancel(true);

    auto expectedRpc = proto::Rpc{};
    expectedRpc.set_magicconst(magicConst);
    expectedRpc.set_flowcontrol(expectedFlowControlValue);
    *expectedRpc.mutable_call() = callMessage;

    testing::InSequence seq;
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));
    EXPECT_CALL(mock.transfer, write(RpcEq(expectedRpc))).WillOnce(testing::Return(true));

    ASSERT_TRUE(channel.write(std::move(callMessage)));
}

TEST_F(ClientChannel, writeButTransferFails)
{
    auto callMessage = proto::Call{};

    callMessage.set_id(std::rand());
    callMessage.set_cancel(true);

    auto expectedRpc = proto::Rpc{};
    expectedRpc.set_magicconst(magicConst);
    expectedRpc.set_flowcontrol(expectedFlowControlValue);
    *expectedRpc.mutable_call() = callMessage;

    testing::InSequence seq;
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));
    EXPECT_CALL(mock.transfer, write(RpcEq(expectedRpc))).WillOnce(testing::Return(false));

    ASSERT_FALSE(channel.write(std::move(callMessage)));
}

TEST_F(ClientChannel, read)
{
    auto resultMessage = proto::Result{};

    resultMessage.set_callid(std::rand());
    const auto expectedResultMessage = resultMessage;

    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(expectedFlowControlValue);
    *rpc.mutable_result() = std::move(resultMessage);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));

    resultMessage = {};
    ASSERT_TRUE(channel.read(resultMessage, waitFor));
    ASSERT_TRUE(resultMessage == expectedResultMessage);
}

TEST_F(ClientChannel, readButTransferFails)
{
    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor)).WillOnce(testing::Return(false));

    auto resultMessage = proto::Result{};
    ASSERT_FALSE(channel.read(resultMessage, waitFor));
}

TEST_F(ClientChannel, readButWrongMagicConst)
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(0);
    rpc.set_flowcontrol(expectedFlowControlValue);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));

    auto resultMessage = proto::Result{};
    ASSERT_FALSE(channel.read(resultMessage, waitFor));
}

TEST_F(ClientChannel, readButWrongFlowControl)
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(expectedFlowControlValue + 1);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue));

    auto resultMessage = proto::Result{};
    ASSERT_FALSE(channel.read(resultMessage, waitFor));
}

TEST_F(ClientChannel, readButContainsNoResultMessage)
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(expectedFlowControlValue);

    testing::InSequence seq;
    EXPECT_CALL(mock.transfer, read(testing::_, waitFor))
        .WillOnce(testing::Invoke(
            [&](google::protobuf::MessageLite &message, const std::chrono::milliseconds) {
                static_cast<proto::Rpc &>(message) = rpc;
                return true;
            }));
    EXPECT_CALL(mock.flowControlGenerator, next)
        .WillOnce(testing::Return(expectedFlowControlValue + 1));

    auto resultMessage = proto::Result{};
    ASSERT_FALSE(channel.read(resultMessage, waitFor));
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
