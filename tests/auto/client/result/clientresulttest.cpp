#include <gmock/gmock.h>
#include <google/protobuf/wrappers.pb.h>

#include <mock/ascendingnumbergenerator.h>
#include <mock/call.h>
#include <psyrpc/client/result.h>
#include <psyrpc/impl/common/stream/reader.h>

namespace psyrpc {

using Message = google::protobuf::Int32Value;

struct ClientResult : public testing::Test {
    std::unique_ptr<impl::common::stream::Reader> makeD()
    {
        auto d = std::unique_ptr<impl::common::stream::Reader>{
            new impl::common::stream::Reader{mock.ascendingNumberGenerator}};
        d->call = &mock.call;
        this->d = d.get();
        return d;
    }

    struct {
        testing::StrictMock<mock::Call> call;
        testing::StrictMock<mock::AscendingNumberGenerator> ascendingNumberGenerator;
    } mock;
    impl::common::stream::Reader *d = nullptr;
    client::Result<Message> reader{makeD()};
    std::chrono::milliseconds waitFor{std::rand()};
};

constexpr auto packageCount = 10;

TEST_F(ClientResult, read)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex);
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce([&package](proto::Package &parameterPackage, const std::chrono::milliseconds) {
            parameterPackage = package;
            return true;
        });
    EXPECT_CALL(mock.ascendingNumberGenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_TRUE(reader.read(message, waitFor));
    ASSERT_EQ(message.value(), expectedMessage.value());
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(ClientResult, readButCallReadFails)
{
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce([&package](proto::Package &parameterPackage, const std::chrono::milliseconds) {
            parameterPackage = package;
            return false;
        });

    auto message = Message{};
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(ClientResult, readButInvalidPackageIndex)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex + 1);
    package.mutable_data()->set_value(expectedMessage.SerializeAsString());
    package.set_eot(true);

    testing::InSequence seq;
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce([&package](proto::Package &parameterPackage, const std::chrono::milliseconds) {
            parameterPackage = package;
            return true;
        });
    EXPECT_CALL(mock.ascendingNumberGenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_EQ(d->call, nullptr);
}

TEST_F(ClientResult, readButInvalidData)
{
    static constexpr auto packageIndex = 1;
    auto expectedMessage = Message{};
    expectedMessage.set_value(std::rand());

    auto package = proto::Package{};
    package.set_index(packageIndex);
    package.set_eot(true);
    package.mutable_data()->set_value("Some Bad Data");
    EXPECT_CALL(mock.call, read(testing::_, waitFor))
        .WillOnce([&package](proto::Package &parameterPackage, const std::chrono::milliseconds) {
            parameterPackage = package;
            return true;
        });
    EXPECT_CALL(mock.ascendingNumberGenerator, next).WillOnce(testing::Return(packageIndex));

    auto message = Message{};
    ASSERT_FALSE(reader.read(message, waitFor));
    ASSERT_EQ(d->call, nullptr);
}

} // namespace psyrpc

int main(int argc, char *argv[])
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
