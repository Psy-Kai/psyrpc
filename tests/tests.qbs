import qbs

Project {
    references: [
        "auto/client/call",
        "auto/client/channel",
        "auto/client/result",
        "auto/client/service",
        "auto/common/stream/reader",
        "auto/common/stream/writer",
        "auto/generator",
        "auto/impl/ascendingnumbergenerator",
        "auto/impl/remote/calldispatcher",
        "auto/integration",
        "auto/mocks",
        "auto/remote/call",
        "auto/remote/channel",
        "auto/remote/server",
        "auto/remote/service",
        "helpers",
    ]

    AutotestRunner { name: "autotest-runner" }
}
