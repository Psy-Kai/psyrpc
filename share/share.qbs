Product {
    files: [
        "qbs/**/*.js",
        "qbs/**/*.qbs",
    ]

    qbs.install: true
    qbs.installSourceBase: sourceDirectory
}
