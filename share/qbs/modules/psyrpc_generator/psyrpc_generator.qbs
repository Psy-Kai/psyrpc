import qbs.Probes
import "psyrpc_generator.js" as HelperFunctions

Module {
    property string pluginPath: pluginProbe.filePath
    property string pluginName: "protoc-gen-psyRPC"

    property pathList importPaths: []

    readonly property string outputDir: product.buildDirectory + "/protobuf"

    Probes.BinaryProbe {
        id: pluginProbe
        names: pluginName
    }

    validate: {
        if (!HelperFunctions.checkPath(pluginPath)) {
            throw "Can't find psyRPC protobuf plugin. Please set the pluginPath property.";
        }
    }
}
