import "../psyrpc_generator.qbs" as PsyRPCGenerator
import "../psyrpc_generator.js" as HelperFunctions

PsyRPCGenerator {
    readonly property string compilerPath: protobuf.cpp.compilerPath

    Rule {
        inputs: ["psyrpc.client.input", "psyrpc.remote.input"]
        outputFileTags: ["hpp", "psyrpc.client.hpp", "psyrpc.remote.hpp", "cpp"]
        outputArtifacts: {
            var outputDir = HelperFunctions.getOutputDir(input.psyrpc_generator.cpp, input);
            var result = [];
            if (input.fileTags.contains("psyrpc.client.input")) {
                result.push(
                        HelperFunctions.cppArtifact(outputDir, input, ["hpp", "psyrpc.client.hpp"],
                                                    ".psyrpc_client.h"),
                        HelperFunctions.cppArtifact(outputDir, input, "cpp", ".psyrpc_client.cpp"));
            }
            if (input.fileTags.contains("psyrpc.remote.input")) {
                result.push(
                        HelperFunctions.cppArtifact(outputDir, input, ["hpp", "psyrpc.remote.hpp"],
                                                    ".psyrpc_remote.h"),
                        HelperFunctions.cppArtifact(outputDir, input, "cpp", ".psyrpc_remote.cpp"));
            }
            return result;
        }

        prepare: {
            var result = HelperFunctions.doPrepare(
                        input.psyrpc_generator.cpp, product, input, outputs, "psyRPC",
                        input.psyrpc_generator.cpp.pluginPath);
            return result;
        }
    }

    Depends { name: "protobuf.cpp" }
    // Depends { name: "psyRPC.full.cpp"; required: false }
}
