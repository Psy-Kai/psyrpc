var File = require("qbs.File");
var FileInfo = require("qbs.FileInfo");

function checkPath(path) {
    return path && File.exists(path);
};

function getOutputDir(module, input) {
    var outputDir = module.outputDir;
    var importPaths = module.importPaths;
    if (importPaths.length !== 0) {
        var canonicalInput = File.canonicalFilePath(FileInfo.path(input.filePath));
        for (var i = 0; i < importPaths.length; ++i) {
            var path = File.canonicalFilePath(importPaths[i]);

            if (canonicalInput.startsWith(path)) {
                return outputDir + "/" + FileInfo.relativePath(path, canonicalInput);
            }
        }
    }
    return outputDir;
}

function cppArtifact(outputDir, input, tags, suffix) {
    return {
        fileTags: tags,
        filePath: FileInfo.joinPaths(outputDir, FileInfo.baseName(input.fileName) + suffix),
        cpp: {
            includePaths: [].concat(input.cpp.includePaths, outputDir),
            warningLevel: "none",
        }
    };
}

function doPrepare(module, product, input, outputs, generator, plugin, generatorOptions) {
    var outputDir = module.outputDir;
    var args = [];

    if (!!plugin) {
        args.push("--plugin=protoc-gen-" + generator + "=" + plugin)
    }

    args.push("--" + generator + "_out", outputDir);
    if (!!generatorOptions) {
        for (var i = 0; i < generatorOptions.length; ++i) {
            args.push("--" + generator + "_opt=" + generatorOptions[i])
        }
    }

    var importPaths = module.importPaths;
    if (importPaths.length === 0) {
        importPaths = [FileInfo.path(input.filePath)];
    }
    importPaths.forEach(function (path) {
        if (!FileInfo.isAbsolutePath(path)) {
            path = FileInfo.joinPaths(product.sourceDirectory, path);
        }
        args.push("--proto_path", path);
    });

    args.push(input.filePath);

    var cmd = new Command(module.compilerPath, args);
    cmd.highlight = "codegen";
    cmd.description = "generating " + generator + " files for " + input.fileName;
    return [cmd];
}
