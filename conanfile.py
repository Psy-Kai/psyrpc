import os
from conan import ConanFile
from conan.tools.files import update_conandata
from conan.tools.scm import Git
from conan.tools.files import save
from conan.tools.env import VirtualBuildEnv


def gitlab_ci_strip_credentials(url):
    def is_http(url):
        return url.startswith("http")

    if not is_http(url):
        return url

    if "@" not in url:
        return url

    scheme_and_credentials, path = url.split("@")
    scheme, _user, _password = scheme_and_credentials.split(":")

    return "{}://{}".format(scheme, path)

def generateCustomBuildEnvInfo(conanfile):
    deps_env_info_dict = {}
    env_vars = VirtualBuildEnv(conanfile).vars()
    for name, value in env_vars.items():
        deps_env_info_dict[name] = value.split(env_vars._pathsep)

    dependencies_dict = {}
    for _require, dependency in conanfile.dependencies.host.items():
        dependencies_dict[dependency.ref.name] = {
            'rootpath': dependency.package_folder,
            'include_paths': dependency.cpp_info.includedirs,
            'lib_paths': dependency.cpp_info.libdirs
        }

    custom_conan_build_info_dict = {
        'deps_env_info': deps_env_info_dict,
        'dependencies': dependencies_dict
    }

    save(conanfile, path=os.path.join(conanfile.generators_folder, "customconanbuildinfo.json"),
            content=str(custom_conan_build_info_dict).replace('"', '\\"').replace("'", '"'))


class PsyRpcConanfile(ConanFile):
    name = "psyrpc"
    author = "Kai Dohmen"
    settings = "os", "arch", "compiler", "build_type"
    requires = [
        "optional-lite/3.5.0",
        "protobuf/3.21.4",
        "string-view-lite/1.7.0",
        "xxhash/0.8.1",
    ]
    tool_requires = [
        "protobuf/3.21.4",
        "qbs/1.23.1@psy-kai/protobuf_patch",
    ]
    python_requires = [
        "conan-tools-qbs/2022.09.29@psy-kai/devel",
    ]
    generators = ["PkgConfigDeps"]

    @property
    def _qbs(self):
        return self.python_requires["conan-tools-qbs"].module

    def export(self):
        git = Git(self, self.recipe_folder)
        scm_url, scm_commit = git.get_url_and_commit()
        scm_url = gitlab_ci_strip_credentials(scm_url)
        update_conandata(self, {"sources": {"commit": scm_commit, "url": scm_url}})

    def build_requirements(self):
        self.test_requires("gtest/1.11.0")

    def layout(self):
        self._qbs.qbs_layout(self)

    def source(self):
        git = Git(self)
        sources = self.conan_data["sources"]
        url = sources["url"]
        commit = sources["commit"]
        git.clone(url=url, target=".")
        git.checkout(commit=commit)

    def generate(self):
        qbs_profile = self._qbs.QbsProfile(self)
        qbs_profile.generate()

        generateCustomBuildEnvInfo(self)

    def build(self):
        qbs = self._qbs.Qbs(self)
        qbs.add_configuration(values = {
            "modules.qbs.installPrefix": "",
            "projects.psyRPC.conanGeneratorsFolder": self.generators_folder})
        qbs.build()

    def package(self):
        qbs = self._qbs.Qbs(self)
        qbs.install()

    def package_info(self):
        path = os.path.join(self.package_folder, 'bin')
        self.buildenv_info.prepend_path("PATH", path)
        self.cpp_info.libs = ["psyrpc-cpp"]
