import qbs.FileInfo
import qbs.Probes

Project {
    Probes.BinaryProbe {
        id: protocProbe
        names: "protoc"
        searchPaths: project._customConanBuildInfoJson.deps_env_info.PATH
    }

    Profile {
        name: "conan"
        baseProfile: "conan_toolchain_profile"

        moduleProviders.qbspkgconfig.libDirs: project._conanGeneratorsFolder
        moduleProviders.qbspkgconfig.mergeDependencies: false

        protobuf.cpp.compilerPath: {
            if (!protocProbe.found) {
                throw "failed to find protoc in " +
                        JSON.stringify(project._customConanBuildInfoJson.deps_env_info.PATH);
            }

            return protocProbe.filePath;
        }
        protobuf.cpp.libraryPath: project._customConanBuildInfoJson.dependencies["protobuf"].lib_paths[0]
        protobuf.cpp.includePath: project._customConanBuildInfoJson.dependencies["protobuf"].include_paths[0]
        protobuf.cpp.importPaths: project._customConanBuildInfoJson.dependencies["protobuf"].include_paths[0]
    }
}
