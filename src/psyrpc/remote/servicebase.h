#pragma once

#include <memory>

#include <psyrpc/proto/call.pb.h>

#include "serviceinterface.h"
#include "stream/parameter.h"
#include "stream/result.h"

namespace psyrpc {
namespace remote {
namespace detail {
enum class CallState { Success, Failure };
namespace stream {

template <class TStream>
TStream make(common::call::Interface &call)
{
    return TStream{call};
}

template <class TMessage, template <class> class TStream = remote::stream::Result>
TStream<TMessage> makeResult(common::call::Interface &call)
{
    return make<TStream<TMessage>>(call);
}

template <class TMessage, template <class> class TStream = remote::stream::Parameter>
TStream<TMessage> makeParameter(common::call::Interface &call)
{
    return make<TStream<TMessage>>(call);
}

} // namespace stream
} // namespace detail

template <ServiceId tId, ServiceId tCustom>
class ServiceBase : public ServiceInterface {
public:
    ServiceId id() const override
    {
        return tId;
    }
    ServiceId customId() const override
    {
        return tCustom;
    }

protected:
    using Eot = psyrpc::Eot;
    using Rpc = detail::CallState;
};

} // namespace remote
} // namespace psyrpc
