#pragma once

#include "../../common/stream/writer.h"

namespace psyrpc {
namespace remote {
namespace stream {

template <class TMessage>
using Result = common::stream::Writer<TMessage>;

} // namespace stream
} // namespace remote
} // namespace psyrpc
