#pragma once

#include "../../common/stream/reader.h"

namespace psyrpc {
namespace remote {
namespace stream {

template <class TMessage>
using Parameter = common::stream::Reader<TMessage>;

} // namespace stream
} // namespace remote
} // namespace psyrpc
