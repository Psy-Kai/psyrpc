#pragma once

#include <memory>

#include <psyrpc/proto/channel.pb.h>

#include "channelinterface.h"

namespace psyrpc {
namespace impl {
namespace remote {
struct Channel;
} // namespace remote
} // namespace impl
namespace remote {

class Channel final : public ChannelInterface {
public:
    explicit Channel(TransferInterface &transfer);
    explicit Channel(std::unique_ptr<impl::remote::Channel> d);
    Channel(Channel &&) noexcept;
    ~Channel() override;
    Channel &operator=(Channel &&) noexcept;
    bool read(proto::Call &call, const std::chrono::milliseconds waitFor) override;
    bool write(proto::Result &&result) override;
    void write(proto::Rpc::Error error) override;
    const TransferInterface &transfer() const override;

private:
    proto::Rpc createRpcToWrite();
    bool validateRpc(const proto::Rpc &message);

    std::unique_ptr<impl::remote::Channel> m_d;
};

} // namespace remote
} // namespace psyrpc
