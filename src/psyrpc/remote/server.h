#pragma once

#include "calldispatcherinterface.h"
#include "channelmanagerinterface.h"
#include "serviceinterface.h"

namespace psyrpc {
namespace impl {
namespace remote {
struct Server;
} // namespace remote
} // namespace impl
namespace remote {

class Server final : public ChannelManagerInterface {
public:
    Server();
    explicit Server(std::unique_ptr<impl::remote::Server> d);
    Server(Server &&) noexcept;
    ~Server() override;
    Server &operator=(Server &&) noexcept;

    void add(ServiceInterface &service);
    void remove(ServiceInterface &service);

private:
    void onOpened(TransferInterface &transfer) override;
    void onClosed(TransferInterface &transfer) override;

    std::unique_ptr<impl::remote::Server> m_d;
};

} // namespace remote
} // namespace psyrpc
