#pragma once

#include "channelmanagerinterface.h"

namespace psyrpc {
namespace remote {

class AcceptorInterface {
public:
    virtual ~AcceptorInterface() = default;
    virtual void assign(ChannelManagerInterface &) = 0;
};

} // namespace remote
} // namespace psyrpc
