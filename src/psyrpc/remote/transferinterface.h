#pragma once

#include <cstddef>

#include <google/protobuf/message_lite.h>

#include "calldispatcherinterface.h"

namespace psyrpc {
namespace remote {

class TransferInterface {
public:
    virtual ~TransferInterface() = default;
    virtual bool read(google::protobuf::MessageLite &, const std::chrono::milliseconds) = 0;
    virtual bool write(const google::protobuf::MessageLite &) = 0;
    virtual void attach(CallDispatcherInterface *callDispatcher) = 0;
};

} // namespace remote
} // namespace psyrpc
