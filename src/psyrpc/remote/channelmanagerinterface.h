#pragma once

#include "transferinterface.h"

namespace psyrpc {
namespace remote {

class ChannelManagerInterface {
public:
    virtual ~ChannelManagerInterface() = default;
    virtual void onOpened(TransferInterface &transfer) = 0;
    virtual void onClosed(TransferInterface &transfer) = 0;
};

} // namespace remote
} // namespace psyrpc
