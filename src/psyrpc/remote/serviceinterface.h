#pragma once

#include "../common/call/interface.h"
#include "../common/types.h"

namespace psyrpc {
namespace remote {

class ServiceInterface {
public:
    virtual ~ServiceInterface() = default;
    virtual bool has(const proto::XxHashedSignature &method) = 0;
    virtual bool call(const proto::XxHashedSignature &method, common::call::Interface &call,
                      const std::chrono::milliseconds waitFor) = 0;
    virtual ServiceId id() const = 0;
    virtual ServiceId customId() const = 0;
};

} // namespace remote
} // namespace psyrpc
