#include "server.h"

#include "../impl/remote/server.h"

namespace psyrpc {
namespace remote {

Server::Server() : m_d{new impl::remote::ServerWithDispatcher} {}

Server::Server(std::unique_ptr<impl::remote::Server> d) : m_d{std::move(d)} {}

Server::Server(Server &&) noexcept = default;

Server::~Server() = default;

Server &Server::operator=(Server &&) noexcept = default;

void Server::add(ServiceInterface &service)
{
    const auto it = std::find(std::begin(m_d->services), std::end(m_d->services), &service);
    assert(it == std::end(m_d->services));
    m_d->services.emplace_back(&service);
}

void Server::remove(ServiceInterface &service)
{
    const auto it = std::remove(std::begin(m_d->services), std::end(m_d->services), &service);
    m_d->services.erase(it, std::end(m_d->services));
}

void Server::onOpened(TransferInterface &transfer)
{
    m_d->channels.emplace_back(transfer);
    transfer.attach(&m_d->callDispatcher);
}

void Server::onClosed(TransferInterface &transfer)
{
    transfer.attach(nullptr);
    const auto it = std::remove_if(std::begin(m_d->channels), std::end(m_d->channels),
                                   [&transfer](impl::remote::ChannelData &channel) {
                                       return &channel.channel.transfer() == &transfer;
                                   });
    m_d->channels.erase(it, std::end(m_d->channels));
}

} // namespace remote
} // namespace psyrpc
