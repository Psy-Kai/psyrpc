#pragma once

#include <memory>

#include "../common/call/interface.h"

namespace psyrpc {
namespace impl {
namespace remote {
struct Call;
} // namespace remote
} // namespace impl
namespace remote {

class ChannelInterface;

class Call final : public common::call::Interface {
public:
    Call(const common::call::Id id, ChannelInterface &channel);
    Call(Call &&) noexcept;
    ~Call() override;
    Call &operator=(Call &&) noexcept;

    bool write(proto::Package &&package) override;
    bool read(proto::Package &package, const std::chrono::milliseconds waitFor) override;

private:
    std::unique_ptr<impl::remote::Call> m_d;
};

} // namespace remote
} // namespace psyrpc
