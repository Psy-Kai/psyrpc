#pragma once

#include <chrono>

namespace psyrpc {
namespace remote {

class TransferInterface;
class CallDispatcherInterface {
public:
    virtual ~CallDispatcherInterface() = default;
    virtual void dispatch(TransferInterface &,
                          const std::chrono::milliseconds = std::chrono::milliseconds::max()) = 0;
};

} // namespace remote
} // namespace psyrpc
