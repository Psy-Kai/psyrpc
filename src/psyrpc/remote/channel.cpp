#include "channel.h"

#include "../impl/magic.h"
#include "../impl/remote/channel.h"

namespace psyrpc {
namespace remote {

Channel::Channel(TransferInterface &transfer) :
    m_d{new impl::remote::ChannelWithGenerator{transfer}}
{}

Channel::Channel(std::unique_ptr<impl::remote::Channel> d) : m_d{std::move(d)} {}

Channel::Channel(Channel &&) noexcept = default;

Channel::~Channel() = default;

Channel &Channel::operator=(Channel &&) noexcept = default;

bool Channel::read(proto::Call &call, const std::chrono::milliseconds waitFor)
{
    auto message = proto::Rpc{};
    if (!m_d->transfer.read(message, waitFor)) {
        return false;
    }
#if PSYRPC_PRINT_MESSAGES
    std::puts("server read:");
    message.PrintDebugString();
    std::fflush(stdout);
#endif // PSYRPC_PRINT_MESSAGES
    if (!validateRpc(message) || !message.has_call()) {
        return false;
    }
    call = std::move(*message.mutable_call());
    return true;
}

bool Channel::write(proto::Result &&result)
{
    auto message = createRpcToWrite();
    *message.mutable_result() = std::move(result);
    return m_d->transfer.write(message);
}

void Channel::write(proto::Rpc::Error error)
{
    auto message = createRpcToWrite();
    message.set_error(error);
    m_d->transfer.write(message);
}

const TransferInterface &Channel::transfer() const
{
    return m_d->transfer;
}

proto::Rpc Channel::createRpcToWrite()
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(m_d->flowControl.next());
    return rpc;
}

bool Channel::validateRpc(const proto::Rpc &message)
{
    return message.magicconst() == magicConst && message.flowcontrol() == m_d->flowControl.next();
}

} // namespace remote
} // namespace psyrpc
