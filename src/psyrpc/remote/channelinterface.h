#pragma once

#include <psyrpc/proto/channel.pb.h>

#include "transferinterface.h"

namespace psyrpc {
namespace remote {

class ChannelInterface {
public:
    virtual ~ChannelInterface() = default;
    virtual bool read(proto::Call &call, const std::chrono::milliseconds waitFor) = 0;
    virtual bool write(proto::Result &&result) = 0;
    virtual void write(proto::Rpc::Error error) = 0;
    virtual const TransferInterface &transfer() const = 0;
};

} // namespace remote
} // namespace psyrpc
