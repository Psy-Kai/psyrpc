#include "call.h"

#include "../impl/remote/call.h"
#include "channelinterface.h"

namespace psyrpc {
namespace remote {

Call::Call(const common::call::Id id, ChannelInterface &channel) :
    m_d{new impl::remote::Call{id, channel}}
{}

Call::Call(Call &&) noexcept = default;

Call::~Call() = default;

Call &Call::operator=(Call &&) noexcept = default;

bool Call::write(proto::Package &&package)
{
    auto result = psyrpc::proto::Result{};
    result.set_callid(m_d->callId);
    *result.mutable_result() = std::move(package);

    return m_d->channel.write(std::move(result));
}

bool Call::read(proto::Package &package, const std::chrono::milliseconds waitFor)
{
    auto call = psyrpc::proto::Call{};
    if (!m_d->channel.read(call, waitFor))
        return false;
    if (call.id() != m_d->callId)
        return false;
    package = std::move(*call.mutable_parameter());
    return true;
}

} // namespace remote
} // namespace psyrpc
