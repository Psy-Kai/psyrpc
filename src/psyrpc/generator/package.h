#pragma once

#include "sectioninterfaces.h"

namespace psyrpc {
namespace generator {
namespace package {

class Start final : public section::FileInterface {
public:
    void generate(const google::protobuf::FileDescriptor &file,
                  google::protobuf::io::Printer &printer) const override;
};

class End final : public section::FileInterface {
public:
    void generate(const google::protobuf::FileDescriptor &file,
                  google::protobuf::io::Printer &printer) const override;
};

class Package final : public section::FileInterface {
public:
    explicit Package(section::FileInterface &start, section::ServiceInterface &service,
                     section::FileInterface &end);
    void generate(const google::protobuf::FileDescriptor &file,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::FileInterface &m_start;
    section::ServiceInterface &m_service;
    section::FileInterface &m_end;
};

} // namespace package
} // namespace generator
} // namespace psyrpc
