#pragma once

#include <string>

#include <nonstd/string_view.hpp>

namespace psyrpc {
namespace generator {
namespace utilities {

std::string normalized(std::string fullName);
nonstd::string_view baseName(nonstd::string_view protoFileName);

} // namespace utilities
} // namespace generator
} // namespace psyrpc
