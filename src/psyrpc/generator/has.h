#pragma once

#include "sectioninterfaces.h"

namespace psyrpc {
namespace generator {
namespace has {

class Remote final : public section::ServiceInterface {
public:
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;
};

} // namespace has
} // namespace generator
} // namespace psyrpc
