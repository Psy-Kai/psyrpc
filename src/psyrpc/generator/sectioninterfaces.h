#pragma once

#include <google/protobuf/descriptor.h>

namespace psyrpc {
namespace generator {
namespace section {

class FileInterface {
public:
    virtual ~FileInterface() = default;
    virtual void generate(const google::protobuf::FileDescriptor &file,
                          google::protobuf::io::Printer &printer) const = 0;
};

class ServiceInterface {
public:
    virtual ~ServiceInterface() = default;
    virtual void generate(const google::protobuf::ServiceDescriptor &service,
                          google::protobuf::io::Printer &printer) const = 0;
};

class MethodInterface {
public:
    virtual ~MethodInterface() = default;
    virtual void generate(const google::protobuf::MethodDescriptor &method,
                          google::protobuf::io::Printer &printer) const = 0;
};

} // namespace section
} // namespace generator
} // namespace psyrpc
