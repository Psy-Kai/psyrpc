#include "package.h"

#include <google/protobuf/io/printer.h>
#include <nonstd/string_view.hpp>

namespace psyrpc {
namespace generator {
constexpr auto newLine = nonstd::string_view{"\n"};
namespace package {

namespace placeholer {
constexpr auto name = nonstd::string_view{"name"};
}

constexpr auto open = nonstd::string_view{"namespace %name% {\n"};
constexpr auto close = nonstd::string_view{"}\n"};

constexpr auto delimiter = '.';

void Start::generate(const google::protobuf::FileDescriptor &file,
                     google::protobuf::io::Printer &printer) const
{
    const auto &packageName = file.package();
    const auto print = [&printer, &packageName](const std::size_t begin, const std::size_t end) {
        printer.Print({{placeholer::name.data(),
                        {std::begin(packageName) + begin, std::begin(packageName) + end}}},
                      open.data());
    };
    std::size_t begin = 0;
    for (std::size_t next = packageName.find(delimiter, begin); next < packageName.size();
         next = packageName.find(delimiter, begin)) {
        print(begin, next);
        begin = next + 1;
    }
    print(begin, packageName.size());
}

void End::generate(const google::protobuf::FileDescriptor &file,
                   google::protobuf::io::Printer &printer) const
{
    const auto &packageName = file.package();
    std::size_t begin = 0;
    for (std::size_t next = packageName.find(delimiter, begin); next < packageName.size();
         next = packageName.find(delimiter, begin)) {
        printer.PrintRaw(close.data());
        begin = next + 1;
    }
    printer.PrintRaw(close.data());
}

Package::Package(FileInterface &start, section::ServiceInterface &service, FileInterface &end) :
    m_start{start}, m_service{service}, m_end{end}
{}

void Package::generate(const google::protobuf::FileDescriptor &file,
                       google::protobuf::io::Printer &printer) const
{
    m_start.generate(file, printer);
    printer.PrintRaw(newLine.data());
    for (int i = 0; i < file.service_count(); ++i) {
        const auto *service = file.service(i);
        m_service.generate(*service, printer);
        printer.PrintRaw(newLine.data());
    }
    m_end.generate(file, printer);
}

} // namespace package
} // namespace generator
} // namespace psyrpc
