#pragma once

#include "sectioninterfaces.h"

namespace psyrpc {
namespace generator {
namespace method {
namespace client {

class Header final : public section::MethodInterface {
public:
    void generate(const google::protobuf::MethodDescriptor &method,
                  google::protobuf::io::Printer &printer) const override;
};

class Source final : public section::MethodInterface {
public:
    void generate(const google::protobuf::MethodDescriptor &method,
                  google::protobuf::io::Printer &printer) const override;
};

} // namespace client

class Remote final : public section::MethodInterface {
public:
    void generate(const google::protobuf::MethodDescriptor &method,
                  google::protobuf::io::Printer &printer) const override;
};

} // namespace method
} // namespace generator
} // namespace psyrpc
