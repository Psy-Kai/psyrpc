#include "has.h"

#include <xxhash.h>
#include <google/protobuf/io/printer.h>
#include <nonstd/string_view.hpp>

namespace psyrpc {
namespace generator {
namespace has {
namespace placeholder {
constexpr auto name = nonstd::string_view{"name"};
constexpr auto methodId = nonstd::string_view{"methodId"};
constexpr auto customMethodId = nonstd::string_view{"customMethodId"};
} // namespace placeholder
namespace templates {
constexpr auto start =
    nonstd::string_view{"bool %name%Base::has(const psyrpc::proto::XxHashedSignature &method)\n"
                        "{\n"
                        "    switch (method.value()) {\n"};
constexpr auto caze =
    nonstd::string_view{"case %methodId%:\n"
                        "    return method.customidtoavoidcollision() == %customMethodId%;\n"};
constexpr auto end = nonstd::string_view{"    }\n"
                                         "    return false;\n"
                                         "}\n"};
} // namespace templates

void Remote::generate(const google::protobuf::ServiceDescriptor &service,
                      google::protobuf::io::Printer &printer) const
{
    printer.Print({{placeholder::name.data(), service.name()}}, templates::start.data());
    printer.Indent();
    printer.Indent();
    printer.Indent();
    printer.Indent();
    for (int i = 0; i < service.method_count(); ++i) {
        const auto &method = *service.method(i);
        printer.Print({{placeholder::methodId.data(),
                        std::to_string(XXH32(method.name().data(), method.name().size(), 0))},
                       {placeholder::customMethodId.data(), std::to_string(0)}},
                      templates::caze.data());
    }
    printer.Outdent();
    printer.Outdent();
    printer.Outdent();
    printer.Outdent();
    printer.PrintRaw(templates::end.data());
}

} // namespace has
} // namespace generator
} // namespace psyrpc
