#pragma once

#include "sectioninterfaces.h"

namespace psyrpc {
namespace generator {
namespace call {

class Remote final : public section::ServiceInterface {
public:
    void generate(const google::protobuf::ServiceDescriptor &method,
                  google::protobuf::io::Printer &printer) const override;
};

} // namespace call
} // namespace generator
} // namespace psyrpc
