#include "service.h"

#include <xxhash.h>
#include <google/protobuf/io/printer.h>
#include <nonstd/string_view.hpp>

namespace psyrpc {
namespace generator {
namespace service {
constexpr auto newLine = nonstd::string_view{"\n"};
namespace placeholder {
constexpr auto name = nonstd::string_view{"name"};
constexpr auto serviceId = nonstd::string_view{"serviceId"};
constexpr auto customServiceId = nonstd::string_view{"customServiceId"};
} // namespace placeholder

constexpr auto separator = '.';

std::string fullTypeSignature(const google::protobuf::ServiceDescriptor &service)
{
    if (!service.file()->package().empty())
        return service.file()->package() + separator + service.name();
    return service.name();
}

namespace client {
namespace header {

constexpr auto startTemplate =
    nonstd::string_view{"class %name% final : public psyrpc::client::ServiceBase {\n"
                        "public:\n"
                        "    explicit %name%(psyrpc::client::ChannelInterface &channel);\n"};
constexpr auto endTemplate =
    nonstd::string_view{"private:\n"
                        "    static constexpr auto serviceId = %serviceId%;\n"
                        "    static constexpr auto customServiceId = %customServiceId%;\n"
                        "};\n"};

void Start::generate(const google::protobuf::ServiceDescriptor &service,
                     google::protobuf::io::Printer &printer) const
{
    printer.Print({{std::string{placeholder::name}, service.name()}}, startTemplate.data());
}

void End::generate(const google::protobuf::ServiceDescriptor &service,
                   google::protobuf::io::Printer &printer) const
{
    const auto fullService = fullTypeSignature(service);
    printer.Print({{std::string{placeholder::name}, service.name()},
                   {std::string{placeholder::serviceId},
                    std::to_string(XXH32(fullService.data(), fullService.size(), 0))},
                   {std::string{placeholder::customServiceId}, std::to_string(0)}},
                  endTemplate.data());
}

} // namespace header

Header::Header(section::ServiceInterface &start, section::MethodInterface &method,
               section::ServiceInterface &end) :
    m_start{start}, m_method{method}, m_end{end}
{}

void Header::generate(const google::protobuf::ServiceDescriptor &service,
                      google::protobuf::io::Printer &printer) const
{
    m_start.generate(service, printer);
    for (int i = 0; i < service.method_count(); ++i) {
        const auto *method = service.method(i);
        m_method.generate(*method, printer);
    }
    m_end.generate(service, printer);
}

constexpr auto sourceTemplate =
    nonstd::string_view{"%name%::%name%(psyrpc::client::ChannelInterface &channel) :\n"
                        "    psyrpc::client::ServiceBase{channel}\n"
                        "{}\n"};

Source::Source(section::MethodInterface &method) : m_method{method} {}

void Source::generate(const google::protobuf::ServiceDescriptor &service,
                      google::protobuf::io::Printer &printer) const
{
    printer.Print({{std::string{placeholder::name}, service.name()}}, sourceTemplate.data());
    printer.PrintRaw(newLine.data());
    for (int i = 0; i < service.method_count(); ++i) {
        const auto *method = service.method(i);
        m_method.generate(*method, printer);
        printer.PrintRaw(newLine.data());
    }
}

} // namespace client
namespace remote {
namespace header {

constexpr auto startTemplate = nonstd::string_view{
    "class %name%Base : public psyrpc::remote::ServiceBase<%serviceId%, %customServiceId%> {\n"
    "private:\n"
    "    bool has(const psyrpc::proto::XxHashedSignature &method) override;\n"
    "    bool call(const psyrpc::proto::XxHashedSignature &method, "
    "psyrpc::common::call::Interface &call, const std::chrono::milliseconds waitFor) override;\n"};
constexpr auto endTemplate = nonstd::string_view{"};\n"};

void Start::generate(const google::protobuf::ServiceDescriptor &service,
                     google::protobuf::io::Printer &printer) const
{
    const auto fullService = fullTypeSignature(service);
    printer.Print({{placeholder::name.data(), service.name()},
                   {placeholder::serviceId.data(),
                    std::to_string(XXH32(fullService.data(), fullService.size(), 0))},
                   {placeholder::customServiceId.data(), std::to_string(0)}},
                  startTemplate.data());
}

void End::generate(const google::protobuf::ServiceDescriptor & /*service*/,
                   google::protobuf::io::Printer &printer) const
{
    printer.PrintRaw(endTemplate.data());
}

} // namespace header

Header::Header(section::ServiceInterface &start, section::MethodInterface &method,
               section::ServiceInterface &end) :
    m_start{start}, m_method{method}, m_end{end}
{}

void Header::generate(const google::protobuf::ServiceDescriptor &service,
                      google::protobuf::io::Printer &printer) const
{
    m_start.generate(service, printer);
    for (int i = 0; i < service.method_count(); ++i) {
        const auto *method = service.method(i);
        m_method.generate(*method, printer);
    }
    m_end.generate(service, printer);
}

Source::Source(section::ServiceInterface &has, // section::MethodInterface &method,
               section::ServiceInterface &call) :
    m_has{has}, m_call{call}
{}

void Source::generate(const google::protobuf::ServiceDescriptor &service,
                      google::protobuf::io::Printer &printer) const
{
    m_has.generate(service, printer);
    printer.PrintRaw(newLine.data());
    m_call.generate(service, printer);
}

} // namespace remote
} // namespace service
} // namespace generator
} // namespace psyrpc
