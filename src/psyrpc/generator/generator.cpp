#include <google/protobuf/compiler/code_generator.h>
#include <google/protobuf/compiler/plugin.h>

#include "builder.h"

namespace psyrpc {

class Generator final : public google::protobuf::compiler::CodeGenerator {
public:
    Generator()
    {
        m_clientBuilder.build(m_client);
        m_remoteBuilder.build(m_remote);
    }

    bool Generate(const google::protobuf::FileDescriptor *file, const std::string &parameter,
                  google::protobuf::compiler::GeneratorContext *generator_context,
                  std::string *error) const override
    {
        return m_client->Generate(file, parameter, generator_context, error) &&
            m_remote->Generate(file, parameter, generator_context, error);
    }

private:
    nonstd::optional<generator::Client> m_client;
    generator::client::Builder m_clientBuilder;
    nonstd::optional<generator::Remote> m_remote;
    generator::remote::Builder m_remoteBuilder;
};

} // namespace psyrpc

int main(int argc, char *argv[])
{
    psyrpc::Generator psyrpcGenerator;
    return google::protobuf::compiler::PluginMain(argc, argv, &psyrpcGenerator);
}
