#include "builder.h"

namespace psyrpc {
namespace generator {

void client::Builder::build(nonstd::optional<Client> &client)
{
    client.emplace(header.file, source.file);
}

void remote::Builder::build(nonstd::optional<Remote> &remote)
{
    remote.emplace(header.file, source.file);
}

} // namespace generator
} // namespace psyrpc
