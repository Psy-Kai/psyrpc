#pragma once

#include "sectioninterfaces.h"

namespace psyrpc {
namespace generator {
namespace service {
namespace client {
namespace header {

class Start final : public section::ServiceInterface {
public:
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;
};

class End final : public section::ServiceInterface {
public:
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;
};

} // namespace header

class Header final : public section::ServiceInterface {
public:
    explicit Header(section::ServiceInterface &start, section::MethodInterface &method,
                    section::ServiceInterface &end);
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::ServiceInterface &m_start;
    section::MethodInterface &m_method;
    section::ServiceInterface &m_end;
};

class Source final : public section::ServiceInterface {
public:
    explicit Source(section::MethodInterface &method);
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::MethodInterface &m_method;
};

} // namespace client
namespace remote {
namespace header {

class Start final : public section::ServiceInterface {
public:
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;
};

class End final : public section::ServiceInterface {
public:
    void generate(const google::protobuf::ServiceDescriptor &,
                  google::protobuf::io::Printer &printer) const override;
};

} // namespace header

class Header final : public section::ServiceInterface {
public:
    Header(section::ServiceInterface &start, section::MethodInterface &method,
           section::ServiceInterface &end);
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::ServiceInterface &m_start;
    section::MethodInterface &m_method;
    section::ServiceInterface &m_end;
};

class Source final : public section::ServiceInterface {
public:
    Source(section::ServiceInterface &has, section::ServiceInterface &call);
    void generate(const google::protobuf::ServiceDescriptor &service,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::ServiceInterface &m_has;
    section::ServiceInterface &m_call;
};

} // namespace remote
} // namespace service
} // namespace generator
} // namespace psyrpc
