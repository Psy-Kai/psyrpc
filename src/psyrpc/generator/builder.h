#pragma once

#include <nonstd/optional.hpp>

#include "call.h"
#include "client.h"
#include "file.h"
#include "has.h"
#include "method.h"
#include "package.h"
#include "remote.h"
#include "service.h"

namespace psyrpc {
namespace generator {
namespace client {

class BuilderInterface {
public:
    virtual ~BuilderInterface() = default;
    virtual void build(nonstd::optional<Client> &client) = 0;
};

class Builder final : public BuilderInterface {
public:
    void build(nonstd::optional<Client> &client) override;

private:
    struct {
        struct {
            struct {
                struct {
                    service::client::header::Start start;
                    method::client::Header method;
                    service::client::header::End end;
                } impl;
                package::Start start;
                service::client::Header service{impl.start, impl.method, impl.end};
                package::End end;
            } impl;
            package::Package package{impl.start, impl.service, impl.end};
        } impl;
        file::client::Header file{impl.package};
    } header;

    struct {
        struct {
            struct {
                struct {
                    method::client::Source method;
                } impl;
                package::Start start;
                service::client::Source service{impl.method};
                package::End end;
            } impl;
            package::Package package{impl.start, impl.service, impl.end};
        } impl;
        file::client::Source file{impl.package};
    } source;
};

} // namespace client
namespace remote {

class BuilderInterface {
public:
    virtual ~BuilderInterface() = default;
    virtual void build(nonstd::optional<Remote> &client) = 0;
};

class Builder final : public BuilderInterface {
public:
    void build(nonstd::optional<Remote> &remote) override;

private:
    struct {
        struct {
            struct {
                struct {
                    service::remote::header::Start start;
                    method::Remote method;
                    service::remote::header::End end;
                } impl;
                package::Start start;
                service::remote::Header service{impl.start, impl.method, impl.end};
                package::End end;
            } impl;
            package::Package package{impl.start, impl.service, impl.end};
        } impl;
        file::remote::Header file{impl.package};
    } header;

    struct {
        struct {
            struct {
                struct {
                    has::Remote has;
                    call::Remote call;
                } impl;
                package::Start start;
                service::remote::Source service{impl.has, impl.call};
                package::End end;
            } impl;
            package::Package package{impl.start, impl.service, impl.end};
        } impl;
        file::remote::Source file{impl.package};
    } source;
};

} // namespace remote
} // namespace generator
} // namespace psyrpc
