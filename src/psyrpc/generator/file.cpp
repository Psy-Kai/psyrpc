#include "file.h"

#include <vector>

#include <google/protobuf/io/printer.h>

#include "utilities.h"

namespace psyrpc {
namespace generator {
namespace file {
constexpr auto newLine = nonstd::string_view{"\n"};
namespace templates {
constexpr auto local = nonstd::string_view{"#include \"%name%.h\"\n"};
constexpr auto global = nonstd::string_view{"#include <%name%.h>\n"};
constexpr auto standard = nonstd::string_view{"#include <%name%>\n"};

namespace special {
constexpr auto proto = nonstd::string_view{"#include <%name%.pb.h>\n"};
constexpr auto localProto = nonstd::string_view{"#include \"%name%.pb.h\"\n"};
constexpr auto client = nonstd::string_view{"#include \"%name%.psyrpc_client.h\"\n"};
constexpr auto remote = nonstd::string_view{"#include \"%name%.psyrpc_remote.h\"\n"};
} // namespace special
} // namespace templates
namespace placeholer {
constexpr auto name = nonstd::string_view{"name"};
}

struct Local final {
    nonstd::string_view file;
};
struct Global final {
    nonstd::string_view file;
};
struct Standard final {
    nonstd::string_view file;
};
struct Special final {
    nonstd::string_view file;
    nonstd::string_view templat;
};

void print(google::protobuf::io::Printer &printer, Local local)
{
    printer.Print({{placeholer::name.data(), std::string{local.file}}}, templates::local.data());
}

void print(google::protobuf::io::Printer &printer, Global global)
{
    printer.Print({{placeholer::name.data(), std::string{global.file}}}, templates::global.data());
}

void print(google::protobuf::io::Printer &printer, Standard standard)
{
    printer.Print({{placeholer::name.data(), std::string{standard.file}}},
                  templates::standard.data());
}

void print(google::protobuf::io::Printer &printer, Special special)
{
    printer.Print({{placeholer::name.data(), std::string{special.file}}}, special.templat.data());
}

namespace client {

Header::Header(section::FileInterface &package) : m_package{package} {}

void Header::generate(const google::protobuf::FileDescriptor &file,
                      google::protobuf::io::Printer &printer) const
{
    static constexpr auto serviceBase = nonstd::string_view{"psyrpc/client/servicebase"};
    static constexpr auto chrono = nonstd::string_view{"chrono"};

    print(printer, Global{serviceBase});
    printer.PrintRaw(newLine.data());
    print(printer, Standard{chrono});
    printer.PrintRaw(newLine.data());

    for (int i = 0; i < file.dependency_count(); ++i)
        print(printer,
              Special{utilities::baseName(file.dependency(i)->name()), templates::special::proto});

    printer.PrintRaw(newLine.data());
    print(printer, Special{utilities::baseName(file.name()), templates::special::localProto});
    printer.PrintRaw(newLine.data());
    m_package.generate(file, printer);
}

Source::Source(section::FileInterface &package) : m_package{package} {}

void Source::generate(const google::protobuf::FileDescriptor &file,
                      google::protobuf::io::Printer &printer) const
{
    print(printer, Special{utilities::baseName(file.name()), templates::special::client});

    printer.PrintRaw(newLine.data());
    m_package.generate(file, printer);
}

} // namespace client

namespace remote {

Header::Header(section::FileInterface &package) : m_package{package} {}

void Header::generate(const google::protobuf::FileDescriptor &file,
                      google::protobuf::io::Printer &printer) const
{
    static constexpr auto serviceBase = nonstd::string_view{"psyrpc/remote/servicebase"};
    static constexpr auto protoTypes = nonstd::string_view{"psyrpc/proto/types"};

    print(printer, Global{serviceBase});
    print(printer, Special{protoTypes, templates::special::proto});
    printer.PrintRaw(newLine.data());

    for (int i = 0; i < file.dependency_count(); ++i)
        print(printer,
              Special{utilities::baseName(file.dependency(i)->name()), templates::special::proto});

    printer.PrintRaw(newLine.data());
    print(printer, Special{utilities::baseName(file.name()), templates::special::localProto});
    printer.PrintRaw(newLine.data());
    m_package.generate(file, printer);
}

Source::Source(section::FileInterface &package) : m_package{package} {}

void Source::generate(const google::protobuf::FileDescriptor &file,
                      google::protobuf::io::Printer &printer) const
{
    print(printer, Special{utilities::baseName(file.name()), templates::special::remote});

    printer.PrintRaw(newLine.data());
    m_package.generate(file, printer);
}

} // namespace remote
} // namespace file
} // namespace generator
} // namespace psyrpc
