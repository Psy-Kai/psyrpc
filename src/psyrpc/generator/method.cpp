#include "method.h"

#include <xxhash.h>
#include <google/protobuf/io/printer.h>

#include "utilities.h"

namespace psyrpc {
namespace generator {
namespace method {
namespace placeholder {
constexpr auto serviceName = nonstd::string_view{"serviceName"};
constexpr auto methodName = nonstd::string_view{"methodName"};
constexpr auto resultType = nonstd::string_view{"resultType"};
constexpr auto parameterType = nonstd::string_view{"parameterType"};
constexpr auto methodId = nonstd::string_view{"methodId"};
constexpr auto customMethodId = nonstd::string_view{"customMethodId"};
} // namespace placeholder

namespace client {
namespace header {
namespace signature {
constexpr auto simpleResult =
    nonstd::string_view{"nonstd::optional<psyrpc::client::Result<%resultType%>>\n"
                        "%methodName%("};
constexpr auto streamResult =
    nonstd::string_view{"nonstd::optional<psyrpc::client::stream::Result<%resultType%>>\n"
                        "%methodName%("};
constexpr auto simpleParameter = nonstd::string_view{"const %parameterType% &paramMsg"};
constexpr auto streamParameter =
    nonstd::string_view{"psyrpc::client::stream::Parameter<const %parameterType%> &paramStream"};
constexpr auto separator = nonstd::string_view{",\n"};
constexpr auto waitFor = nonstd::string_view{
    "const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max()"};
constexpr auto end = nonstd::string_view{");\n"};
} // namespace signature

nonstd::string_view result(const google::protobuf::MethodDescriptor &method)
{
    if (method.server_streaming())
        return signature::streamResult;
    return signature::simpleResult;
}

nonstd::string_view parameter(const google::protobuf::MethodDescriptor &method)
{
    if (method.client_streaming())
        return signature::streamParameter;
    return signature::simpleParameter;
}

} // namespace header

void Header::generate(const google::protobuf::MethodDescriptor &method,
                      google::protobuf::io::Printer &printer) const
{
    printer.Indent();
    printer.Indent();
    printer.Print({{placeholder::methodName.data(), method.name()},
                   {placeholder::resultType.data(),
                    utilities::normalized(method.output_type()->full_name())}},
                  header::result(method).data());
    printer.Print({{placeholder::parameterType.data(),
                    utilities::normalized(method.input_type()->full_name())}},
                  header::parameter(method).data());
    printer.Print(header::signature::separator.data());
    printer.Indent();
    printer.Indent();
    printer.Print(header::signature::waitFor.data());
    printer.Print(header::signature::end.data());
    printer.Outdent();
    printer.Outdent();
    printer.Outdent();
    printer.Outdent();
}

namespace source {
namespace signature {
constexpr auto simpleResult =
    nonstd::string_view{"nonstd::optional<psyrpc::client::Result<%resultType%>>\n"
                        "%serviceName%::%methodName%("};
constexpr auto streamResult =
    nonstd::string_view{"nonstd::optional<psyrpc::client::stream::Result<%resultType%>>\n"
                        "%serviceName%::%methodName%("};

nonstd::string_view start(const google::protobuf::MethodDescriptor &method)
{
    if (method.server_streaming()) {
        return streamResult;
    }
    return simpleResult;
}

constexpr auto simpleParameter = header::signature::simpleParameter;
constexpr auto streamParameter = header::signature::streamParameter;
constexpr auto separator = header::signature::separator;
constexpr auto waitFor = nonstd::string_view{"const std::chrono::milliseconds waitFor"};
constexpr auto end = nonstd::string_view{")\n"};
} // namespace signature
namespace body {
constexpr auto open = nonstd::string_view{"{\n"};
constexpr auto ids =
    nonstd::string_view{"static constexpr auto methodId = %methodId%;\n"
                        "static constexpr auto customMethodId = %customMethodId%;\n"
                        "\n"};
constexpr auto simple = nonstd::string_view{
    "auto paramStream = psyrpc::client::stream::Parameter<const %parameterType%>{};\n"
    "auto resultStream =\n"
    "    call<%resultType%, psyrpc::client::servicebase::result::unary>(serviceId, "
    "customServiceId, "
    "methodId, customMethodId, paramStream, "
    "waitFor);\n"
    "if (!resultStream || !paramStream.write(paramMsg, psyrpc::Eot{}))\n"
    "    return {};\n"
    "return resultStream;\n"};
constexpr auto streamingParameter = nonstd::string_view{
    "return call<%resultType%, psyrpc::client::servicebase::result::unary>(serviceId, "
    "customServiceId, methodId, "
    "customMethodId, paramStream, waitFor);\n"};
constexpr auto streamingResult = nonstd::string_view{
    "auto paramStream = psyrpc::client::stream::Parameter<const %parameterType%>{};\n"
    "auto resultStream =\n"
    "    call<%resultType%, psyrpc::client::servicebase::result::stream>(serviceId, "
    "customServiceId, "
    "methodId, customMethodId, paramStream, "
    "waitFor);\n"
    "if (!resultStream || !paramStream.write(paramMsg, psyrpc::Eot{}))\n"
    "    return {};\n"
    "return resultStream;\n"};
constexpr auto streamingBoth = nonstd::string_view{
    "return call<%resultType%, psyrpc::client::servicebase::result::stream>(serviceId, "
    "customServiceId, methodId, "
    "customMethodId, paramStream, waitFor);\n"};
constexpr auto close = nonstd::string_view{"}\n"};

nonstd::string_view text(const google::protobuf::MethodDescriptor &method)
{
    if (method.client_streaming()) {
        if (method.server_streaming()) {
            return body::streamingBoth;
        }
        return body::streamingParameter;
    }
    if (method.server_streaming()) {
        return body::streamingResult;
    }
    return body::simple;
}

} // namespace body
} // namespace source

void Source::generate(const google::protobuf::MethodDescriptor &method,
                      google::protobuf::io::Printer &printer) const
{
    printer.Print({{placeholder::serviceName.data(), method.service()->name()},
                   {placeholder::methodName.data(), method.name()},
                   {placeholder::resultType.data(),
                    utilities::normalized(method.output_type()->full_name())}},
                  source::signature::start(method).data());
    const auto parameter = method.client_streaming() ? source::signature::streamParameter :
                                                       source::signature::simpleParameter;
    printer.Print({{placeholder::parameterType.data(),
                    utilities::normalized(method.input_type()->full_name())}},
                  parameter.data());
    printer.Print(source::signature::separator.data());
    printer.Indent();
    printer.Indent();
    printer.Print(source::signature::waitFor.data());
    printer.Print(source::signature::end.data());
    printer.Outdent();
    printer.Outdent();
    printer.Print(source::body::open.data());
    printer.Indent();
    printer.Indent();
    printer.Print({{placeholder::methodId.data(),
                    std::to_string(XXH32(method.name().data(), method.name().size(), 0))},
                   {placeholder::customMethodId.data(), std::to_string(0)}},
                  source::body::ids.data());
    printer.Print({{placeholder::parameterType.data(),
                    utilities::normalized(method.input_type()->full_name())},
                   {placeholder::resultType.data(),
                    utilities::normalized(method.output_type()->full_name())}},
                  source::body::text(method).data());
    printer.Outdent();
    printer.Outdent();
    printer.Print(source::body::close.data());
}

} // namespace client

namespace remote {
constexpr auto simple =
    nonstd::string_view{"virtual Rpc %methodName%(const %parameterType% &paramMsg, "
                        "%resultType% &resultMsg) = 0;\n"};
constexpr auto streamParam = nonstd::string_view{
    "virtual Rpc %methodName%(psyrpc::remote::stream::Parameter<%parameterType%> paramStream,\n"
    "    %resultType% &resultMsg) = 0;\n"};
constexpr auto streamResult =
    nonstd::string_view{"virtual Rpc %methodName%(const %parameterType% &paramMsg,\n"
                        "    psyrpc::remote::stream::Result<%resultType%> resultStream) = 0;\n"};
constexpr auto streamBoth = nonstd::string_view{
    "virtual Rpc %methodName%(psyrpc::remote::stream::Parameter<%parameterType%> paramStream,\n"
    "    psyrpc::remote::stream::Result<%resultType%> resultStream) = 0;\n"};

nonstd::string_view text(const google::protobuf::MethodDescriptor &method)
{
    if (method.client_streaming()) {
        if (method.server_streaming())
            return streamBoth;
        return streamParam;
    }
    if (method.server_streaming())
        return streamResult;
    return simple;
}

} // namespace remote

void Remote::generate(const google::protobuf::MethodDescriptor &method,
                      google::protobuf::io::Printer &printer) const
{
    printer.Indent();
    printer.Indent();
    printer.Print({{placeholder::methodName.data(), method.name()},
                   {placeholder::parameterType.data(),
                    utilities::normalized(method.input_type()->full_name())},
                   {placeholder::resultType.data(),
                    utilities::normalized(method.output_type()->full_name())}},
                  remote::text(method).data());
    printer.Outdent();
    printer.Outdent();
}

} // namespace method
} // namespace generator
} // namespace psyrpc
