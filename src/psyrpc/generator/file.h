#pragma once

#include "sectioninterfaces.h"

namespace psyrpc {
namespace generator {
namespace file {
namespace client {

class Header final : public section::FileInterface {
public:
    explicit Header(section::FileInterface &package);
    void generate(const google::protobuf::FileDescriptor &file,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::FileInterface &m_package;
};

class Source final : public section::FileInterface {
public:
    explicit Source(section::FileInterface &package);
    void generate(const google::protobuf::FileDescriptor &file,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::FileInterface &m_package;
};

} // namespace client

namespace remote {

class Header final : public section::FileInterface {
public:
    explicit Header(section::FileInterface &package);
    void generate(const google::protobuf::FileDescriptor &file,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::FileInterface &m_package;
};

class Source final : public section::FileInterface {
public:
    explicit Source(section::FileInterface &package);
    void generate(const google::protobuf::FileDescriptor &file,
                  google::protobuf::io::Printer &printer) const override;

private:
    section::FileInterface &m_package;
};

} // namespace remote
} // namespace file
} // namespace generator
} // namespace psyrpc
