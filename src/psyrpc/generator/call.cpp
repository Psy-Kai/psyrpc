#include "call.h"

#include <xxhash.h>
#include <google/protobuf/io/printer.h>

#include "utilities.h"

namespace psyrpc {
namespace generator {
namespace call {
namespace placeholder {
constexpr auto serviceName = nonstd::string_view{"serviceName"};
constexpr auto methodName = nonstd::string_view{"methodName"};
constexpr auto methodId = nonstd::string_view{"methodId"};
constexpr auto resultType = nonstd::string_view{"resultType"};
constexpr auto parameterType = nonstd::string_view{"parameterType"};
} // namespace placeholder

namespace templates {
const auto start = nonstd::string_view{
    "bool %serviceName%Base::call(const psyrpc::proto::XxHashedSignature &method,\n"
    "    psyrpc::common::call::Interface &call, const std::chrono::milliseconds waitFor)\n"
    "{\n"
    "    assert(has(method));\n"
    "    (void)waitFor;\n"
    "\n"};
constexpr auto simple = nonstd::string_view{
    "if (method.value() == %methodId%) {\n"
    "    auto parameterStream =\n"
    "        psyrpc::remote::detail::stream::makeParameter<%parameterType%>(call);\n"
    "    auto resultStream = psyrpc::remote::detail::stream::makeResult<%resultType%>(call);\n"
    "    auto parameter = %parameterType%{};\n"
    "    if (!parameterStream.read(parameter, waitFor))\n"
    "        return false;\n"
    "    auto result = %resultType%{};\n"
    "    if (%methodName%(parameter, result) != Rpc::Success)\n"
    "        return false;\n"
    "    return resultStream.write(result, Eot{});\n"
    "}\n"};
constexpr auto paramStream = nonstd::string_view{
    "if (method.value() == %methodId%) {\n"
    "    auto result = %resultType%{};\n"
    "    auto resultStream = psyrpc::remote::detail::stream::makeResult<%resultType%>(call);\n"
    "    if (%methodName%(psyrpc::remote::detail::stream::makeParameter<%parameterType%>(call), "
    "result) != Rpc::Success)\n"
    "        return false;\n"
    "    return resultStream.write(result, Eot{});\n"
    "}\n"};
constexpr auto resultStream = nonstd::string_view{
    "if (method.value() == %methodId%) {\n"
    "    auto parameterStream =\n"
    "        psyrpc::remote::detail::stream::makeParameter<%parameterType%>(call);\n"
    "    auto resultStream = psyrpc::remote::detail::stream::makeResult<%resultType%>(call);\n"
    "    auto parameter = %parameterType%{};\n"
    "    if (!parameterStream.read(parameter, waitFor))\n"
    "        return false;\n"
    "    return %methodName%(parameter, std::move(resultStream)) == Rpc::Success;\n"
    "}\n"};
constexpr auto fullStream = nonstd::string_view{
    "if (method.value() == %methodId%) {\n"
    "    return "
    "%methodName%(psyrpc::remote::detail::stream::makeParameter<%parameterType%>(call),\n"
    "        psyrpc::remote::detail::stream::makeResult<%resultType%>(call)) == Rpc::Success;\n"
    "}\n"};
constexpr auto end = nonstd::string_view{"\n"
                                         "    return false;\n"
                                         "}\n"};
} // namespace templates

nonstd::string_view text(const google::protobuf::MethodDescriptor &method)
{
    if (method.client_streaming()) {
        if (method.server_streaming())
            return templates::fullStream;
        return templates::paramStream;
    }
    if (method.server_streaming())
        return templates::resultStream;
    return templates::simple;
}

void Remote::generate(const google::protobuf::ServiceDescriptor &service,
                      google::protobuf::io::Printer &printer) const
{
    printer.Print({{placeholder::serviceName.data(), service.name()}}, templates::start.data());
    printer.Indent();
    printer.Indent();
    for (int i = 0; i < service.method_count(); ++i) {
        const auto *method = service.method(i);
        printer.Print({{placeholder::methodName.data(), method->name()},
                       {placeholder::methodId.data(),
                        std::to_string(XXH32(method->name().data(), method->name().size(), 0))},
                       {placeholder::parameterType.data(),
                        utilities::normalized(method->input_type()->full_name())},
                       {placeholder::resultType.data(),
                        utilities::normalized(method->output_type()->full_name())}},
                      text(*method).data());
    }
    printer.Outdent();
    printer.Outdent();
    printer.Print(templates::end.data());
}

} // namespace call
} // namespace generator
} // namespace psyrpc
