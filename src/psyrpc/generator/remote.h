#pragma once

#include <google/protobuf/compiler/code_generator.h>

#include "sectioninterfaces.h"

namespace psyrpc {
namespace generator {

class Remote final : public google::protobuf::compiler::CodeGenerator {
public:
    Remote(section::FileInterface &header, section::FileInterface &source);
    bool Generate(const google::protobuf::FileDescriptor *file, const std::string &parameter,
                  google::protobuf::compiler::GeneratorContext *generator_context,
                  std::string *error) const override;

private:
    section::FileInterface &m_header;
    section::FileInterface &m_source;
};

} // namespace generator
} // namespace psyrpc
