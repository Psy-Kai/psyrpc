#include "utilities.h"

#include <cassert>

namespace psyrpc {
namespace generator {
namespace utilities {

std::string normalized(std::string fullName)
{
    static constexpr auto delimiterProto = '.';
    static constexpr auto delimiterCpp = "::";
    for (auto pos = fullName.find(delimiterProto, 0); pos < fullName.size();
         pos = fullName.find(delimiterProto, pos + 3)) {
        fullName.replace(pos, 1, delimiterCpp);
    }
    return fullName;
}

nonstd::string_view baseName(nonstd::string_view protoFileName)
{
    constexpr auto extension = nonstd::string_view{".proto"};
    assert(extension.size() <= protoFileName.size());
    const auto result = protoFileName.substr(0, protoFileName.size() - extension.size());
    assert(protoFileName.substr(result.size()) == extension);
    return result;
}

} // namespace utilities
} // namespace generator
} // namespace psyrpc
