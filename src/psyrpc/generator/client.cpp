#include "client.h"

#include <google/protobuf/io/printer.h>
#include <google/protobuf/io/zero_copy_stream.h>

#include "utilities.h"

namespace psyrpc {
namespace generator {
namespace extension {
constexpr auto pre = nonstd::string_view{".psyrpc_client"};
constexpr auto header = nonstd::string_view{".h"};
constexpr auto source = nonstd::string_view{".cpp"};
} // namespace extension

Client::Client(section::FileInterface &header, section::FileInterface &source) :
    m_header{header}, m_source{source}
{}

bool Client::Generate(const google::protobuf::FileDescriptor *file, const std::string &,
                      google::protobuf::compiler::GeneratorContext *generator_context,
                      std::string *) const
{
    const auto print = [&](section::FileInterface &generator, nonstd::string_view extension) {
        constexpr auto variableDelimiter = '%';
        auto stream = std::unique_ptr<google::protobuf::io::ZeroCopyOutputStream>{
            generator_context->Open(std::string{utilities::baseName(file->name())} +
                                    extension::pre.data() + extension.data())};
        google::protobuf::io::Printer printer{stream.get(), variableDelimiter};
        generator.generate(*file, printer);
    };
    print(m_header, extension::header);
    print(m_source, extension::source);
    return true;
}

} // namespace generator
} // namespace psyrpc
