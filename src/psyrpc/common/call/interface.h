#pragma once

#include <psyrpc/proto/call.pb.h>

namespace psyrpc {
namespace common {
namespace call {

using Id = uint64_t;

class Interface {
public:
    virtual ~Interface() = default;
    virtual bool write(proto::Package &&package) = 0;
    virtual bool read(proto::Package &package, const std::chrono::milliseconds waitFor) = 0;
};

} // namespace call
} // namespace common
} // namespace psyrpc
