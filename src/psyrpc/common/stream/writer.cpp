#include "writer.h"

#include "../../impl/common/stream/writer.h"

namespace psyrpc {
namespace common {
namespace stream {
namespace base {

Writer::Writer() : m_d{new impl::common::stream::WriterWithGenerator{}} {}

Writer::Writer(call::Interface &call) : Writer{}
{
    m_d->call = &call;
}

Writer::Writer(std::unique_ptr<impl::common::stream::Writer> d) : m_d{std::move(d)} {}

Writer::Writer(Writer &&) noexcept = default;

Writer::~Writer() = default;

Writer &Writer::operator=(Writer &&) noexcept = default;

bool Writer::write(const google::protobuf::MessageLite *message, const bool eot)
{
    if (m_d->call == nullptr) {
        return false;
    }

    auto package = psyrpc::proto::Package{};
    package.set_index(m_d->packageIndex.next());
    package.set_eot(eot);
    if (message != nullptr) {
        package.mutable_data()->set_value(message->SerializeAsString());
    }

    if (!m_d->call->write(std::move(package))) {
        m_d->call = nullptr;
        return false;
    }
    if (eot) {
        m_d->call = nullptr;
    }
    return true;
}

} // namespace base
} // namespace stream
} // namespace common
} // namespace psyrpc
