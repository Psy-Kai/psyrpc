#include "reader.h"

#include "../../impl/common/stream/reader.h"

namespace psyrpc {
namespace common {
namespace stream {
namespace base {

Reader::Reader() : m_d{new impl::common::stream::ReaderWithGenerator{}} {}

Reader::Reader(call::Interface &call) : Reader{}
{
    m_d->call = &call;
}

Reader::Reader(std::unique_ptr<impl::common::stream::Reader> d) : m_d{std::move(d)} {}

Reader::Reader(Reader &&) noexcept = default;

Reader::~Reader() = default;

Reader &Reader::operator=(Reader &&) noexcept = default;

bool Reader::read(google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor)
{
    if (eot() || (m_d->call == nullptr)) {
        return false;
    }

    auto package = psyrpc::proto::Package{};
    if (!m_d->call->read(package, waitFor)) {
        m_d->call = nullptr;
        return false;
    }
    if (package.index() != m_d->packageIndex.next()) {
        m_d->call = nullptr;
        return false;
    }
    if (package.has_data() && !message.ParseFromString(package.data().value())) {
        m_d->call = nullptr;
        return false;
    }
    if (package.eot()) {
        m_d->call = nullptr;
        m_d->eot = true;
    }
    return package.has_data();
}

bool Reader::eot() const
{
    return m_d->eot;
}

void Reader::releaseCall()
{
    m_d->call = nullptr;
}

} // namespace base
} // namespace stream
} // namespace common
} // namespace psyrpc
