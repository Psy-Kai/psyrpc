#pragma once

#include <memory>

#include <psyrpc/proto/package.pb.h>

#include "../call/interface.h"

namespace psyrpc {
namespace impl {
namespace common {
namespace stream {
struct Writer;
} // namespace stream
} // namespace common
} // namespace impl
struct Eot final {};
constexpr auto eot = Eot{};
namespace common {
namespace stream {
namespace base {

class Writer {
public:
    Writer();
    Writer(call::Interface &call);
    Writer(std::unique_ptr<impl::common::stream::Writer> d);
    Writer(Writer &&) noexcept;
    virtual ~Writer();
    Writer &operator=(Writer &&) noexcept;

protected:
    static constexpr auto eot = true;

    bool write(const google::protobuf::MessageLite *message, const bool eot);

private:
    std::unique_ptr<impl::common::stream::Writer> m_d;
};

} // namespace base

template <class TMessage>
class Writer final : private base::Writer {
public:
    using Message = TMessage;
    using base::Writer::Writer;

    bool write(const Message &message, Eot)
    {
        return base::Writer::write(&message, eot);
    }
    bool write(const Message &message)
    {
        return base::Writer::write(&message, !eot);
    }
    bool write(Eot)
    {
        return base::Writer::write(nullptr, eot);
    }
};

} // namespace stream
} // namespace common
} // namespace psyrpc
