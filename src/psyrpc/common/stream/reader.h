#pragma once

#include <memory>

#include <psyrpc/proto/package.pb.h>

#include "../../common/call/interface.h"

namespace psyrpc {
namespace impl {
namespace common {
namespace stream {
struct Reader;
} // namespace stream
} // namespace common
} // namespace impl
namespace common {
namespace stream {
namespace base {

class Reader {
public:
    Reader();
    Reader(call::Interface &call);
    explicit Reader(std::unique_ptr<impl::common::stream::Reader> d);
    Reader(Reader &&) noexcept;
    virtual ~Reader();
    Reader &operator=(Reader &&) noexcept;

protected:
    bool read(google::protobuf::MessageLite &message, const std::chrono::milliseconds waitFor);
    bool eot() const;
    void releaseCall();

private:
    std::unique_ptr<impl::common::stream::Reader> m_d;
};

} // namespace base

template <class TMessage>
class Reader final : private base::Reader {
public:
    using Message = TMessage;
    using base::Reader::Reader;

    bool read(Message &message,
              const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max())
    {
        return base::Reader::read(message, waitFor);
    }
    bool eot() const
    {
        return base::Reader::eot();
    }
};

} // namespace stream
} // namespace common
} // namespace psyrpc
