#pragma once

#include <cstdint>

#if !defined(XXH_PUBLIC_API)
using XXH32_hash_t = uint32_t;
#endif // XXH_PUBLIC_API

namespace psyrpc {

using ServiceId = XXH32_hash_t;
using MethodId = XXH32_hash_t;

} // namespace psyrpc
