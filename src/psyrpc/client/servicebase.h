#pragma once

#include <nonstd/optional.hpp>

#include "callinterface.h"
#include "channelinterface.h"
#include "result.h"
#include "stream/parameter.h"

namespace psyrpc {
namespace impl {
namespace client {
class ServiceBase;
} // namespace client
} // namespace impl
namespace client {
namespace servicebase {
namespace result {

constexpr bool stream = true;
constexpr bool unary = false;

} // namespace result
} // namespace servicebase

class ServiceBase {
public:
    explicit ServiceBase(ChannelInterface &channel);
    explicit ServiceBase(std::unique_ptr<impl::client::ServiceBase> d);
    ServiceBase(ServiceBase &&) noexcept;
    virtual ~ServiceBase();
    ServiceBase &operator=(ServiceBase &&) noexcept;

    template <class TResult, bool tStreamResult, class TParameter>
    typename std::enable_if<tStreamResult, nonstd::optional<stream::Result<TResult>>>::type
    call(const ServiceId serviceId, const ServiceId customServiceId, const MethodId methodId,
         const MethodId customMethodId, stream::Parameter<TParameter> &paramStream,
         const std::chrono::milliseconds waitFor)
    {
        if (auto *call =
                this->call(serviceId, customServiceId, methodId, customMethodId, waitFor)) {
            paramStream = stream::Parameter<TParameter>{*call};
            return stream::Result<TResult>{*call};
        }
        return {};
    }

    template <class TResult, bool tStreamResult, class TParameter>
    typename std::enable_if<!tStreamResult, nonstd::optional<Result<TResult>>>::type
    call(const ServiceId serviceId, const ServiceId customServiceId, const MethodId methodId,
         const MethodId customMethodId, stream::Parameter<TParameter> &paramStream,
         const std::chrono::milliseconds waitFor)
    {
        if (auto *call =
                this->call(serviceId, customServiceId, methodId, customMethodId, waitFor)) {
            paramStream = stream::Parameter<TParameter>{*call};
            return Result<TResult>{*call};
        }
        return {};
    }

private:
    CallInterface *call(const ServiceId serviceId, const ServiceId customServiceId,
                        const MethodId methodId, const MethodId customMethodId,
                        const std::chrono::milliseconds waitFor);

    std::unique_ptr<impl::client::ServiceBase> m_d;
};

} // namespace client
} // namespace psyrpc
