#pragma once

#include <cstddef>

#include <google/protobuf/message_lite.h>

namespace psyrpc {
namespace client {

class TransferInterface {
public:
    virtual ~TransferInterface() = default;
    virtual bool read(google::protobuf::MessageLite &, const std::chrono::milliseconds) = 0;
    virtual bool write(const google::protobuf::MessageLite &) = 0;
};

} // namespace client
} // namespace psyrpc
