#pragma once

#include "../common/call/interface.h"
#include "../common/types.h"

namespace psyrpc {
namespace client {

class CallInterface : public common::call::Interface {
public:
    virtual bool init(const ServiceId serviceId, const ServiceId customServiceId,
                      const MethodId methodId, const MethodId customMethodId,
                      const std::chrono::milliseconds waitFor) = 0;
};

} // namespace client
} // namespace psyrpc
