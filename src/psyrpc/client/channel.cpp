#include "channel.h"

#include <cstdio>

#include "../impl/client/channel.h"
#include "../impl/magic.h"

namespace psyrpc {
namespace client {

Channel::Channel(TransferInterface &transfer) :
    m_d{new impl::client::ChannelWithGenerator{transfer}}
{}

Channel::Channel(std::unique_ptr<impl::client::Channel> d) : m_d{std::move(d)} {}

Channel::Channel(Channel &&) noexcept = default;

Channel::~Channel() = default;

Channel &Channel::operator=(Channel &&) noexcept = default;

bool Channel::write(proto::Call &&call)
{
    auto message = createRpcToWrite();
    *message.mutable_call() = std::move(call);
    return m_d->transfer.write(message);
}

bool Channel::read(proto::Result &result, const std::chrono::milliseconds waitFor)
{
    auto message = proto::Rpc{};
    if (!m_d->transfer.read(message, waitFor)) {
        return false;
    }
#if PSYRPC_PRINT_MESSAGES
    std::puts("client read:");
    message.PrintDebugString();
    std::fflush(stdout);
#endif // PSYRPC_PRINT_MESSAGES
    if (!validateRpc(message) || !message.has_result()) {
        return false;
    }
    result = std::move(*message.mutable_result());
    return true;
}

proto::Rpc Channel::createRpcToWrite()
{
    auto rpc = proto::Rpc{};
    rpc.set_magicconst(magicConst);
    rpc.set_flowcontrol(m_d->flowControl.next());
    return rpc;
}

bool Channel::validateRpc(const proto::Rpc &message)
{
    return message.magicconst() == magicConst && message.flowcontrol() == m_d->flowControl.next();
}

} // namespace client
} // namespace psyrpc
