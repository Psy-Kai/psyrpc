#pragma once

#include <memory>

#include <psyrpc/proto/call.pb.h>

#include "../common/types.h"
#include "callinterface.h"

namespace psyrpc {
namespace impl {
namespace client {
class Call;
} // namespace client
} // namespace impl
namespace client {

class ChannelInterface;

class Call final : public CallInterface {
public:
    Call(const common::call::Id id, ChannelInterface &channel);
    Call(Call &&) noexcept;
    ~Call() override;
    Call &operator=(Call &&) noexcept;
    bool init(const ServiceId serviceId, const ServiceId customServiceId, const MethodId methodId,
              const MethodId customMethodId, const std::chrono::milliseconds waitFor) override;
    bool write(proto::Package &&package) override;
    bool read(proto::Package &package, const std::chrono::milliseconds waitFor) override;

private:
    std::unique_ptr<impl::client::Call> m_d;
};

} // namespace client
} // namespace psyrpc
