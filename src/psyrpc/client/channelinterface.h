#pragma once

#include <psyrpc/proto/channel.pb.h>

namespace psyrpc {
namespace client {

class ChannelInterface {
public:
    virtual ~ChannelInterface() = default;
    virtual bool write(proto::Call &&) = 0;
    virtual bool read(proto::Result &, const std::chrono::milliseconds waitFor) = 0;
};

} // namespace client
} // namespace psyrpc
