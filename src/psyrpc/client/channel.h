#pragma once

#include <memory>

#include "channelinterface.h"
#include "transferinterface.h"

namespace psyrpc {
namespace impl {
namespace client {
class Channel;
} // namespace client
} // namespace impl
namespace client {

class Channel final : public ChannelInterface {
public:
    explicit Channel(TransferInterface &transfer);
    explicit Channel(std::unique_ptr<impl::client::Channel> d);
    Channel(Channel &&) noexcept;
    ~Channel() override;
    Channel &operator=(Channel &&) noexcept;
    bool write(proto::Call &&call) override;
    bool read(proto::Result &result, const std::chrono::milliseconds waitFor) override;

private:
    proto::Rpc createRpcToWrite();
    bool validateRpc(const proto::Rpc &message);

    std::unique_ptr<impl::client::Channel> m_d;
};

} // namespace client
} // namespace psyrpc
