#pragma once

#include "../../common/stream/writer.h"

namespace psyrpc {
namespace client {
namespace stream {

template <class TMessage>
using Parameter = common::stream::Writer<TMessage>;

} // namespace stream
} // namespace client
} // namespace psyrpc
