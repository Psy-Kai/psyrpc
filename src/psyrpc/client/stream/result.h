#pragma once

#include "../../common/stream/reader.h"

namespace psyrpc {
namespace client {
namespace stream {

template <class TMessage>
using Result = common::stream::Reader<TMessage>;

} // namespace stream
} // namespace client
} // namespace psyrpc
