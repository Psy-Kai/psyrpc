#pragma once

#include "stream/result.h"

namespace psyrpc {
namespace client {

template <class TMessage>
class Result final : private common::stream::base::Reader {
    using Base = common::stream::base::Reader;

public:
    using Message = TMessage;
    using common::stream::base::Reader::Reader;

    bool read(Message &message,
              const std::chrono::milliseconds waitFor = std::chrono::milliseconds::max())
    {
        const auto result = Base::read(message, waitFor);
        releaseCall();
        return result;
    }
};

} // namespace client
} // namespace psyrpc
