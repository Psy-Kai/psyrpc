#include "servicebase.h"

#include "../impl/client/servicebase.h"

namespace psyrpc {
namespace client {

ServiceBase::ServiceBase(ChannelInterface &channel) :
    m_d{new impl::client::ServiceBaseWithGenerator{channel}}
{}

ServiceBase::ServiceBase(std::unique_ptr<impl::client::ServiceBase> d) : m_d{std::move(d)} {}

ServiceBase::ServiceBase(ServiceBase &&) noexcept = default;

ServiceBase::~ServiceBase() = default;

ServiceBase &ServiceBase::operator=(ServiceBase &&) noexcept = default;

CallInterface *ServiceBase::call(const ServiceId serviceId, const ServiceId customServiceId,
                                 const MethodId methodId, const MethodId customMethodId,
                                 const std::chrono::milliseconds waitFor)
{
    auto &call = m_d->createCall(m_d->callIdGenerator.next(), m_d->channel);
    if (!call.init(serviceId, customServiceId, methodId, customMethodId, waitFor)) {
        return nullptr;
    }
    return &call;
}

} // namespace client
} // namespace psyrpc
