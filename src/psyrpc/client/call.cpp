#include "call.h"

#include "../impl/client/call.h"
#include "channelinterface.h"

namespace psyrpc {
namespace client {

Call::Call(const common::call::Id id, ChannelInterface &channel) :
    m_d{new impl::client::Call{id, channel}}
{}

Call::Call(Call &&) noexcept = default;

Call::~Call() = default;

Call &Call::operator=(Call &&) noexcept = default;

bool Call::init(const ServiceId serviceId, const ServiceId customServiceId, const MethodId methodId,
                const MethodId customMethodId, const std::chrono::milliseconds waitFor)
{
    auto call = psyrpc::proto::Call{};
    call.set_id(m_d->callId);
    auto &init = *call.mutable_init();
    init.mutable_service()->set_value(serviceId);
    init.mutable_service()->set_customidtoavoidcollision(customServiceId);
    init.mutable_method()->set_value(methodId);
    init.mutable_method()->set_customidtoavoidcollision(customMethodId);
    if (!m_d->channel.write(std::move(call))) {
        return false;
    }

    auto result = psyrpc::proto::Result{};
    if (!m_d->channel.read(result, waitFor)) {
        return false;
    }
    if (result.callid() != m_d->callId) {
        return false;
    }
    if (!result.init()) {
        return false;
    }
    return true;
}

bool Call::write(proto::Package &&package)
{
    auto call = psyrpc::proto::Call{};
    call.set_id(m_d->callId);
    *call.mutable_parameter() = std::move(package);

    return m_d->channel.write(std::move(call));
}

bool Call::read(proto::Package &package, const std::chrono::milliseconds waitFor)
{
    auto result = psyrpc::proto::Result{};
    if (!m_d->channel.read(result, waitFor))
        return false;
    if (result.callid() != m_d->callId)
        return false;
    if (!result.has_result())
        return false;
    package = std::move(*result.mutable_result());
    return true;
}

} // namespace client
} // namespace psyrpc
