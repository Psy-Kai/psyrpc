#pragma once

#include <atomic>
#include <vector>

#include "../../remote/call.h"
#include "../../remote/channel.h"
#include "../../remote/serviceinterface.h"

namespace psyrpc {
namespace impl {
namespace remote {

struct ChannelData final {
    explicit ChannelData(psyrpc::remote::TransferInterface &transfer) : channel{transfer} {}
    ChannelData(ChannelData &&other) :
        channel{std::move(other.channel)}, hasActiveCall{other.hasActiveCall.load()}
    {}
    ChannelData &operator=(ChannelData &&other)
    {
        if (this == &other) {
            return *this;
        }

        channel = std::move(other.channel);
        hasActiveCall = other.hasActiveCall.load();
        return *this;
    }

    psyrpc::remote::Channel channel;
    std::atomic<bool> hasActiveCall{false};
};

class ChannelDataFacade final {};

namespace container {

using Services = std::vector<psyrpc::remote::ServiceInterface *>;

using ChannelData = std::vector<ChannelData>;

} // namespace container
} // namespace remote
} // namespace impl
} // namespace psyrpc
