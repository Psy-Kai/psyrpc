#pragma once

#include "../../remote/channelinterface.h"
#include "../ascendingnumbergenerator.h"

namespace psyrpc {
namespace impl {
namespace remote {

struct Channel {
    ascendingnumbergenerator::Interface &flowControl;
    psyrpc::remote::TransferInterface &transfer;
};

struct ChannelWithGenerator : public Channel {
    explicit ChannelWithGenerator(psyrpc::remote::TransferInterface &transfer) :
        Channel{ascendingNumberGenerator, transfer}
    {}
    AscendingNumberGenerator ascendingNumberGenerator;
};

} // namespace remote
} // namespace impl
} // namespace psyrpc
