#pragma once

#include "calldispatcher.h"
#include "data.h"

namespace psyrpc {
namespace impl {
namespace remote {

struct Server {
    explicit Server(psyrpc::remote::CallDispatcherInterface &callDispatcher) :
        callDispatcher{callDispatcher}
    {}

    container::Services services;
    container::ChannelData channels;
    psyrpc::remote::CallDispatcherInterface &callDispatcher;
};

struct ServerWithDispatcher : public Server {
    ServerWithDispatcher() : Server{callDispatcher} {}
    CallDispatcher callDispatcher{services, channels};
};

} // namespace remote
} // namespace impl
} // namespace psyrpc
