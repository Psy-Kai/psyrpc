#pragma once

#include "../../remote/calldispatcherinterface.h"
#include "data.h"

namespace psyrpc {
namespace impl {
namespace remote {
namespace calldispatcher {
namespace adapter {

template <class T>
class ChannelData final {
public:
    explicit ChannelData(T &t) : m_t{t} {}

    psyrpc::remote::ChannelInterface &channel()
    {
        return m_t.channel;
    }

    std::atomic<bool> &hasActiveCall()
    {
        return m_t.hasActiveCall;
    }

private:
    T &m_t;
};

} // namespace adapter

template <class T>
struct ChannelFinder {
    template <class TInputIterator>
    static T *find(TInputIterator begin, TInputIterator end,
                   psyrpc::remote::TransferInterface &transfer)
    {
        const auto it = std::find_if(begin, end, [&transfer](T &item) {
            return &adapter::ChannelData<T>{item}.channel().transfer() == &transfer;
        });
        if (it == end) {
            return nullptr;
        }
        return &(*it);
    }
};

class Dispatcher {
public:
    explicit Dispatcher(container::Services &services) : m_services{services} {}

    template <class T>
    void dispatch(adapter::ChannelData<T> channelData, const std::chrono::milliseconds waitFor)
    {
        if (channelData.hasActiveCall().exchange(true)) {
            return;
        }

        proto::Call callMessage;
        if (!channelData.channel().read(callMessage, waitFor)) {
            return;
        }
        if (!callMessage.has_init()) {
            channelData.channel().write(proto::Rpc::NoActiveCall);
            return;
        }

        const auto &service = callMessage.init().service();
        const auto serviceIt =
            std::find_if(std::begin(m_services), std::end(m_services),
                         [&service](const psyrpc::remote::ServiceInterface *item) {
                             return item->id() == service.value() &&
                                 item->customId() == service.customidtoavoidcollision();
                         });

        auto result = proto::Result{};
        result.set_callid(callMessage.id());

        if (serviceIt == std::end(m_services) || !(*serviceIt)->has(callMessage.init().method())) {
            result.set_init(false);
            channelData.channel().write(std::move(result));
            return;
        } else {
            result.set_init(true);
            channelData.channel().write(std::move(result));
        }

        psyrpc::remote::Call call{callMessage.id(), channelData.channel()};
        (*serviceIt)->call(callMessage.init().method(), call, waitFor);
        channelData.hasActiveCall() = false;
    }

private:
    container::Services &m_services;
};

struct Wiring {
    Wiring(container::Services &services) : dispatcher{services} {}

    Dispatcher dispatcher;
};

template <class TChannelFinder, class TDispatcher>
class CallDispatcher : public psyrpc::remote::CallDispatcherInterface {
public:
    CallDispatcher(TDispatcher &dispatcher, container::ChannelData &channels) :
        m_dispatcher{dispatcher}, m_channels{channels}
    {}

    void dispatch(psyrpc::remote::TransferInterface &transfer,
                  const std::chrono::milliseconds waitFor) override
    {
        if (auto *channelData =
                TChannelFinder::find(std::begin(m_channels), std::end(m_channels), transfer)) {
            m_dispatcher.dispatch(adapter::ChannelData<ChannelData>{*channelData}, waitFor);
        }
    }

private:
    TDispatcher &m_dispatcher;
    container::ChannelData &m_channels;
};

using Base = CallDispatcher<ChannelFinder<ChannelData>, Dispatcher>;

} // namespace calldispatcher

class CallDispatcher final : calldispatcher::Wiring, public calldispatcher::Base {
public:
    CallDispatcher(container::Services &services, container::ChannelData &channels) :
        Wiring{services}, calldispatcher::Base{Wiring::dispatcher, channels}
    {}
};

} // namespace remote
} // namespace impl
} // namespace psyrpc
