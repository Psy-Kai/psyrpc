#pragma once

#include "../../remote/channelinterface.h"
#include "../common/call.h"

namespace psyrpc {
namespace impl {
namespace remote {

struct Call {
    ascendingnumbergenerator::ValueType callId;
    psyrpc::remote::ChannelInterface &channel;
};

} // namespace remote
} // namespace impl
} // namespace psyrpc
