#pragma once

#include <cstdint>
#include <type_traits>

namespace psyrpc {
namespace ascendingnumbergenerator {

using ValueType = uint64_t;

class Interface {
public:
    virtual ~Interface() = default;
    virtual ValueType next() = 0;
};

struct Wiring {
    ValueType value = 0;
};

class AscendingNumberGenerator : public ascendingnumbergenerator::Interface {
public:
    AscendingNumberGenerator(ValueType &value) : m_flowControlValue{value} {}

    ascendingnumbergenerator::ValueType next() override
    {
        ++m_flowControlValue;
        if (m_flowControlValue == 0)
            m_flowControlValue = 1;
        return m_flowControlValue;
    }

private:
    ValueType &m_flowControlValue;
};

} // namespace ascendingnumbergenerator

class AscendingNumberGenerator final :
    ascendingnumbergenerator::Wiring,
    public ascendingnumbergenerator::AscendingNumberGenerator {
public:
    AscendingNumberGenerator() : ascendingnumbergenerator::AscendingNumberGenerator{value} {}
};

} // namespace psyrpc
