#pragma once

#include <cstdint>

namespace psyrpc {
/* "Psy-Kai" */
constexpr uint64_t magicConst = 0x5073792d4b616900;
} // namespace psyrpc
