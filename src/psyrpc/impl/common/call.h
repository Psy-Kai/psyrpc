#include "../../common/call/interface.h"
#include "../ascendingnumbergenerator.h"

namespace psyrpc {
namespace impl {
namespace common {

static_assert(std::is_same<psyrpc::common::call::Id, ascendingnumbergenerator::ValueType>::value,
              "");

} // namespace common
} // namespace impl
} // namespace psyrpc
