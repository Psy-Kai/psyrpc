#pragma once

#include "../../../common/call/interface.h"
#include "../../ascendingnumbergenerator.h"

namespace psyrpc {
namespace impl {
namespace common {
namespace stream {

struct Reader {
    explicit Reader(psyrpc::ascendingnumbergenerator::Interface &packageIndex) :
        packageIndex{packageIndex}
    {}

    psyrpc::ascendingnumbergenerator::Interface &packageIndex;
    psyrpc::common::call::Interface *call = nullptr;
    bool eot = false;
};

struct ReaderWithGenerator : public Reader {
    ReaderWithGenerator() : Reader{ascendingNumberGenerator} {}
    psyrpc::AscendingNumberGenerator ascendingNumberGenerator;
};

} // namespace stream
} // namespace common
} // namespace impl
} // namespace psyrpc
