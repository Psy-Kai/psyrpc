#pragma once

#include "../../../common/call/interface.h"
#include "../../ascendingnumbergenerator.h"

namespace psyrpc {
namespace impl {
namespace common {
namespace stream {

struct Writer {
    psyrpc::ascendingnumbergenerator::Interface &packageIndex;
    psyrpc::common::call::Interface *call;
};

struct WriterWithGenerator : public Writer {
    WriterWithGenerator() : Writer{ascendingNumberGenerator, nullptr} {}
    psyrpc::AscendingNumberGenerator ascendingNumberGenerator;
};

} // namespace stream
} // namespace common
} // namespace impl
} // namespace psyrpc
