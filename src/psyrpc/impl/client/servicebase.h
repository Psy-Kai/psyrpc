#pragma once

#include <nonstd/optional.hpp>

#include "../../client/call.h"
#include "../../client/channelinterface.h"
#include "../ascendingnumbergenerator.h"

namespace psyrpc {
namespace impl {
namespace client {

struct ServiceBase {
    ServiceBase(ascendingnumbergenerator::Interface &callIdGenerator,
                psyrpc::client::ChannelInterface &channel) :
        callIdGenerator{callIdGenerator}, channel{channel}
    {}
    virtual ~ServiceBase() = default;

    virtual psyrpc::client::CallInterface &createCall(const std::size_t id,
                                                      psyrpc::client::ChannelInterface &channel)
    {
        return call.emplace(id, channel);
    }

    ascendingnumbergenerator::Interface &callIdGenerator;
    psyrpc::client::ChannelInterface &channel;
    nonstd::optional<psyrpc::client::Call> call;
};

struct ServiceBaseWithGenerator : public ServiceBase {
    explicit ServiceBaseWithGenerator(psyrpc::client::ChannelInterface &channel) :
        ServiceBase{ascendingNumberGenerator, channel}
    {}
    AscendingNumberGenerator ascendingNumberGenerator;
};

} // namespace client
} // namespace impl
} // namespace psyrpc
