#pragma once

#include "../../client/transferinterface.h"
#include "../ascendingnumbergenerator.h"

namespace psyrpc {
namespace impl {
namespace client {

struct Channel {
    psyrpc::client::TransferInterface &transfer;
    ascendingnumbergenerator::Interface &flowControl;
};

struct ChannelWithGenerator : public Channel {
    ChannelWithGenerator(psyrpc::client::TransferInterface &transfer) :
        Channel{transfer, ascendingNumberGeneratorInstance}
    {}

    AscendingNumberGenerator ascendingNumberGeneratorInstance;
};

} // namespace client
} // namespace impl
} // namespace psyrpc
