#pragma once

#include "../../client/channelinterface.h"
#include "../common/call.h"

namespace psyrpc {
namespace impl {
namespace client {

struct Call {
    ascendingnumbergenerator::ValueType callId;
    psyrpc::client::ChannelInterface &channel;
};

} // namespace client
} // namespace impl
} // namespace psyrpc
