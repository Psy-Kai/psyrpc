import qbs.FileInfo

Project {
    StaticLibrary {
        name: "psyRPC.generator.cpp.library"
        qbs.profile: "conan"

        files: [
            "psyrpc/generator/**/*.cpp",
            "psyrpc/generator/**/*.h",
        ]

        cpp.cxxLanguageVersion: "c++11"

        Depends { name: "optional-lite" }
        Depends { name: "string-view-lite" }
        Depends { name: "protobuf.cpp" }
        Depends { name: "libxxhash" }
        Depends { name: "protobuf_full_package" }
        Export {
            cpp.includePaths: exportingProduct.sourceDirectory
            cpp.cxxLanguageVersion: "c++11"

            Depends { name: "cpp" }
            Depends { name: "optional-lite" }
            Depends { name: "string-view-lite" }
            Depends { name: "protobuf.cpp" }
            Depends { name: "libxxhash" }
            Depends { name: "protobuf_full_package" }
        }
    }

    CppApplication {
        name: "psyRPC.generator.cpp"
        qbs.profile: "conan"
        targetName: "protoc-gen-psyRPC"

        files: [
            "psyrpc/generator/generator.cpp",
        ]

        cpp.cxxLanguageVersion: "c++11"

        Group {
            fileTagsFilter: "application"
            qbs.install: true
            qbs.installDir: "bin"
        }

        Depends { name: "psyRPC.generator.cpp.library" }

        Export {
            readonly property path pluginPath: exportingProduct.buildDirectory

            Depends { name: "psyRPC.cpp" }
        }
    }
}
