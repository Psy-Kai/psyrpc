import qbs
import qbs.FileInfo

StaticLibrary {
    name: "psyRPC.cpp"
        qbs.profile: "conan"
    targetName: "psyrpc-cpp"

    readonly property bool printMessages: false
    readonly property var _defines: {
        var defines = [];
        if (printMessages)
            defines.push("PSYRPC_PRINT_MESSAGES=1");
        return defines;
    }

    files: [
        "psyrpc/client/**/*.cpp",
        "psyrpc/client/**/*.hpp",
        "psyrpc/common/**/*.cpp",
        "psyrpc/common/**/*.hpp",
        "psyrpc/impl/**/*.cpp",
        "psyrpc/impl/**/*.hpp",
        "psyrpc/remote/**/*.cpp",
        "psyrpc/remote/**/*.hpp",
        "psyrpc/proto/*.proto",
    ]

    cpp.cxxLanguageVersion: "c++11"
    cpp.defines: _defines
    protobuf.cpp.importPaths: sourceDirectory

    Group {
        name: "public header"
        files: [
            "psyrpc/client/**/*.h",
            "psyrpc/common/**/*.h",
            "psyrpc/remote/**/*.h",
        ]
        qbs.install: true
        qbs.installSourceBase: sourceDirectory
        qbs.installDir: "include"
    }
    Group {
        fileTagsFilter: "protobuf.hpp"
        qbs.install: true
        qbs.installSourceBase: protobuf.cpp.outputDir
        qbs.installDir: "include"
    }
    Group {
        fileTagsFilter: "staticlibrary"
        qbs.install: true
        qbs.installDir: "lib"
    }

    Depends { name: "optional-lite" }
    Depends { name: "string-view-lite" }
    Depends { name: "protobuf.cpp" }
    Export {
        cpp.includePaths: [].concat(
            exportingProduct.sourceDirectory,
            exportingProduct.protobuf.cpp.outputDir)
        cpp.cxxLanguageVersion: "c++11"
        cpp.defines: exportingProduct._defines

        Depends { name: "cpp" }
        Depends { name: "optional-lite" }
        Depends { name: "string-view-lite" }
        Depends { name: "protobuf.cpp" }
    }
}
