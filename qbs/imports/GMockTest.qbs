CppApplication {
    condition: psyRPCbuildconfig.buildTests
    type: base.concat("autotest")
    qbs.profile: "conan"

    Depends { name: "gmock" }
    Depends { name: "psyRPCbuildconfig" }
}
